# General

All error responses are returned as a JSON object that contains at least a
field code and any additional parameters.

Unless otherwise specified, the content of all requests and responses is in JSON format.

For a description of the auth schema and login process see also  [https://laji.fi/about/1328](https://laji.fi/about/1328).

## Authentication and security

No need to log in for interface calls. Customer systems must verify authentication
with a validation call. All traffic must be secure (https).

### /login/

The address to which the client system should redirect the end user to for authentication. Redirection should be included in parameters described below in more detail. The end user is shown an authentication portal from which one can choose a method to authenticate.

#### Parameters

The parameters are given in the query section of the address (`?Source=<source address>&next=<path to redirect to after authentication>`)

* target - Client system ontology database identifier (eg KE.176)
* next - Relative path in the client system to which the user should be directed. For example, if the login-address defined in triplestore database for the service is `https://myapp.example.com/login` and next parameter is  `/page/4` then after logging in the user is redirected to `https://myapp.example.com/login` and it is up for the service to read the next-parameter from the login event and redirect the user to internal page of the service: `/page/4`.
* redirectMethod * - Default POST. GET can also be given. Specifies which method directs the user back to the client system after successful login.
* offerPermanent - Whether to offer the user the option of a permanent authentication ["true" | "false"].

### /token/

The interface verifies the authenticity of the authentication received from the user or removes the authentication.

### GET

Queries the user's token for user information (and validates that the token is valid and exists)

```plaintext
GET /token/{token}
```

Response upon successful validation:

```plaintext
200 OK

AuthenticationEvent {
    user (UserDetails),
    source (String): authentication system [HAKA | VIRTU | FACEBOOK | GOOGLE | LOCAL | INATURALIST | OMARIISTA] APPLE,
    target (String): the ontology database identifier of the client system requesting authentication (KE. *),
    next (String): The relative address given by the system requesting authentication to which the user was aiming
}

UserDetails {
     qname (String): The identifier of the authenticated end user (MA. *)
     name (String): the name of the authenticated end user,
     email (String): The email address of the authenticated end user
     roles (Array [String]): user roles in the ontology database
}
```

If validation fails:

```plaintext
400 BAD REQUEST
```

### DELETE

Logging out will delete the session authentication token:

```plaintext
DELETE /token/{token}
```

with response:

```plaintext
200 OK
```
