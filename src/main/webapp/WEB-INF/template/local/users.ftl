<#import "../common.ftl" as common>
<!doctype html>
<html lang="${language!""}">
<@common.head title="Paikalliset käyttäjät"></@common.head>
<body>
<@common.navbar></@common.navbar>

<div class="container">
    <#if (parameters['deleted'])??>
        <h3 class="text-success">Käyttäjä ${parameters['deleted'][0]} poistettiin</h3>
    </#if>

    <#if (users?size == 0) >
        <h4 class="text-warning">Ei paikallisia käyttäjiä</h4>
    </#if>
    <#if (users?size > 0)>
        <div class="panel panel-info" style="margin-top: 10px">
            <table class="table table-responsive">
                <div class="panel-heading">
                    <h2>Paikalliset käyttäjät</h2>
                </div>

                <thead>
                <tr>
                    <th>Sähköpostiosoite</th>
                    <th>Nimi</th>
                    <th>Sähköposti varmistettu</th>
                    <th></th>
                </tr>
                </thead>

                <tbody>
                    <#list users as user>
                        <#if user_index % 2 == 0>
                        <#else>
                        <tr class="stripe">
                        </#if>
                        <td><a href="mailto:${user.email?html}">${user.email?html}</a></td>
                        <td>${(user.fullName?html)!""}</td>
                        <td>
                            <#if user.confirmed>
                                <span class="label label-success">Kyllä</span>
                            <#else >
                                <span class="label label-danger">Ei</span>
                            </#if>
                        </td>
                        <td colspan="1">
                            <form method="POST">
                                <input type="hidden" name="email" value="${user.email?html}">
                                <button name="action" value="${deleteAction}" class="btn btn-danger delete-button"
                                        style="width: 100%">
                                    Poista
                                </button>
                            </form>
                        </td>
                    </tr>
                    </#list>
                </tbody>
            </table>
        </div>
    </#if>
</div>

<script src="${baseUri}webjars/jquery/1.11.3/jquery.min.js" type="application/javascript"></script>
<script type="application/javascript">
    (function () {
        $('.delete-button').click(function (e) {
            if (!confirm('Haluatko varmasti poistaa käyttäjän?')) {
                e.preventDefault();
            }
        });
    })();
</script>
</body>
</html>
