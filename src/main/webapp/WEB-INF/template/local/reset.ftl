<#import "../common.ftl" as common>
<!doctype html>
<html lang="${language!""}">
<@common.head title=messages.get('title')></@common.head>
<body>
<@common.navbar></@common.navbar>

<div class="container">
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default" style="margin-top: 10px">
                <div class="panel-heading">
                    <h3>${messages.get('title')}</h3>
                </div>
                <div class="panel-body">
                    <form method="POST" id="reset-form">
                        <div class="form-group">
                            <label>${messages.get('password')}</label>
                            <input type="password" name="password" class="form-control"
                                   data-parsley-required="true"
                                   data-parsley-error-message="${messages.get('password_error')}"
                                   data-parsley-pattern="^(?=.*[A-Z])(?=.*[a-z])(?=.*((\W)|(\d))).{8,}$"
                                   data-parsley-minlength="10" id="password-field"/>
                        </div>
                        <div class="form-group">
                            <label>${messages.get('password_again')}</label>
                            <input type="password" name="passwordAgain" class="form-control"
                                   data-parsley-equalto="#password-field"/>
                        </div>
                        <input type="hidden" name="email" class="form-control" value="${email?html}">
                        <input type="hidden" name="hash" class="form-control" value="${hash?html}">
                        <input type="hidden" name="nonce" class="form-control" value="${nonce?html}">

                        <button class="btn btn-primary btn-lg submit">${messages.get('reset')}</button>
                    </form>
                </div>
            </div>
        </div>

        <div class="col-md-push-2 col-md-4" style="background-color: #ECF0F1">
            <h4>${messages.get('password_requirements_title')}</h4>
            <ul>
            	<li>${messages.get('password_requirements_list_1')}</li>
            	<li>${messages.get('password_requirements_list_2')}</li>
            	<li>${messages.get('password_requirements_list_3')}</li>
            </ul>
        </div>
    </div>
</div>

<@common.validateForm selector='#reset-form'></@common.validateForm>
</body>
</html>
