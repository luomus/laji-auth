<#import "../common.ftl" as common>
<!doctype html>
<html lang="${language!""}">
<@common.head title=messages.get('title')></@common.head>
<body>
<@common.navbar></@common.navbar>
<div class="container">
    <div class="row">
        <div class="col-md-6">
        <#if (parameters['emailInUse'])??>
            <h3 class="text-danger">${messages.get('email_in_use')}</h3>
        </#if>
            <div class="panel panel-default" style="margin-top: 10px">
                <div class="panel-heading">
                    <h3>${messages.get('title')}</h3>
                </div>
                <form method="POST" id="register-form">
                	<input type="hidden" name="target" value="${target?html}"/>
                    <input type="hidden" name="next" value="${next?html}"/>
                    <input type="hidden" name="redirectMethod" value="${redirectMethod?html}"/>
                    <input type="hidden" name="permanent" value="${permanent?html}"/>
                    <div class="panel-body">
                        <div class="form-group">
							<label>${messages.get('preferredName')}</label>
							<input type="text" name="preferredName" id="preferredName" class="form-control" value="${preferredName?html}"
                    	           data-parsley-required="true"
                        	       data-parsley-minlength="2">
						</div>
						<div class="form-group">
							<label>${messages.get('inheritedName')}</label>
    		    	        <input type="text" name="inheritedName" id="inheritedName" class="form-control" value="${inheritedName?html}"
                    	           data-parsley-required="true"
                                   data-parsley-minlength="2">
						</div>
                        <div class="form-group">
							<label>${messages.get('group')}</label>
    		    	        <input type="text" name="group" id="group" class="form-control"
                    	           data-parsley-required="false"
                                   data-parsley-minlength="3">
						</div>                        <div class="form-group">
                            <label>${messages.get('your_email')}</label>
                            <div class="input-group">
                                <span class="input-group-addon">@</span>
                                <input type="text" name="email" class="form-control" value="${email?html}"
                                       data-parsley-required="true"
                                       data-parsley-type="email"
                                       placeholder="${messages.get('email_placeholder')}"/>
                            </div>
                        </div>
						<div class="form-group">
                            <label>${messages.get('password')}</label>
                            <input type="password" name="password" class="form-control"
                                   data-parsley-required="true"
                                   data-parsley-error-message="${messages.get('password_error')}"
                                   data-parsley-pattern="^(?=.*[A-Z])(?=.*[a-z])(?=.*((\W)|(\d))).{8,}$"
                                   data-parsley-minlength="10" id="password-field"/>
                        </div>
                        <div class="form-group">
                            <label>${messages.get('password_again')}</label>
                            <input type="password" name="passwordAgain" class="form-control"
                                   data-parsley-required="true"
                                   data-parsley-equalto="#password-field"/>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <button class="btn btn-primary btn-lg submit">${messages.get('register')}</button>
                    </div>
                </form>
            </div>
        </div>

		<div class="col-md-push-2 col-md-4" style="background-color: #ECF0F1">

            <h5>${messages.get('system_name')}</h5>
			<h3>${systemName?html}</h3>

			<br />
			
            <h4>${messages.get('password_requirements_title')}</h4>
            <ul>
            	<li>${messages.get('password_requirements_list_1')}</li>
            	<li>${messages.get('password_requirements_list_2')}</li>
            	<li>${messages.get('password_requirements_list_3')}</li>
            </ul>
            
			<br/>
            <#if helpUrl??><p><a target="_blank" href="${helpUrl}">${messages.get('help')}</a></p></#if>
            <p><a target="_blank" href="${privacyPolicyUrl}">${messages.get('privacy_policy')}</a></p>
        </div>
        
    </div>

<@common.validateForm selector='#register-form'></@common.validateForm>
</div>
</body>
</html>
