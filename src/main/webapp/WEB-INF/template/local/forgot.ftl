<#import "../common.ftl" as common>
<!doctype html>
<html lang="${language!""}">
<@common.head title=messages.get('title')></@common.head>
<body>
<@common.navbar></@common.navbar>
<div class="container">
    <div class="row">
        <div class="col-md-6">
        <#if parameters['sent']??>
            <h4 id="sent-notification" class="text-success">${messages.get('reset_sent')}</h4>
        </#if>
            <div class="panel panel-default" style="margin-top: 10px">
                <div class="panel-heading">
                    <h3>${messages.get('title')}</h3>
                </div>
                <form method="POST" id="forgot-form">
                    <div class="panel-body">
                        <div class="form-group">
                            <label>${messages.get('your_email')}</label>

                            <div class="input-group">
                                <span class="input-group-addon">@</span>
                                <input type="text" name="email" class="form-control"
                                       data-parsley-required="true"
                                       data-parsley-type="email"
                                       placeholder="${messages.get('email_placeholder')}"/>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <button class="btn btn-primary btn-lg submit">${messages.get('submit')}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</body>
<@common.validateForm selector='#forgot-form'></@common.validateForm>
</html>
