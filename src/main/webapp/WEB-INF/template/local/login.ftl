<#import "../common.ftl" as common>
<!doctype html>
<html lang="${language!""}">
<@common.head title=messages.get('title')></@common.head>
<body>
<@common.navbar></@common.navbar>

<div class="container">
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default" style="margin-top: 10px">
                <div class="panel-heading">
                    <h3>${messages.get('title')}</h3>
                    <#if incorrect>
                        <h4 id="login-incorrect" class="text-danger">${messages.get('incorrect')}</h4>
                    </#if>
                    <#if blocked>
                        <h4 id="login-incorrect" class="text-danger">${messages.get('blocked')}</h4>
                    </#if>
                </div>

                <form method="POST" id="login-form">
                    <div class="panel-body">
                        <div class="form-group">
                            <label>${messages.get('your_email')}</label>

                            <div class="input-group">
                                <span class="input-group-addon">@</span>
                                <input type="text" name="email" class="form-control"
                                       data-parsley-required="true"
                                       data-parsley-type="email"
                                       placeholder="${messages.get('email_placeholder')}"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>${messages.get('password')}</label>
                            <input type="password" name="password" class="form-control"
                                   data-parsley-required="true"/>
                        </div>
                        <input type="hidden" name="target" value="${target?html}"/>
                        <input type="hidden" name="next" value="${next?html}"/>
                        <input type="hidden" name="redirectMethod" value="${redirectMethod?html}"/>
                        <input type="hidden" name="permanent" value="${permanent?html}"/>
                    </div>
                    <div class="panel-footer">
                        <button class="btn btn-primary btn-lg submit">${messages.get('log_in')}</button>
                    </div>
                </form>
            </div>
        </div>

        <div class="col-md-push-2 col-md-4" style="background-color: #ECF0F1">
			<h5>${messages.get('system_name')}</h5>
			<h3>${systemName?html}</h3>
			<br />
            <a href="${registerUri?html}">
                <button id="register" class="btn btn-lg btn-success"
                        style="margin-top:20px; margin-bottom: 10px; width: 100%">${messages.get('register')}</button>
            </a>
            <a href="${forgotUri?html}">
                <button id="forgot-password" class="btn btn-lg btn-primary"
                        style="margin-bottom: 20px; width: 100%">${messages.get('forgot_password')}</button>
            </a>
            
            <#if helpUrl??><p><a target="_blank" href="${helpUrl}">${messages.get('help')}</a></p></#if>
			<p><a target="_blank" href="${privacyPolicyUrl}">${messages.get('privacy_policy')}</a></p>
        </div>
    </div>
</div>

<@common.validateForm selector='#login-form'></@common.validateForm>
</body>
</html>
