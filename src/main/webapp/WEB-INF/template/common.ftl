﻿<#macro head title>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <title>${title}</title>
    
	<link rel="icon" href="${staticDir}/favicon.ico" type="image/x-icon"/>
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,500,700" rel="stylesheet" type="text/css">
    <link href="${baseUri}webjars/jquery-ui/1.11.3/jquery-ui.min.css" rel="stylesheet">
    <link href="${staticDir}/bootstrap.min.css" rel="stylesheet">
    <link href="${staticDir}/style.css?${timestamp}" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
</#macro>

<#macro navbar>
<nav class="navbar navbar-inverse">
    <div class="container">
        <div class="navbar-header">
        	<#if devMode>
        		<a class="navbar-brand uppercase" href="https://dev.laji.fi">laji.fi &nbsp;<span style="color: lightcoral">KEHITYSYMPÄRISTÖ</span></a>
			<#elseif stagingMode>
				<a class="navbar-brand uppercase" href="https://dev.laji.fi">laji.fi &nbsp;<span style="color: lightcoral">TESTAUSYMPÄRISTÖ</span></a>
			<#else> 
				<a class="navbar-brand uppercase" href="https://laji.fi">laji.fi</a> 
			</#if>
        </div>
        <div class="pull-right" id="language-bar">
            <ul>
                <#if language != 'fi'>
                    <li><a href="${languages['fi']}">suomeksi</a></li></#if>
                <#if language != 'sv'>
                    <li><a href="${languages['sv']}">på svenska</a></li></#if>
                <#if language != 'en'>
                    <li><a href="${languages['en']}">in english</a></li></#if>
				<#if loggedIn && logoutUri??>
					<li><a href="${logoutUri?html}">
					<button class="btn btn-lg btn-primary" id="logout">${messages.get('logout')}</button>
				</a></li>
				</#if>
            </ul>
	        
        </div>

    </div>
</nav>
</#macro>

<#macro validateForm selector>
<script src="${baseUri}webjars/jquery/1.11.3/jquery.min.js" type="application/javascript"></script>
<script src="${baseUri}webjars/parsleyjs/2.1.2/dist/parsley.min.js" type="application/javascript"></script>
<script src="${baseUri}webjars/parsleyjs/2.1.2/src/i18n/${language}.js" type="application/javascript"></script>
<script>
    (function () {
        var getFormGroup = function (parsleyField) {
            return parsleyField.$element.closest('.form-group');
        };

        var $registerForm = $('${selector}');
        $registerForm.parsley({
            successClass: 'has-success',
            errorClass: 'has-error',
            classHandler: getFormGroup,
            errorsContainer: getFormGroup,
            errorsWrapper: '<span class="help-inline"></span>',
            errorTemplate: '<span></span>'
        });
    })();
</script>
</#macro>

<#macro authSource name>
	<img src="${staticDir}/${authSourceLogo(name)}" alt="${name?html}">
    <label>${messages.get(name)}</label>
</#macro>

<#function authSourceLogo name>
  <#if name == "FACEBOOK"> <#return "facebook.png"></#if>
  <#if name == "GOOGLE"> <#return "google.png"></#if>
  <#if name == "HAKA"> <#return "haka.gif"></#if>
  <#if name == "LOCAL"> <#return "LAJI_fisven.png"></#if>
  <#if name == "INATURALIST"> <#return "inaturalist.png"></#if>
  <#if name == "OMARIISTA"> <#return "omariista.png"></#if>
  <#if name == "VIRTU"> <#return "virtu.png"></#if>
  <#if name == "APPLE"> <#return "apple.png"></#if>
  <#if name == "LINTUVAARA"> <#return "lintuvaara.png"></#if>
  <#if name == "HATIKKA"> <#return "hatikka.png"></#if>
  <#if name == "VANHA_HATIKKA"> <#return "vanha_hatikka.png"></#if>
  <#if name == "HYONTEISTIETOKANTA"> <#return "hyonteistietokanta.png"></#if>
  <#return "unknown.png">
</#function>
