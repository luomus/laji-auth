<#import "common.ftl" as common>
<!doctype html>
<html lang="${language!""}">
<@common.head title="Testisivu"></@common.head>
<body>
<@common.navbar></@common.navbar>

<div class="container">
    <h1>Laji-auth-testijärjestelmä</h1>
    <#if (token)??>
        <h2>Tervetuloa ${(authenticationInfo.user.name)!"NO NAME"} (<span id="user-id">${authenticationInfo.user.qname.or('NO QNAME')}</span>)!</h2>

        <h3>Token:</h3>
        <pre id="auth-token">${token?html}</pre>
        
        <h3>Info:</h3>
        <pre id="auth-info">${serializedAuthInfo?html}</pre>
    <#else>
        <h2>Et ole kirjautunut sisään</h2>
        <a href="${loginUrl?html}">
            <button id="login" class="btn btn-default btn-lg">Kirjaudu</button>
        </a>
        <br/>
        <a href="${hakaLoginUrl?html}">
            <button class="btn btn-default btn-lg" style="margin-top: 10px">Kirjaudu (HAKA)</button>
        </a>
    </#if>
</div>

</body>
</html>
