<#import "common.ftl" as common>
<!doctype html>
<html lang="${language!""}">
<@common.head title=messages.get('title')></@common.head>
<body>
<@common.navbar></@common.navbar>
<div class="container">

	<#if associations?has_content>
    <div class="row">
        <div class="col-md-8">
            <h1>${messages.get('title')}</h1>

            <p>${messages.get('first_paragraph', authentication.user.email)}</p>
            
            <div class="associations">
           	<#list associations as association>
           		<div class="thumbnail"><a href="${loginLinks[association.name()?lower_case + "Url"]?html}"><@common.authSource association /></a></div>
           	</#list>
			</div>
			
            <p>
				${messages.get('second_paragraph', messages.get(authentication.source))}
			</p>
        </div>
    </div>
	<#else>
    <div class="row">
        <div class="col-md-8">
            <h1>${messages.get('title')}</h1>

            <p>${messages.get('account_deleted', authentication.user.email)}</p>
        </div>
    </div>
	</#if>
</div>

</body>
</html>
