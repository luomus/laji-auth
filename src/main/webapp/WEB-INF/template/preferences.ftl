<#import "common.ftl" as common>
<!doctype html>
<html lang="${language!""}">
<@common.head title="Asetukset"></@common.head>
<body>
<@common.navbar></@common.navbar>
<div class="container">
    <h1>Asetukset</h1>
<#if saved>
    <h3 class="text-success">Asetukset tallennettiin</h3>
</#if>
    <form method="post">
        <table class="table">
            <thead>
            <tr>
                <th>Avain</th>
                <th>Arvo</th>
            </tr>
            </thead>
            <tbody>
            <#list preferences?keys as preference>
            <tr>
                <td style="width: 25%"><label>${preference?html}</label></td>
                <td style="width: 75%">
                    <input type="text" name="${preference?html}" value="${preferences[preference]?html}" class="form-control" />
                </td>
            </tr>
            </#list>
            </tbody>
        </table>
        <button class="btn btn-lg btn-primary">Tallenna asetukset</button>
    </form>
</div>
</body>
</html>