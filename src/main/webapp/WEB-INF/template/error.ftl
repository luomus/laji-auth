<#import "common.ftl" as common>
<!doctype html>
<html lang="${language!""}">
<head>
</head>
<@common.head title=messages.get('title')></@common.head>
</head>
<body>
<@common.navbar></@common.navbar>
<div class="container">
    <h3 class="text-danger">${messages.get('title')}</h3>

    <p>${messages.get('body')}</p>
<#if (model)??>
    <#if (model.message)??><i>${model.message?html}</i></#if>
</#if>
</div>
</body>
</html>
