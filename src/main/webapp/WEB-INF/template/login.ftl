<#import "common.ftl" as common>
<!DOCTYPE html>
<html lang="${language!""}">
<#if associated?has_content>
<@common.head title=messages.get('associate')></@common.head>
<#else>
<@common.head title=messages.get('title')></@common.head>
</#if>

<body>
<@common.navbar></@common.navbar>
<div class="container">
<div class="row">

	<div class="col-md-9">
    
    	<#if associated?has_content>
    		<div class="row">
    			<div class="col-md-8">
    				<h3>${messages.get('associate')}</h3>
    				<p>${messages.get('choose_new_association')}</p>
    			</div> 
    		</div>
    	</#if>
    
		<div class="row source-thumbs">

			<div class="col-md-4">
				<#if !associated?has_content>
					<h3>${messages.get('local_source')}</h3>
				</#if>
				<#if associated?seq_contains("LOCAL")>
					<div class="thumbnail disabled" style="margin-bottom: 0px;">
						<div class="login">
							<@common.authSource "LOCAL" />
						</div>
					</div>
				<#else>
					<div class="thumbnail" style="margin-bottom: 0px;">
						<div class="login">
							<a id="local-login" href="${localUrl?html}">
								<@common.authSource "LOCAL" />
							</a>
						</div>
					</div>
					<a id="registerLink" href="${localRegisterUri?html}">
						<button id="register" class="btn btn-lg btn-success" style="margin-top:20px; margin-bottom: 10px; width: 100%">${messages.get('register')}</button>
					</a>
				</#if>
			</div>
		
			<div class="col-md-8">
				<#if !associated?has_content>
					<h3>${messages.get('other_sources')}</h3>
				</#if>
				
				<div class="row source-thumbs">
					<div class="col-md-6"><@source "HAKA" hakaUrl /></div>
					<div class="col-md-6"><@source "VIRTU" virtuUrl /></div>
				</div>
				
				<div class="row source-thumbs">
					<div class="col-md-6"><@source "GOOGLE" googleUrl /></div>
					<div class="col-md-6"><@source "FACEBOOK" facebookUrl /></div>
				</div>
				
				<div class="row source-thumbs">
					<div class="col-md-6"><@source "INATURALIST" inaturalistUrl /></div>
					<div class="col-md-6"><@source "APPLE" appleUrl /></div>
				</div>
				
				<div class="row source-thumbs">
					<div class="col-md-6"><@source "OMARIISTA" omariistaUrl /></div>
					<div class="col-md-6"></div>
				</div>
            	
			</div>	
		</div>
		
		<#if offerPermanent>
			<div class="row">
				<div class="col-md-offset-1 col-md-10">
					<div class="panel panel-info">
						<div class="panel-heading">
							${messages.get('permanent_title')}
						</div>
						<div class="panel-body">
							<input type="checkbox" id="permanent" />
							${messages.get('permanent_body')}
						</div>
					</div>
				</div>
			</div>
		</#if>

		<#if !associated?has_content>
			<div class="row">
				<div class="col-md-offset-1 col-md-10">
					<div class="panel panel-info">
						<div class="panel-heading">
							${messages.get('help_title')}
						</div>
						<div class="panel-body">
							<p>${messages.get('help_body_1')}</p>
							<p>${messages.get('help_body_2')}</p>
							<p>${messages.get('help_body_3')}</p>
						</div>
					</div>
				</div>
			</div>
		</#if>

		<#if associated?has_content>
			<div class="row">
				<div class="col-md-offset-4 col-md-6">
					<a href="self">
						<button class="btn btn-lg btn-success" style="margin:20px;">${messages.get('back_to_self')}</button>
					</a>
				</div>
			</div>
		</#if>
					
	</div>

	<div class="col-md-3" style="background-color: #ECF0F1">
		<h5>${messages.get('system_name')}</h5>
		<h3>${systemName?html}</h3>
		<br/>
		<#if helpUrl??><p><a target="_blank" href="${helpUrl}">${messages.get('help')}</a></p></#if>
		<p><a target="_blank" href="${privacyPolicyUrl}">${messages.get('privacy_policy')}</a></p>
	</div>
	
	
</div>
</div>

<#macro source source url>
	<#if associated?seq_contains(source)>
		<div class="thumbnail disabled">
			<div class="login">
				<@common.authSource source />
			</div>
		</div>
	<#else>
		<div class="thumbnail">
			<div class="login">
				<a id="${source?lower_case}-login" href="${url?html}">
					<@common.authSource source />
				</a>
			</div>
		</div>
	</#if>
</#macro>

<script src="${baseUri}webjars/jquery/1.11.3/jquery.min.js" type="application/javascript"></script>

<script>
    (function () {
    
    	$("#permanent").on('click', function() {
    		var value = $(this).prop('checked');
    		 $(".login a, #registerLink").attr('href', function(i,a) {
    		 	if (a.indexOf("permanent") !== -1) {
    				a = a.replace("&permanent=false", "").replace("&permanent=true", "");
    			}
    			if (value)
    				return a + "&permanent="+value;
    			return a;
  			});
    	});
    	
    })();
</script>

</body>
</html>
