<#import "common.ftl" as common>
<!DOCTYPE html>
<html lang="${language!""}">
<@common.head title=messages.get('title')></@common.head>
<body>
<@common.navbar></@common.navbar>

<div class="container">
    <div class="row">
        <div class="col-md-8">
        
			<#if emailConfirmed>
				<h3 class="text-success">${messages.get('emailConfirmed_success')}</h3>
				<p>${messages.get('emailConfirmed_body')}</p>
			<#else>
				<h3>${messages.get('title')} ...</h3>
			</#if>

			<#if !autosubmit>
        		<div class="panel panel-default" style="margin-top: 10px">
                	<div class="panel-heading">
                		${messages.get('system_name')} 
                		<h4>${system_name?html}</h4>
                	</div>
                	<div class="panel-body">
                		${messages.get('as_user')}
                		<h4>${person_name}</h4>
                		${person_id}
                	</div>
                	<div class="panel-footer">
						<a class="btn btn-primary btn-lg" href="#" onclick="submitForm(); return false;">${messages.get('resume')}</a>
					</div>
	            </div>
          	</#if>
        </div>
    </div>
</div>

<noscript>${messages.get('javascript')}</noscript>

<form action="${nextUrl?html}" method="${redirectMethod?html}" id="token-form">
    <input type="hidden" name="token" id="token-input" value="${token?html}">
    <input type="hidden" name="next" id="next-input" value="${(next!"")?html}">
</form>

<form action="login/changeUser" method="POST" id="changeUser-form">
    <input type="hidden" name="token" id="token-input" value="${token?html}">
</form>

<script type="application/javascript">
	<#if autosubmit>
    (function () {
        submitForm();
    })();
    </#if>
	function submitForm() {
    	document.getElementById('token-form').submit();
    }
    function submitChangeUserForm() {
    	document.getElementById('changeUser-form').submit();
    }
</script>

</body>
</html>
