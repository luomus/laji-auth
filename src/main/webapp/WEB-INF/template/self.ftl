<#import "common.ftl" as common>
<!doctype html>
<html lang="${language!""}">
<@common.head title=messages.get('your_info')></@common.head>
<body>
<@common.navbar></@common.navbar>

<div class="container">
    <div class="row">
        <div class="col-md-8" style="margin-top: 20px">
            <#if parameters['newSource']??>
				<h3 class="text-success" style="margin-top: 0">${messages.get('new_source', messages.get(parameters['newSource'][0]))}</h3>
            </#if>
            
            <#if warning??>
            <div class="row">
				<div class="col-md-8">
            		<h3 class="text-warning">${messages.get(warning + '_title')}</h3>
            		<p>${messages.get(warning + '_body')}</p>
        		</div>
    		</div>
    		</#if>
    
    		<#if info??>
            <div class="row">
				<div class="col-md-8">
            		<h3 class="text-info">${messages.get(info + '_title')}</h3>
            		<p>${messages.get(info + '_body')}</p>
        		</div>
    		</div>
    		</#if>
    		
    		<#if saved??>
            <div class="row">
				<div class="col-md-8">
            		<h3 class="text-success">${messages.get('changes_saved')}</h3>
            		<#if emailChanged??>
            			<p>${messages.get('email_change')}</p>
            		</#if>
					<#if permanentDeleted??>
						<p>${messages.get('permanent_deleted')}</p>
					</#if>
       			</div>
    		</div>
    		</#if>
    		    		
			<#if linkedSystems??>
			<div class="row">
				<div class="col-md-8">
            		<h3 class="text-success">${messages.get('changes_saved')}</h3>
            		<div class="associations">
            			<#if linkedUserIdCount == 1>
           					<p>${messages.get('additional_user_id_success_single')}:</p>
           				<#else>
           					<p>${messages.get('additional_user_id_success_many')}:</p>
           				</#if>
            			<#list linkedSystems as system>
            				<div class=" thumbnail" style="margin-bottom: 0px;">
            					<@common.authSource system />
            				</div>
            			</#list>
            			<p>${messages.get('additional_user_id_success_check_bellow')}</p>
            		</div>
       			</div>
    		</div>
			</#if>
			
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3>${messages.get('your_info')}</h3>
                </div>
                <table class="table">
                    <tr>
                        <th>${messages.get('user_qname')}</th>
                        <td>${authInfo.user.qname.get()?html} &nbsp; &mdash; &nbsp; <a target="_blank" href="http://tun.fi/${authInfo.user.qname.get()?html}">http://tun.fi/${authInfo.user.qname.get()?html}</a></td>
                    </tr>
                    <tr>
                    	<th>${messages.get('firstJoined')}</th>
                    	<td>
                    		<#if authInfo.user.firstJoined.present>
                    			${authInfo.user.firstJoined.get()?datetime("yyyy-MM-dd")?string(messages.get('date_format'))}
							<#else>
								${messages.get('missing_info')}
                    		</#if>
                    	</td>
                    </tr>
                    <tr>
                        <th>${messages.get('your_name')}<br />${messages.get('your_group')}</th>
                        <td>
							${authInfo.user.name?html} <#if authInfo.user.group.present>(${authInfo.user.group.get()?html})</#if>
							<a class="modify" href="#">[${messages.get('change')}]</a>
							<br />
							<#if authInfo.user.group.present>
                    			${authInfo.user.group.get()?html} <a class="modify" href="#">[${messages.get('change')}]</a>
                    		<#else>
                    			<a href="#" class="modify">[${messages.get('add')}]</a>
                    		</#if>
							
							<div class="hidden-panel panel panel-default" style="margin-top: 10px">
								<form action="self/name" method="POST" id="name-change-form">
    	                			<div class="panel-body">
        	                			<div class="form-group">
            	                			<label>${messages.get('preferredName')}</label>
    		    	                    	<input type="text" name="preferredName" id="preferredName" class="form-control"
    		    	                    			value="${authInfo.user.preferredName.or("")?html}" 
                    	               		   		data-parsley-required="true"
                        	                   		data-parsley-minlength="2">
                            			</div>
                            			<div class="form-group">
            	                			<label>${messages.get('inheritedName')}</label>
    		    	                    	<input type="text" name="inheritedName" id="inheritedName" class="form-control"
    		    	                    			value="${authInfo.user.inheritedName.or("")?html}" 
                    	               		   		data-parsley-required="true"
                        	                   		data-parsley-minlength="2">
                            			</div>
                            			<div class="form-group">
            	                			<label>${messages.get('group')}</label>
    		    	                    	<input type="text" name="group" id="group" class="form-control"
    		    	                    			value="${authInfo.user.group.or("")?html}" 
                    	               		   		data-parsley-required="false"
                        	                   		data-parsley-minlength="3">
                            			</div>
                            			<div class="form-group">
                            				<label>${messages.get('name_for_friends')}</label>
                            				<p id="name-for-friends"></p>
                            			</div>
                            			<div class="form-group">
                            				<label>${messages.get('name_for_observations')}</label>
                            				<p id="name-for-observations"></p>
                            			</div>
                             		</div>
									<div class="panel-footer">
										<button class="btn btn-primary btn-lg submit">${messages.get('save')}</button>
                    				</div>
                				</form>
                			</div>
                        </td>
                    </tr>
                    <tr>
                        <th>${messages.get('your_email')}</th>
                        <td>
							${authInfo.user.email?html} <a class="modify"  href="#">[${messages.get('change')}]</a>
							<div class="hidden-panel panel panel-default" style="margin-top: 10px">
								<form action="self/email" method="POST" id="email-change-form">
    	                			<div class="panel-body">
        	                			<div class="form-group">
            	                			<label>${messages.get('new_email')}</label>
    		    	                    	<div class="input-group">
                               					<span class="input-group-addon">@</span>
                                				<input type="text" name="email" class="form-control"
                                       					data-parsley-required="true"
                                       					data-parsley-type="email"
                                       					placeholder="${messages.get('email_placeholder')}"/>
                            				</div>
                            			</div>
                             		</div>
									<div class="panel-footer">
			               				<p style="margin-bottom: 1em">${messages.get('email_change')}</p>
										<button class="btn btn-primary btn-lg submit">${messages.get('save')}</button>
                    				</div>
                				</form>
                			</div>
                        </td>
                    </tr>
                    <#if authInfo.user.previousEmailAddresses?has_content>
                    	<tr>
                    		<th>${messages.get('previousEmailAddresses')}</th>
                    		<td>
                    			<#list authInfo.user.previousEmailAddresses as email>
                    				${ email?html}
									<#if email_has_next><br></#if>
								</#list>
                    		</td>
                    	</tr>
                    </#if>
                    <tr>
                    	<th>${messages.get('yearOfBirth')}</th>
                    	<td>
                    		<#if authInfo.user.yearOfBirth.present>
                    			${authInfo.user.yearOfBirth.get()?c} <a class="modify" href="#">[${messages.get('change')}]</a>
                    		<#else>
                    			<a href="#" class="modify">[${messages.get('add')}]</a>
                    		</#if>
							<div class="hidden-panel panel panel-default" style="margin-top: 10px">
								<form action="self/yearOfBirth" method="POST" id="yearOfBirth-change-form">
    	                			<div class="panel-body">
        	                			<div class="form-group">
            	                			<label>${messages.get('yearOfBirth')}</label>
    		    	                    	<input type="text" name="yearOfBirth" class="form-control"
    		    	                    			value="${(authInfo.user.yearOfBirth.orNull()?c)!""}" 
                    	               		   		data-parsley-required="true"
                    	               		   		data-parsley-type="integer"
                        	                   		data-parsley-min="1900"
                        	                   		data-parsley-max="${.now?string('yyyy')}">
                            			</div>
                             		</div>
									<div class="panel-footer">
										<button class="btn btn-primary btn-lg submit">${messages.get('save')}</button>
                    				</div>
                				</form>
                			</div>
                    	</td>
                    </tr>
                    <#if organisationDescriptions?has_content>
                    <tr>
                    	<th>${messages.get('organisations')}</th>
                    	<td>
							<#list (organisationDescriptions?keys) as organisationId>
                    			${organisationDescriptions[organisationId]!organisationId}
								&nbsp; &mdash; &nbsp; <a target="_blank" href="http://tun.fi/${organisationId?html}">http://tun.fi/${organisationId?html}</a>
								<#if organisationId_has_next><br></#if>
							</#list>
						</td>
                    </tr>
                    </#if>
                    <#if adminOfOrganisationDescriptions?has_content>
                    <tr>
                    	<th>${messages.get('adminOfOrganisations')}</th>
                    	<td>
							<#list (adminOfOrganisationDescriptions?keys) as organisationId>
                    			${adminOfOrganisationDescriptions[organisationId]!organisationId}
								&nbsp; &mdash; &nbsp; <a target="_blank" href="http://tun.fi/${organisationId?html}">http://tun.fi/${organisationId?html}</a>
								<#if organisationId_has_next><br></#if>
							</#list>
						</td>
                    </tr>
                    </#if>
                    <#if authInfo.user.roles?has_content>
                    <tr>
                    	<th>${messages.get('roles')}</th>
                    	<td><#list authInfo.user.roles as role>${roleDescriptions[role]!role}<#if role_has_next><br></#if></#list></td>
                    </tr>
                    </#if>
                    <#if securePortalRoleExpires?has_content>
                    	<tr>
                    		<th>${messages.get('securePortalRoleExpires')}</th>
                    		<td>
                    			${securePortalRoleExpires?datetime("yyyy-MM-dd")?string(messages.get('date_format'))}
	                    	</td>
    	                </tr>
               		</#if>
					<tr>
						<th>${messages.get('defaultLanguage')}</th>
						<td>
							<#if authInfo.user.defaultLanguage.present>
								${messages.get('language_'+authInfo.user.defaultLanguage.get())}
							<#else>
								${messages.get('language_fi')}
							</#if>
							<a class="modify" href="#">[${messages.get('change')}]</a>
							<div class="hidden-panel panel panel-default" style="margin-top: 10px">
								<form action="self/defaultLanguage" method="POST" id="defaultLanguage-change-form">
    	                			<div class="panel-body">
        	                			<div class="form-group">
            	                			<label>${messages.get('defaultLanguage')}</label>
    		    	                    	<select name="defaultLanguage" class="form-control">
                    	               		   		<option value="" selected="selected"></option>
                    	               		   		<option value="fi">${messages.get('language_fi')}</option>
                    	               		   		<option value="sv">${messages.get('language_sv')}</option>
                    	               		   		<option value="en">${messages.get('language_en')}</option>
                    	               		</select>
                            			</div>
                             		</div>
									<div class="panel-footer">
										<button class="btn btn-primary btn-lg submit">${messages.get('save')}</button>
                    				</div>
                				</form>
                			</div>
						</td>
					</tr>
					<tr>
                    	<th>${messages.get('address')}</th>
                    	<td>
                    		<#if authInfo.user.address.present>
                    			<pre>${authInfo.user.address.get()}</pre> <a class="modify" href="#">[${messages.get('change')}]</a>
                    		<#else>
                    			<a href="#" class="modify">[${messages.get('add')}]</a>
                    		</#if>
							<div class="hidden-panel panel panel-default" style="margin-top: 10px">
								<form action="self/address" method="POST" id="address-change-form">
    	                			<div class="panel-body">
        	                			<div class="form-group">
            	                			<label>${messages.get('address')}</label>
    		    	                    	<input name="address" class="form-control"" />
                            			</div>
                            			<div class="form-group">
            	                			<label>${messages.get('address_postcode')}</label>
    		    	                    	<input name="postCode" class="form-control"" />
                            			</div>
                            			<div class="form-group">
            	                			<label>${messages.get('address_city')}</label>
    		    	                    	<input name="city" class="form-control"" />
                            			</div>
                             		</div>
									<div class="panel-footer">
										<button class="btn btn-primary btn-lg submit">${messages.get('save')}</button>
                    				</div>
                				</form>
                			</div>
                    	</td>
                    </tr>
                </table>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3>${messages.get('your_associations')}</h3>
                        </div>
                        <table class="table associations">
                        	<tr>
                        		<th>${messages.get('auth_source')}</th>
                        		<th>${messages.get('auth_source_user_id')}</th>
                        	</tr>
                        	<#list (associations?keys) as source>
							<tr>
								<td>		
    	                        	<div class=" thumbnail" style="margin-bottom: 0px;">
											<@common.authSource source />
									</div>
								</td>
								<td>
									${associations[source]?html}
									<#if source == "LOCAL">
										<a href="#" class="modify"><button class="btn btn-warning btn-sm submit">${messages.get('change_password')}</button></a>
										
										<div id="change-local-password" class="hidden-panel" title="${messages.get('change_password')}">
    										<form action="self/changeLocalPassword" method="POST" id="change-local-password-form">
        										<div class="form-group">
        											<label>${messages.get('change_password_current')}</label>
													<input type="password" name="passwordCurrent" class="form-control" data-parsley-required="true"/>
												</div>
												<div class="form-group">
													<label>${messages.get('change_password_new')}</label>
													<input type="password" id="password-field" name="password" class="form-control"
														data-parsley-required="true"
                										data-parsley-error-message="${messages.get('password_error')}"
                										data-parsley-pattern="^(?=.*[A-Z])(?=.*[a-z])(?=.*((\W)|(\d))).{8,}$"
                										data-parsley-minlength="10" />
												</div>
        										<div class="form-group">
        											<label>${messages.get('password_again')}</label>
                									<input type="password" name="passwordAgain" class="form-control" data-parsley-equalto="#password-field"/>
        										</div>
        										<div style="background-color: #ECF0F1; margin-top: 1em;">
        											<h4>${messages.get('password_requirements_title')}</h4>
        											<ul>
        												<li>${messages.get('password_requirements_list_1')}</li>
        												<li>${messages.get('password_requirements_list_2')}</li>
        												<li>${messages.get('password_requirements_list_3')}</li>
        											</ul>
												</div>
												<button class="btn btn-primary btn-lg submit">${messages.get('reset')}</button>
   											</form>  
										</div>
									</#if>
								</td>
							</tr>
                        	</#list>
                    	</table>
                    	<div class="panel-footer">
            				<p>${messages.get('associate_new_body')}</p>
                    		<a href="${associateUri?html}">
                				<button class="btn btn-lg btn-primary">${messages.get('associate')}</button>
            				</a>
            			</div>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3>${messages.get('your_additional_user_ids')}</h3>
                        </div>
                        <#if authInfo.user.additionalUserIds?has_content>
                        <table class="table associations">
                        	<tr>
                        		<th>${messages.get('additional_system')}</th>
                        		<th>${messages.get('auth_source_user_id')}</th>
                        	</tr>
                        	<#list authInfo.user.additionalUserIds?keys as system>
                        		<tr>
                        			<td>
                        				<@otherSystem system />
                        			</td>
                        			<td>
                        				<#list authInfo.user.additionalUserIds[system] as userid>
                        					${userid}<#if userid_has_next><br></#if>
                        				</#list>
                        			</td>
                        		</tr>
                        	</#list>
                    	</table>
                    	<#else>
                    		<div class="panel-body">${messages.get('no_additional_user_ids')}</div>
                    	</#if>
                    	<div class="panel-footer">
                    		<p>${messages.get('associate_others_desc')}</p>
                    		<button class="associateOtherButton btn btn-lg btn-primary">${messages.get('associate_other')}</button>
                    	</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-4" style="background-color: #ECF0F1; padding-top: 2em; padding-bottom: 2em;">
        
        	<#if helpUrl??><p><a target="_blank" href="${helpUrl}">${messages.get('help')}</a></p></#if>
			<p><a target="_blank" href="${privacyPolicyUrl}">${messages.get('privacy_policy')}</a></p>

            <h3>${messages.get('associate_new_title')}</h3>

            <p>${messages.get('associate_new_body')}</p>

            <a href="${associateUri?html}">
                <button class="btn btn-lg btn-primary" style="margin-bottom: 20px">${messages.get('associate')}</button>
            </a>

 			<h3>${messages.get('associate_others')}</h4>
 			<p>${messages.get('associate_others_desc')}</p>
 			<ul>
 				<#list additionalSystems as system>
 					<li>${messages.get(system)}</li>
 				</#list>
 			</ul>
			<button class="associateOtherButton btn btn-lg btn-primary" id="associateOther" style="margin-bottom: 20px">${messages.get('associate_other')}</button>
			
			<h3>${messages.get('permanent_logins')}</h3>
			<p>${messages.get('permanent_logins_info')}</p>
			<ul>
				<#if permanentLogins?has_content>
					<#list permanentLogins as l>
						<li>
							<p>
								${permanentLoginSystems[l.event.target]} -
								<#if l.lastUsed??>
									${messages.get('last_logged_in')} ${l.lastUsed?string(messages.get('date_format'))}
								<#else>
									${messages.get('not_yet_logged_in')}
								</#if>
							</p>
							<p style="font-size:50%">${l.token?html}</p> 
							<form action="self/removePermanentLogin" method="POST">
								<input type="hidden" name="token" value="${l.token?html}"/>
								<button class="btn btn-xs btn-danger">${messages.get('delete')}</button>
							</form>
						</li>
					</#list>
				<#else>
					<li>${messages.get('no_permanent_logins')}</li>
				</#if>
			</ul>
			
			<h3>${messages.get('delete_account')}</h3>
			<button class="btn btn-lg btn-danger" id="deleteAccount">${messages.get('delete')}</button>

        </div>
    </div>
</div>

<div id="additional-user-ids-add" title="${messages.get('your_additional_user_ids')}">
	<table class="table associations">
       <#list additionalSystems as system>
			<tr>
				<td>
					<@otherSystem system />
				</td>
				<td>
					<#if system == "HYONTEISTIETOKANTA">
						<p>${messages.get('associate_contact_admin')}</p>
					<#else>
						<a href="#" class="modify"><button class="btn btn-primary btn-lg submit">${messages.get('associate_this_other')}</button></a>
						<div class="hidden-panel">
							<form action="self/additionalUserId" method="POST">
								<input type="hidden" name="additional-system" value="${system}" /> 
								<div class="form-group">
									<label>${messages.get('username')}</label>
									<input type="text" name="additional-system-username" class="form-control" />
								</div>
								<div class="form-group">
									<label>${messages.get('password')}</label>
									<input type="password" name="additional-system-password" class="form-control" />
								</div>
								<p>${messages.get('additional_password_policy')}</p>
								<button class="btn btn-primary btn-lg submit">${messages.get('submit')}</button>
							</form>
						</div>
				</#if>
				</td>
			</tr>
		</#list>
	</table>
</div>

<div id="delete-account-dialog" title="${messages.get('delete_account')}">
	<form action="self/deleteAccount" method="POST">
		<p>${messages.get('delete_account_confirmation')}</p>
		<input type="text" name="confirmation" class="form-control" />
		<p>${authInfo.user.name?html}</p>
		<button class="btn btn-lg btn-danger">${messages.get('delete')}</button>
	</form>
</div>

<script src="${baseUri}csrfguard"></script>
<script src="${baseUri}webjars/jquery/1.11.3/jquery.min.js" type="application/javascript"></script>
<script src="${baseUri}webjars/jquery-ui/1.11.3/jquery-ui.min.js" type="application/javascript"></script>
<script src="${baseUri}webjars/parsleyjs/2.1.2/dist/parsley.min.js" type="application/javascript"></script>
<script src="${baseUri}webjars/parsleyjs/2.1.2/src/i18n/${language}.js" type="application/javascript"></script>
<script>
    (function () {
    
    	// CSRF guard causes the page to 'blink'; the following hides the page and then shows it again so that the blink is not visible
    	document.body.style.display ='none';
    	setTimeout("document.body.style.display ='block'", 200);
    	
    	updateNameDisplay();
    	
        var getFormGroup = function (parsleyField) {
            return parsleyField.$element.closest('.form-group');
        };

        $('#yearOfBirth-change-form, #name-change-form, #email-change-form, #defaultLanguage-change-form, #change-local-password-form').parsley({
            successClass: 'has-success',
            errorClass: 'has-error',
            classHandler: getFormGroup,
            errorsContainer: getFormGroup,
            errorsWrapper: '<span class="help-inline"></span>',
            errorTemplate: '<span></span>'
        });
        
        $("a.modify").on('click', function() {
        	$(this).parent().find('.hidden-panel').removeClass('hidden-panel');
        	$(this).remove();
        	return false;
        });
        
        $("#name-change-form").find(":input").on('keyup', function() {
        	updateNameDisplay();
        });
        
         $("#name-change-form").find(":input").on('change', function() {
        	updateNameDisplay();
        });
        
        $("#additional-user-ids-add").dialog({ 
        	autoOpen: false,
        	resizable: false,
        	modal: true,
        	height: 'auto',
        	width: $(window).width()-180,
        	closeText: "${messages.get('close')}",
        	buttons: [{ text: "${messages.get('close')}", icon: "ui-icon-close", click: function() { $(this).dialog("close"); } }]
        });
        
        $(".associateOtherButton").on('click', function() {
        	$("#additional-user-ids-add").find("input[type!='hidden']").val('');
        	$("#additional-user-ids-add").dialog("open");
        });
        
        $("#delete-account-dialog").dialog({ 
        	autoOpen: false,
        	resizable: false,
        	modal: true,
        	height: 'auto',
        	width: 400,
        	closeText: "${messages.get('close')}",
        	buttons: [{ text: "${messages.get('close')}", icon: "ui-icon-close", click: function() { $(this).dialog("close"); } }]
        });
        
        $("#deleteAccount").on('click', function() {
        	$("#delete-account-dialog").dialog("open");
        });
        
        // prevent form submit using enter
        $(window).keydown(function(event){
    		if (event.keyCode == 13) {
      			event.preventDefault();
      			return false;
    		}
  		});
  		
    })();
    
    function updateNameDisplay() {
    	var preferredName = $("#preferredName").val();
    	var inheritedName = $("#inheritedName").val();
    	var group = $("#group").val();
    	var friendsName = preferredName + ' ' + inheritedName;
    	if (group) {
    		friendsName += ' (' + group + ')';
    	}
    	$("#name-for-friends").html(friendsName);
    	$("#name-for-observations").html(inheritedName + ', ' + preferredName);
    }
    
</script>

<#macro otherSystem name> 
	<div class=" thumbnail" style="margin-bottom: 0px;">
		<@common.authSource name />
	</div>
	<p>${messages.get(name+'_desc')}</p>
</#macro>


</body>
</html>
