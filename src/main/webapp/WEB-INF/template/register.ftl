<#import "common.ftl" as common>
<!doctype html>
<html lang="${language!""}">
<@common.head title=messages.get('title')></@common.head>
<body>
<@common.navbar></@common.navbar>
<div class="container">

    <div class="row">
        <div class="col-md-8">
            <h1>${messages.get('welcome')}</h1>

            <p>${messages.get('first_paragraph', messages.get(authentication.source), authentication.user.authSourceId)}</p>

            <p>${messages.get('second_paragraph')}</p>
        </div>
    </div>

    <div class="row" style="margin-top: 10px">
        <div class="col-md-6">
            <div class="panel panel-default">
                <form method="POST" id="approval-form">
                    <div class="panel-body">
                        <div class="form-group">
                            <label>${messages.get('your_email')}</label>
                            <input id="email-input" type="email" name="email" class="form-control"
                                   data-parsley-required="true"
                                   data-parsley-type="email"
                                   placeholder="${messages.get('email_placeholder')}" 
                                   <#if (authentication.user.email??)>value="${authentication.user.email?html}"</#if> />
                        </div>
						<div class="form-group">
            	        	<label>${messages.get('preferredName')}</label>
							<input type="text" name="preferredName" id="preferredName" class="form-control"
								    value="${authentication.user.preferredName.or("")?html}" 
								    data-parsley-required="true"
								    data-parsley-minlength="2">
						</div>
						<div class="form-group">
							<label>${messages.get('inheritedName')}</label>
							<input type="text" name="inheritedName" id="inheritedName" class="form-control"
								   value="${authentication.user.inheritedName.or("")?html}" 
								   data-parsley-required="true"
								   data-parsley-minlength="2">
						</div>
						<div class="form-group">
							<label>${messages.get('group')}</label>
							<input type="text" name="group" id="group" class="form-control"
								   value="${authentication.user.group.or("")?html}" 
								   data-parsley-required="false"
								   data-parsley-minlength="3">
						</div>
                        <input type="hidden" name="token" value="${token?html}"/>
                    </div>

                    <div class="panel-footer">
                        <button id="approval-submit" class="btn btn-lg btn-primary submit">${messages.get('send')}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>
<@common.validateForm selector='#approval-form'></@common.validateForm>
</body>
</html>
