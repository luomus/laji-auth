<#import "common.ftl" as common>
<!doctype html>
<html lang="${language!""}">
<head>
</head>
<@common.head title=messages.get(key + '_title')></@common.head>
</head>
<body>
<@common.navbar></@common.navbar>
<div class="container" id="${key?html}-container">
    <div class="row">
        <div class="col-md-8">
            <h3 class="text-${headingClass?html}">${messages.get(key + '_title')}</h3>
            <p>${messages.get(key + '_body')}</p>
        </div>
    </div>
</div>
</body>
</html>
