package fi.luomus.lajiauth;

import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

import org.apache.commons.codec.binary.Base64;
import org.glassfish.jersey.server.mvc.Viewable;

import fi.luomus.lajiauth.model.BaseErrorEntity;
import fi.luomus.lajiauth.service.LajiAuthClient;
import fi.luomus.lajiauth.service.SystemService;
import fi.luomus.utils.exceptions.ApiException;

public class Utils {

	private Utils() {
	}

	public static LajiAuthClient getLajiAuthClient(String targetSystemId, SystemService systemService) {
		return new LajiAuthClient(targetSystemId, systemService.getLajiAuthBaseURI());
	}

	public static String generateToken() {
		String tokenSource = UUID.randomUUID().toString();
		String token = Base64.encodeBase64URLSafeString(tokenSource.getBytes(Charset.forName("UTF-8")));
		return token;
	}

	public static void error(int status, BaseErrorEntity entity) throws WebApplicationException {
		throw new WebApplicationException(Response.status(status).entity(entity).build());
	}

	public static void error(ApiException ax) throws WebApplicationException {
		final String source = ax.getSource();
		final String message = ax.getMessage();
		error(502, new BaseErrorEntity("API_ERROR") {
			@SuppressWarnings("unused")
			public String getSource() {
				return source;
			}

			@SuppressWarnings("unused")
			public String getMessage() {
				return message;
			}
		});
	}

	public static void error(int status, String code) throws WebApplicationException {
		error(status, new BaseErrorEntity(code));
	}

	public static void error(Response.Status status, String code) throws WebApplicationException {
		error(status.getStatusCode(), new BaseErrorEntity(code));
	}

	public static void notFound() throws WebApplicationException {
		error(404, "NOT_FOUND");
	}

	public static void invalidParameters() throws WebApplicationException {
		error(400, "INVALID_PARAMETERS");
	}

	public static Viewable generic(String key, String headingClass) {
		Map<String, String> model = new HashMap<>();
		model.put("key", key);
		model.put("headingClass", headingClass);
		return new Viewable("/generic", model);

	}

	public static String getPrivacyPolicyUrl(String language) {
		if ("fi".equals(language)) {
			return "https://laji.fi/tietosuojaseloste";
		}
		return "https://laji.fi/privacypolicy";
	}

	public static String getHelpUrl(String language) {
		if ("fi".equals(language)) {
			return "https://laji.fi/about/2189";
		}
		return null;
	}

}
