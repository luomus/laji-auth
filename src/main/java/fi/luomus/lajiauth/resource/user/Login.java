package fi.luomus.lajiauth.resource.user;

import static fi.luomus.lajiauth.util.ErrorPageWebApplicationException.wrap;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.HttpMethod;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.glassfish.jersey.server.mvc.Viewable;

import fi.luomus.lajiauth.model.AuthenticationEvent;
import fi.luomus.lajiauth.model.Constants;
import fi.luomus.lajiauth.resource.user.authsources.Local;
import fi.luomus.lajiauth.service.LoginLinksService;
import fi.luomus.lajiauth.service.UserAuthenticatedService;
import fi.luomus.lajiauth.util.ErrorPageWebApplicationException;
import fi.luomus.utils.exceptions.ApiException;

@Path("login")
public class Login {

	@Context
	private UriInfo uriInfo;
	@Inject
	private HttpServletRequest request;
	@Inject
	private UserAuthenticatedService userAuthenticatedService;
	@Inject
	LoginLinksService loginLinksService;

	@GET
	@Produces(MediaType.TEXT_HTML)
	public Response login(
			@QueryParam(Constants.TARGET_SYSTEM_PARAMETER) String targetSystemId,
			@QueryParam(Constants.NEXT_PARAMETER) String next,
			@QueryParam(Constants.REDIRECT_METHOD) @DefaultValue(HttpMethod.POST) String redirectMethod,
			@QueryParam(Constants.OFFER_PERMANENT) @DefaultValue("false") boolean offerPermanent,
			@QueryParam("associated") List<String> associated,
			@QueryParam("OWASP-CSRFTOKEN") String csrfToken) throws ApiException, ErrorPageWebApplicationException {

		if (targetSystemId == null || next == null) {
			throw wrap(new WebApplicationException("MISSING_PARAMETER", Response.Status.BAD_REQUEST));
		}

		if (csrfToken != null) {
			next += "?OWASP-CSRFTOKEN=" + csrfToken;
		}

		Map<String, Object> model = loginLinksService.initCommonLoginModel(targetSystemId);

		model.putAll(loginLinksService.allSourceURIs(uriInfo, targetSystemId, next, redirectMethod));
		model.put("localRegisterUri", Local.getRegisterLinkURI(uriInfo, targetSystemId, next, redirectMethod, false).build());
		model.put("associated", associated);
		model.put("offerPermanent", offerPermanent);

		return Response.ok(new Viewable("/login", model)).build();
	}

	@POST
	@Path("changeUser")
	@Produces(MediaType.TEXT_HTML)
	public Response changeUser(@FormParam(Approval.TOKEN) String token) throws ApiException, ErrorPageWebApplicationException {
		HttpSession session = request.getSession(false);
		if (session != null) session.invalidate();
		AuthenticationEvent event = userAuthenticatedService.getAuthenticationInfoOrFail(token);
		return login(event.getTarget(), event.getNext(), event.getRedirectMethod(), false, new ArrayList<String>(), null);
	}

}
