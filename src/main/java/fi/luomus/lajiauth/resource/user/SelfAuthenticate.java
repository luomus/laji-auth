package fi.luomus.lajiauth.resource.user;

import java.net.URI;
import java.net.URISyntaxException;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.context.ApplicationContext;

import fi.luomus.lajiauth.model.AuthenticationEvent;
import fi.luomus.lajiauth.service.LajiAuthClient;
import fi.luomus.lajiauth.service.LanguageService;
import fi.luomus.lajiauth.service.SessionService;
import fi.luomus.lajiauth.service.SystemService;
import fi.luomus.utils.exceptions.ApiException;

@Path(SelfAuthenticate.AUTHENTICATE)
public class SelfAuthenticate {

	public static final String AUTHENTICATE = "authenticate";

	@Inject
	private ApplicationContext applicationContext;
	@Inject
	private SystemService systemService;
	@Inject
	private LanguageService languageService;
	@Inject
	private SessionService sessionService;
	
	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response authenticate(@FormParam("token") String token) throws ApiException {
		AuthenticationEvent authenticationInfo = applicationContext.getBean(LajiAuthClient.class).getAndValidateAuthenticationInfo(token);
		if (!authenticationInfo.getUser().getQname().isPresent()) {
			throw new WebApplicationException(403);
		}
		sessionService.authenticateSelf(token, authenticationInfo);
		if (authenticationInfo.getUser().getDefaultLanguage().isPresent()) {
			languageService.setLanguage(authenticationInfo.getUser().getDefaultLanguage().get());
		}
		return redirect(authenticationInfo);
	}

	private Response redirect(AuthenticationEvent authenticationInfo) {
		try {
			URI redirect = getRedirectURI(authenticationInfo);
			return Response.seeOther(redirect).build();
		} catch (URISyntaxException e) {
			throw new WebApplicationException("INVALID NEXT PARAM", Response.Status.BAD_REQUEST);
		}
	}

	private URI getRedirectURI(AuthenticationEvent authenticationInfo) throws URISyntaxException {
		String redirect = systemService.getLajiAuthBaseURI().toString();
		if (authenticationInfo.getNext() != null) {
			redirect += authenticationInfo.getNext();
		}
		return new URI(redirect);
	}

}
