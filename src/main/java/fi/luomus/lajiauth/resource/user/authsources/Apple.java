package fi.luomus.lajiauth.resource.user.authsources;

import static fi.luomus.lajiauth.model.Constants.NEXT_PARAMETER;
import static fi.luomus.lajiauth.model.Constants.REDIRECT_METHOD;
import static fi.luomus.lajiauth.model.Constants.TARGET_SYSTEM_PARAMETER;

import java.net.URI;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.HttpMethod;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import fi.luomus.lajiauth.Utils;
import fi.luomus.lajiauth.model.AuthenticationSource;
import fi.luomus.lajiauth.model.AuthenticationSourceUser;
import fi.luomus.lajiauth.model.Constants;
import fi.luomus.lajiauth.model.OAuthState;
import fi.luomus.lajiauth.model.ServerConstants;
import fi.luomus.lajiauth.service.AppleService;
import fi.luomus.lajiauth.service.OAuthStateService;
import fi.luomus.lajiauth.service.UserAuthenticatedService;
import fi.luomus.utils.exceptions.ApiException;

@Path(Constants.AUTH_SOURCES + "/APPLE/")
public class Apple {

	private final static String OAUTH = "oauth";

	@Inject
	private UriInfo uriInfo;
	@Inject
	private AppleService appleService;
	@Inject
	private OAuthStateService stateService;
	@Inject
	private UserAuthenticatedService authenticatedService;

	@GET
	public Response get(
			@QueryParam(TARGET_SYSTEM_PARAMETER) String target,
			@QueryParam(NEXT_PARAMETER) String next,
			@QueryParam(REDIRECT_METHOD) @DefaultValue(HttpMethod.POST) String redirectMethod,
			@QueryParam(ServerConstants.PERMANENT) @DefaultValue("false") boolean permanent) {

		URI redirectParam = uriInfo.getBaseUriBuilder().path(Constants.AUTH_SOURCES).path(AuthenticationSource.APPLE.toString()).path(OAUTH).build();
		String state = stateService.serializeState(target, next, redirectMethod, permanent);

		URI redirectUri = appleService.getRedirectUri(redirectParam, state);
		return Response.seeOther(redirectUri).build();
	}

	@Path(OAUTH)
	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response oauth(
			@FormParam("state") String stateValue,
			@FormParam("code") String code,
			@FormParam("user") String user,
			@FormParam("error") String error) throws ApiException {
		if (error != null) {
			if ("user_cancelled_authorize".equals(error)) {
				return Response.ok(Utils.generic("auth_source_denied", "danger")).build();
			}
			throw new IllegalStateException("Apple oauth gave unknown error " + error);
		}
		URI redirectURI = uriInfo.getRequestUriBuilder().replaceQuery("").build();
		AuthenticationSourceUser userDetails = appleService.getUserDetails(redirectURI, code, user);
		OAuthState state = stateService.parseState(stateValue);

		String token = authenticatedService.getTokenForLogin(AuthenticationSource.APPLE, userDetails, state);
		URI uri = authenticatedService.getApprovalRedirect(uriInfo, token);
		return Response.seeOther(uri).build();
	}

}
