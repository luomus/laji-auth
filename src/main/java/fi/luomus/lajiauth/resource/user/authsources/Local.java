package fi.luomus.lajiauth.resource.user.authsources;

import static fi.luomus.lajiauth.model.AuthenticationSource.LOCAL;
import static fi.luomus.lajiauth.model.Constants.AUTH_SOURCES;
import static fi.luomus.lajiauth.model.Constants.NEXT_PARAMETER;
import static fi.luomus.lajiauth.model.Constants.REDIRECT_METHOD;
import static fi.luomus.lajiauth.model.Constants.TARGET_SYSTEM_PARAMETER;
import static fi.luomus.lajiauth.util.ErrorPageWebApplicationException.wrap;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.HttpMethod;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import org.glassfish.jersey.server.mvc.Viewable;

import com.google.common.base.Optional;

import fi.luomus.lajiauth.Utils;
import fi.luomus.lajiauth.model.AuthenticationSource;
import fi.luomus.lajiauth.model.AuthenticationSourceUser;
import fi.luomus.lajiauth.model.Constants;
import fi.luomus.lajiauth.model.ServerConstants;
import fi.luomus.lajiauth.repository.UsedHashesRepository;
import fi.luomus.lajiauth.resource.user.Approval;
import fi.luomus.lajiauth.resource.user.Self;
import fi.luomus.lajiauth.service.LanguageService;
import fi.luomus.lajiauth.service.LocalUserService;
import fi.luomus.lajiauth.service.LocalUserService.HashWithNonce;
import fi.luomus.lajiauth.service.LoginLinksService;
import fi.luomus.lajiauth.service.NotificationService;
import fi.luomus.lajiauth.service.UserAuthenticatedService;
import fi.luomus.lajiauth.service.UserService;
import fi.luomus.lajiauth.util.EmailValidator;
import fi.luomus.lajiauth.util.ErrorPageWebApplicationException;
import fi.luomus.lajiauth.util.IPUtil;
import fi.luomus.lajiauth.util.LajiAuthPreferences;
import fi.luomus.utils.exceptions.ApiException;
import fi.luomus.utils.exceptions.NotFoundException;
import fi.luomus.utils.preferences.LuomusPreferences;

@Path(AUTH_SOURCES + "/LOCAL/")
public class Local {

	Random random = new Random();

	private static final String PASSWORD_AGAIN = "passwordAgain";
	private static final String PASSWORD = "password";
	private static final String PREFERRED_NAME = "preferredName";
	private static final String INHERITED_NAME = "inheritedName";
	private static final String UNSUCCESSFUL_LOGIN = "incorrect";
	private static final String BLOCKED_LOGIN = "blocked";
	private static final String EMAIL = "email";
	private static final String GROUP = "group";
	private static final String HASH = "hash";
	private static final String NONCE = "nonce";
	private static final String REGISTER = "register";
	private static final String FORGOT = "forgot";
	private static final String RESET = "reset-password";
	private static final String DONE = "done";

	@Inject
	private UriInfo uriInfo;
	@Inject
	private LocalUserService localUserService;
	@Inject
	private NotificationService notificationService;
	@Inject
	private LanguageService languageService;
	@Inject
	private UserService userService;
	@Inject
	private UserAuthenticatedService userAuthenticatedService;
	@Inject
	private UsedHashesRepository usedHashesRepository;
	@Inject
	private LuomusPreferences preferences;
	@Inject
	private HttpServletRequest request;
	@Inject
	private LoginLinksService loginLinksService;

	@GET
	@Produces(MediaType.TEXT_HTML)
	public Viewable loginForm(
			@QueryParam(TARGET_SYSTEM_PARAMETER) String targetSystemId,
			@QueryParam(NEXT_PARAMETER) String next,
			@QueryParam(REDIRECT_METHOD) @DefaultValue(HttpMethod.POST) String redirectMethod,
			@QueryParam(ServerConstants.PERMANENT) @DefaultValue("false") boolean permanent,
			@QueryParam(UNSUCCESSFUL_LOGIN) @DefaultValue("false") Boolean unsuccessful,
			@QueryParam(BLOCKED_LOGIN) @DefaultValue("false") Boolean blocked) throws ErrorPageWebApplicationException, ApiException {
		if (targetSystemId == null || next == null) {
			throw wrap(new WebApplicationException("MISSING_PARAMETER", Response.Status.BAD_REQUEST));
		}

		Map<String, Object> model = loginLinksService.initCommonLoginModel(targetSystemId);
		model.put(UNSUCCESSFUL_LOGIN, unsuccessful);
		model.put(BLOCKED_LOGIN, blocked);
		model.put(Constants.TARGET_SYSTEM_PARAMETER, targetSystemId);
		model.put(Constants.NEXT_PARAMETER, next);
		model.put(REDIRECT_METHOD, redirectMethod);
		model.put(ServerConstants.PERMANENT, Boolean.toString(permanent));
		model.put("registerUri", getRegisterLinkURI(uriInfo, targetSystemId, next, redirectMethod, permanent).build());
		model.put("forgotUri", getForgotURI().build());

		return new Viewable("/local/login", model);
	}

	private UriBuilder getForgotURI() {
		return uriInfo.getRequestUriBuilder().replaceQuery("").path(FORGOT);
	}

	public static UriBuilder getRegisterLinkURI(UriInfo uriInfo, String targetSystemId, String next, String redirectMethod, boolean permanent) {
		return uriInfo.getBaseUriBuilder()
				.replaceQuery("")
				.path(AUTH_SOURCES).path("LOCAL").path(REGISTER)
				.replaceQueryParam(Constants.TARGET_SYSTEM_PARAMETER, targetSystemId)
				.replaceQueryParam(Constants.NEXT_PARAMETER, next)
				.replaceQueryParam(Constants.REDIRECT_METHOD, redirectMethod)
				.replaceQueryParam(ServerConstants.PERMANENT, permanent);
	}

	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response login(
			@FormParam(TARGET_SYSTEM_PARAMETER) String targetSystemId,
			@FormParam(NEXT_PARAMETER) String next,
			@FormParam(REDIRECT_METHOD) String redirectMethod,
			@FormParam(ServerConstants.PERMANENT) @DefaultValue("false") boolean permanent,
			@FormParam("email") String email,
			@FormParam(PASSWORD) String password) throws ApiException {
		if (email != null) email = email.toLowerCase().trim();
		if (IPUtil.shouldBlock(request, email)) {
			URI uri = uriInfo.getRequestUriBuilder().replaceQueryParam(UNSUCCESSFUL_LOGIN, "").replaceQueryParam(BLOCKED_LOGIN, true).build();
			return Response.seeOther(uri).build();
		}
		try {
			localUserService.login(email, password);
			AuthenticationSourceUser user = localUserService.find(email).get();
			String token = userAuthenticatedService.getTokenForLogin(LOCAL, user, targetSystemId, next, redirectMethod, permanent);
			URI uri = userAuthenticatedService.getApprovalRedirect(uriInfo, token);
			IPUtil.reportOk(request, email);
			return Response.seeOther(uri).build();
		} catch (NotFoundException | LocalUserService.InvalidCredentialsException e) {
			URI uri = uriInfo.getRequestUriBuilder().replaceQueryParam(UNSUCCESSFUL_LOGIN, true).replaceQueryParam(BLOCKED_LOGIN, "").build();
			return Response.seeOther(uri).build();
		}
	}

	@Path(FORGOT)
	@Produces(MediaType.TEXT_HTML)
	@GET
	public Viewable forgotForm() {
		return new Viewable("/local/forgot");
	}

	@Path(FORGOT)
	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response forgot(@FormParam(EMAIL) String email) throws InterruptedException {
		Thread.sleep((long) (this.random.nextDouble() * 1000) + 100);
		if (email != null) email = email.toLowerCase().trim();
		if (IPUtil.shouldBlock(request, "forgot")) {
			return Response.seeOther(sentURI()).build();
		}
		Optional<AuthenticationSourceUser> user = localUserService.find(email);
		if (!user.isPresent()) {
			return Response.seeOther(sentURI()).build();
		}
		HashWithNonce hashWithNonce = localUserService.hashLocalUser(email);
		URI resetUri = uriInfo.getBaseUriBuilder().path(AUTH_SOURCES).path(LOCAL.name()).path(RESET)
				.queryParam(EMAIL, email)
				.queryParam(HASH, hashWithNonce.hash)
				.queryParam(NONCE, hashWithNonce.nonce)
				.build();
		String locale = languageService.getLanguage();
		notificationService.notifyLocalUserForgotPassword(user.get(), locale, resetUri);
		return Response.seeOther(sentURI()).build();
	}

	private URI sentURI() {
		return uriInfo.getRequestUriBuilder().replaceQuery("").replaceQueryParam("sent", Boolean.TRUE).build();
	}

	@Path(RESET)
	@GET
	@Produces(MediaType.TEXT_HTML)
	public Viewable resetPasswordForm(
			@QueryParam(EMAIL) String email,
			@QueryParam(HASH) String hash,
			@QueryParam(NONCE) String nonce) throws ErrorPageWebApplicationException {
		if (email != null) email = email.toLowerCase().trim();
		validateHashForReset(email, hash, nonce);
		Map<String, String> model = new HashMap<>();
		model.put(EMAIL, email);
		model.put(HASH, hash);
		model.put(NONCE, nonce);
		return new Viewable("/local/reset", model);
	}

	@Path(RESET)
	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response resetPasswordForm(
			@FormParam(PASSWORD) String password,
			@FormParam(PASSWORD_AGAIN) String passwordAgain,
			@FormParam(EMAIL) String email,
			@FormParam(HASH) String hash,
			@FormParam(NONCE) String nonce)
					throws NotFoundException, ErrorPageWebApplicationException {
		if (email != null) email = email.toLowerCase().trim();
		validateHashForReset(email, hash, nonce);
		if (!password.equals(passwordAgain) || !localUserService.isPasswordStrongEnough(password)) {
			return Response.seeOther(uriInfo.getRequestUri()).build();
		}
		localUserService.setPassword(email, password);
		usedHashesRepository.addHash(hash);
		return Response.seeOther(doneURI()).build();
	}

	private URI doneURI() {
		return uriInfo.getRequestUriBuilder().replaceQuery("").path(DONE).build();
	}

	private void validateHashForReset(String email, String hash, String nonce) throws ErrorPageWebApplicationException {
		if (!localUserService.isValidLocalUser(email, new HashWithNonce(hash, nonce))) {
			throw wrap(new WebApplicationException("INVALID_HASH", 403));
		}
		if (usedHashesRepository.isHashUsed(hash)) {
			throw wrap(new WebApplicationException("HASH_USED", 403));
		}
	}

	@Path(RESET + "/" + DONE)
	@GET
	@Produces(MediaType.TEXT_HTML)
	public Viewable resetDone() {
		return Utils.generic(RESET, "success");
	}

	@Path(REGISTER)
	@GET
	@Produces(MediaType.TEXT_HTML)
	public Viewable registerForm(
			@QueryParam(TARGET_SYSTEM_PARAMETER) String targetSystemId,
			@QueryParam(NEXT_PARAMETER) String next,
			@QueryParam(REDIRECT_METHOD) @DefaultValue(HttpMethod.POST) String redirectMethod,
			@QueryParam(ServerConstants.PERMANENT) @DefaultValue("false") boolean permanent,
			@QueryParam(EMAIL) String email,
			@QueryParam(PREFERRED_NAME) String preferredName,
			@QueryParam(INHERITED_NAME) String inheritedName) throws ApiException, ErrorPageWebApplicationException {
		if (notGiven(targetSystemId, next, redirectMethod)) {
			targetSystemId = preferences.get(LajiAuthPreferences.LAJI_AUTH_SYSTEM_ID).get();
			next = "/" + Self.SELF;
			redirectMethod = "POST";
		}
		Map<String, Object> model = loginLinksService.initCommonLoginModel(targetSystemId);
		model.put(Constants.TARGET_SYSTEM_PARAMETER, targetSystemId);
		model.put(Constants.NEXT_PARAMETER, next);
		model.put(REDIRECT_METHOD, redirectMethod);
		model.put(ServerConstants.PERMANENT, Boolean.toString(permanent));
		model.put(EMAIL, nullV(email));
		model.put(PREFERRED_NAME, nullV(preferredName));
		model.put(INHERITED_NAME, nullV(inheritedName));
		return new Viewable("/local/register", model);
	}

	private String nullV(String s) {
		if (s == null) return "";
		return s;
	}

	private boolean notGiven(String ... strings) {
		for (String s : strings) {
			if (s == null) return true;
		}
		return false;
	}

	@Path(REGISTER)
	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response register(
			@FormParam(TARGET_SYSTEM_PARAMETER) String targetSystemId,
			@FormParam(NEXT_PARAMETER) String next,
			@FormParam(REDIRECT_METHOD) String redirectMethod,
			@FormParam(ServerConstants.PERMANENT) @DefaultValue("false") boolean permanent,
			@FormParam(EMAIL) String email,
			@FormParam(PREFERRED_NAME) String preferredName,
			@FormParam(INHERITED_NAME) String inheritedName,
			@FormParam(GROUP) String group,
			@FormParam(PASSWORD) String password,
			@FormParam(PASSWORD_AGAIN) String passwordAgain) throws ErrorPageWebApplicationException, ApiException {
		if (notGiven(email, inheritedName, preferredName, targetSystemId, next, redirectMethod)) {
			throw wrap(new WebApplicationException("MISSING_PARAMETER", 400));
		}

		email = email.trim().toLowerCase();
		validateEmail(email);

		if (!password.equals(passwordAgain) || !localUserService.isPasswordStrongEnough(password)) {
			return Response.seeOther(uriInfo.getRequestUri()).build();
		}

		if (localUserService.find(email).isPresent() || userService.findUser(email, AuthenticationSource.LOCAL).isPresent()) {
			return Response.seeOther(uriInfo.getRequestUriBuilder().replaceQueryParam("emailInUse", true).build()).build();
		}

		localUserService.createNewUnconfirmedUser(email, preferredName, inheritedName, password);
		String token = getToken(targetSystemId, next, redirectMethod, email, preferredName, inheritedName, group, permanent);
		generateAndSendConfirmationEmail(token, email, preferredName, inheritedName);

		URI done = doneURI();
		return Response.seeOther(done).build();
	}

	private void generateAndSendConfirmationEmail(String token, String email, String preferredName, String inheritedName) {

		URI confirmUri = userAuthenticatedService.getConfirmLinkURI(uriInfo, token);
		String locale = languageService.getLanguage();
		notificationService.notifyEmailConfirmationRequest(email, preferredName + " " + inheritedName, locale, confirmUri);
	}

	private String getToken(String targetSystemId, String next, String redirectMethod, String email, String preferredName, String inheritedName, String group, boolean permanent) throws ApiException {
		AuthenticationSourceUser user = new AuthenticationSourceUser();
		user.setId(email);
		user.setFullName(preferredName + " " + inheritedName);
		user.setPreferredName(preferredName);
		user.setInheritedName(inheritedName);
		user.setEmail(email);
		user.setGroup(group);

		String token = userAuthenticatedService.getTokenForLogin(AuthenticationSource.LOCAL, user, targetSystemId, next, redirectMethod, permanent);
		return token;
	}

	private void validateEmail(String email) throws ErrorPageWebApplicationException {
		if (!EmailValidator.validEmail(email)) {
			throw wrap(new WebApplicationException("BAD_EMAIL", Response.Status.BAD_REQUEST));
		}
	}

	@Path(REGISTER + "/" + DONE)
	@Produces(MediaType.TEXT_HTML)
	@GET
	public Viewable registerDone() {
		return Utils.generic(Approval.REGISTER_THANKYOU, "success");
	}

}
