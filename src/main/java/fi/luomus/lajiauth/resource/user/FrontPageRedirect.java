package fi.luomus.lajiauth.resource.user;

import java.net.URI;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

@Path("/")
public class FrontPageRedirect {
	
    @Inject
    private UriInfo uriInfo;

    @GET
    public Response redirect() {
    	 URI self = uriInfo.getBaseUriBuilder().path(Self.SELF).build();
         return Response.seeOther(self).build();
    }

}
