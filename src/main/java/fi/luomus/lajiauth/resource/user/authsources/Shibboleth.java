package fi.luomus.lajiauth.resource.user.authsources;

import static fi.luomus.lajiauth.model.Constants.REDIRECT_METHOD;
import static fi.luomus.lajiauth.util.ErrorPageWebApplicationException.wrap;

import java.net.URI;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HttpMethod;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Charsets;
import com.google.common.base.Optional;

import fi.luomus.lajiauth.model.AuthenticationSource;
import fi.luomus.lajiauth.model.AuthenticationSourceUser;
import fi.luomus.lajiauth.model.Constants;
import fi.luomus.lajiauth.model.ServerConstants;
import fi.luomus.lajiauth.service.UserAuthenticatedService;
import fi.luomus.lajiauth.util.ErrorPageWebApplicationException;
import fi.luomus.utils.exceptions.ApiException;

@Path(Constants.AUTH_SOURCES + "/shibboleth/")
public class Shibboleth {

	@Inject
	private HttpServletRequest servletRequest;
	@Inject
	private UriInfo uriInfo;
	@Inject
	private UserAuthenticatedService userAuthenticatedService;
	private final Logger logger = LoggerFactory.getLogger(Shibboleth.class);

	@GET
	@Path("{authSource}")
	@Produces(MediaType.TEXT_HTML)
	public Response get(
			@PathParam("authSource") String authSourceValue,
			@QueryParam(Constants.TARGET_SYSTEM_PARAMETER) String targetSystemId,
			@QueryParam(REDIRECT_METHOD) @DefaultValue(HttpMethod.POST) String redirectMethod,
			@QueryParam(Constants.NEXT_PARAMETER) String next,
			@QueryParam(ServerConstants.PERMANENT) @DefaultValue("false") boolean permanent) throws ApiException, ErrorPageWebApplicationException {
		AuthenticationSource authSource = AuthenticationSource.valueOf(authSourceValue);
		if (targetSystemId == null || next == null) {
			throw wrap(new WebApplicationException("MISSING_PARAMETER", Response.Status.BAD_REQUEST));
		}
		AuthenticationSourceUser user = new AuthenticationSourceUser();
		user.setId(requireAuthSourceUserId(authSource));

		String fullName = requireFullName(); 
		user.setFullName(fullName);

		Optional<String> givenName = getAttribute("givenName");
		Optional<String> inheritedName = getAttribute("sn"); 
		if (givenName.isPresent() && inheritedName.isPresent()) {
			user.setInheritedName(inheritedName.get());
			user.setPreferredName(givenName.get());
		} else {
			user.setInheritedName(parseInheritedName(fullName));
			user.setPreferredName(parsePreferredname(fullName));	
		}

		String token = userAuthenticatedService.getTokenForLogin(authSource, user, targetSystemId, next, redirectMethod, permanent);
		URI uri = userAuthenticatedService.getApprovalRedirect(uriInfo, token);
		return Response.seeOther(uri).build();
	}

	private String parsePreferredname(String fullName) {
		if (fullName.contains(" ")) return fullName.substring(0, fullName.indexOf(" "));
		return "";
	}

	private String parseInheritedName(String fullName) {
		if (fullName.contains(" ")) return fullName.substring(fullName.indexOf(" "));
		return fullName;
	}

	private String requireFullName() throws ErrorPageWebApplicationException {
		return isFakeMode() ? uriInfo.getQueryParameters().getFirst("authSourceName") : requireAttribute("cn");
	}

	private String requireAuthSourceUserId(AuthenticationSource authSource) throws ErrorPageWebApplicationException {
		if (isFakeMode()) {
			String fakeAuthSourceUserId = uriInfo.getQueryParameters().getFirst("authSourceUserId");
			logger.warn(String.format("fake shibboleth mode enabled, using fake authentication source user id \"%s\"",
					fakeAuthSourceUserId));
			return fakeAuthSourceUserId;
		}
		switch (authSource) {
		case HAKA:
			return requireAttribute("eppn");
		case VIRTU:
			return virtuUserId();
		default:
			logger.warn(String.format("login attempt for an unknown auth source %s", authSource));
			throw wrap(new WebApplicationException("INVALID_SHIBBOLETH_SOURCE", Response.Status.NOT_FOUND));
		}
	}

	private boolean isFakeMode() {
		return System.getProperty("lajiauth.fakeShibboleth", "false").equals("true");
	}

	private String virtuUserId() throws ErrorPageWebApplicationException {
		String virtuHomeOrganization = requireAttribute("virtuHomeOrganization");
		String virtuLocalId = requireAttribute("virtuLocalID");
		return String.format("%s@%s", virtuLocalId, virtuHomeOrganization);
	}

	protected String requireAttribute(String key) throws ErrorPageWebApplicationException {
		Optional<String> attribute = getAttribute(key);
		String value = attribute.orNull();
		if (value == null || value.trim().isEmpty()) {
			logger.error(String.format("Missing required attribute %s", key));
			throw wrap(new WebApplicationException("MISSING_ATTRIBUTE", Response.Status.BAD_REQUEST));
		}
		return value;
	}

	protected Optional<String> getAttribute(String key) {
		Object value = servletRequest.getAttribute(key);
		if (value != null) {
			// encoding hack
			return Optional.of(new String(value.toString().getBytes(Charsets.ISO_8859_1), Charsets.UTF_8));
		}
		return Optional.absent();
	}

}
