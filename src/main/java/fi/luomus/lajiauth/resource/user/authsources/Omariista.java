package fi.luomus.lajiauth.resource.user.authsources;

import static fi.luomus.lajiauth.model.Constants.NEXT_PARAMETER;
import static fi.luomus.lajiauth.model.Constants.REDIRECT_METHOD;
import static fi.luomus.lajiauth.model.Constants.TARGET_SYSTEM_PARAMETER;

import java.net.URI;

import javax.inject.Inject;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HttpMethod;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import fi.luomus.lajiauth.model.AuthenticationSource;
import fi.luomus.lajiauth.model.AuthenticationSourceUser;
import fi.luomus.lajiauth.model.Constants;
import fi.luomus.lajiauth.model.OAuthState;
import fi.luomus.lajiauth.model.ServerConstants;
import fi.luomus.lajiauth.service.OAuthStateService;
import fi.luomus.lajiauth.service.OmariistaService;
import fi.luomus.lajiauth.service.UserAuthenticatedService;
import fi.luomus.utils.exceptions.ApiException;

@Path(Constants.AUTH_SOURCES + "/OMARIISTA/")
public class Omariista {

	private final static String OAUTH = "oauth";

	@Inject
	private UriInfo uriInfo;
	@Inject
	private OmariistaService omariistaService;
	@Inject
	private OAuthStateService stateService;
	@Inject
	private UserAuthenticatedService authenticatedService;

	@GET
	public Response get(
			@QueryParam(TARGET_SYSTEM_PARAMETER) String target,
			@QueryParam(NEXT_PARAMETER) String next,
			@QueryParam(REDIRECT_METHOD) @DefaultValue(HttpMethod.POST) String redirectMethod,
			@QueryParam(ServerConstants.PERMANENT) @DefaultValue("false") boolean permanent) {

		String state = stateService.serializeState(target, next, redirectMethod, permanent);
		String redirectUriParam = uriInfo.getBaseUriBuilder().path(Constants.AUTH_SOURCES).path(AuthenticationSource.OMARIISTA.toString()).path(OAUTH).build().toString().replace("login.laji.fi/laji-auth", "login.laji.fi");
		URI redirectUri = omariistaService.getRedirectUri(redirectUriParam, state);
		return Response.seeOther(redirectUri).build();
	}

	@Path(OAUTH)
	@GET
	public Response oauth(
			@QueryParam("state") String stateValue,
			@QueryParam("code") String code) throws ApiException {

		if (code == null || code.trim().isEmpty()) {
			throw new IllegalStateException("Omariista oauth unknown error: no code");
		}
		if (stateValue == null || stateValue.trim().isEmpty()) {
			throw new IllegalStateException("Omraiista oauth unknown error: no state");
		}

		String redirectUriParam = uriInfo.getRequestUriBuilder().replaceQuery("").build().toString().replace("login.laji.fi/laji-auth", "login.laji.fi");
		AuthenticationSourceUser userDetails = omariistaService.getUserDetails(redirectUriParam, code);
		OAuthState state = stateService.parseState(stateValue);

		String token = authenticatedService.getTokenForLogin(AuthenticationSource.OMARIISTA, userDetails, state);
		URI uri = authenticatedService.getApprovalRedirect(uriInfo, token);
		return Response.seeOther(uri).build();
	}

}
