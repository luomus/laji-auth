package fi.luomus.lajiauth.resource.user;

import static fi.luomus.lajiauth.util.ErrorPageWebApplicationException.wrap;

import java.net.URI;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import org.glassfish.jersey.server.mvc.Viewable;
import org.springframework.context.ApplicationContext;

import com.google.common.base.Optional;

import fi.luomus.lajiauth.Utils;
import fi.luomus.lajiauth.model.AuthenticationEvent;
import fi.luomus.lajiauth.model.AuthenticationSource;
import fi.luomus.lajiauth.model.ServerConstants;
import fi.luomus.lajiauth.resource.handlers.RequireAuth;
import fi.luomus.lajiauth.service.LajiAuthClient;
import fi.luomus.lajiauth.service.LanguageService;
import fi.luomus.lajiauth.service.LocalUserService;
import fi.luomus.lajiauth.service.NotificationService;
import fi.luomus.lajiauth.service.SessionService;
import fi.luomus.lajiauth.service.SystemService;
import fi.luomus.lajiauth.service.UserAuthenticatedService;
import fi.luomus.lajiauth.service.UserAuthenticatedService.PermanentLogin;
import fi.luomus.lajiauth.service.UserService;
import fi.luomus.lajiauth.service.UserService.AdditionalUserIdLinkingResult;
import fi.luomus.lajiauth.service.UserService.UserAttributes;
import fi.luomus.lajiauth.util.EmailValidator;
import fi.luomus.lajiauth.util.ErrorPageWebApplicationException;
import fi.luomus.utils.exceptions.ApiException;
import fi.luomus.utils.exceptions.NotFoundException;

@Path(Self.SELF)
public class Self {

	@Inject
	private UriInfo uriInfo;
	@Inject
	private UserService userService;
	@Inject
	private LanguageService languageService;
	@Inject
	private NotificationService notificationService;
	@Inject
	private UserAuthenticatedService authenticatedService;
	@Inject
	private SystemService systemService;
	@Inject
	private SessionService sessionService;
	@Inject
	private ApplicationContext applicationContext;
	@Inject
	private LocalUserService localUserService;

	public static final String SELF = "self";
	private static final String ASSOCIATE = "associate";
	public static final String SELF_ASSOCIATE = "/" + SELF + "/" + ASSOCIATE;
	private final static String ALREADY_ASSOCIATED = "already-associated";
	private final static String ASSOCIATED_TO_OTHER = "associated-to-other";
	private final static String SOURCE_ALREADY_ASSOCIATED = "source-already-associated";

	@GET
	@Produces(MediaType.TEXT_HTML)
	@RequireAuth
	public Viewable self(
			@QueryParam("saved") boolean saved,
			@QueryParam("emailChanged") boolean emailChanged,
			@QueryParam("emailUsed") boolean emailUsed,
			@QueryParam("invalidPassword") boolean invalidPassword,
			@QueryParam("permanentDeleted") boolean permanentDeleted,
			@QueryParam("additionalUserIdSubmitted") boolean additionalUserIdSubmitted,
			@QueryParam("additionalUserIdSuccess") boolean additionalUserIdSuccess,
			@QueryParam("additionalUserIdAlreadyLinked") boolean additionalUserIdAlreadyLinked,
			@QueryParam("linkedSystems") List<String> linkedSystems,
			@QueryParam("linkedUserIdCount") int linkedUserIdCount,
			@QueryParam("deleteAccountNotConfirmed") boolean deleteAccountNotConfirmed) throws ApiException, ErrorPageWebApplicationException {
		Map<String, Object> model = constructSelfModel();
		if (saved) {
			model.put("saved", saved);
		}
		if (permanentDeleted) {
			model.put("permanentDeleted", permanentDeleted);
		}
		if (emailChanged) {
			model.put("emailChanged", emailChanged);
		}
		if (emailUsed) {
			model.put("warning", "email_already_used");
		}
		if (invalidPassword) {
			model.put("warning", "invalid_password");
		}
		if (additionalUserIdSubmitted) {
			if (additionalUserIdSuccess) {
				model.put("linkedSystems", linkedSystems);
				model.put("linkedUserIdCount", linkedUserIdCount);
			} else {
				if (additionalUserIdAlreadyLinked) {
					model.put("warning", "additional_user_id_already_linked");
				} else {
					model.put("warning", "additional_user_id_linking_failed");
				}
			}
		}
		if (deleteAccountNotConfirmed) {
			model.put("warning", "delete_account_not_confirmed");
		}
		return new Viewable("/"+SELF, model);
	}

	private Map<String, Object> constructSelfModel() throws ApiException, ErrorPageWebApplicationException {
		Map<String, Object> model = new HashMap<>();
		AuthenticationEvent authInfo = getCurrentAuthInfo();
		if (authInfo == null) throw wrap(new WebApplicationException("NOT_LOGGED_IN", Response.Status.BAD_REQUEST));

		String locale = languageService.getLanguage();
		Map<String, String> associations = associations(authInfo);
		UserAttributes userInfo = userService.getUser(authInfo.getUser().getQname().get()).get();

		model.put("authInfo", authInfo);
		model.put("associations", associations);
		model.put("associateUri", assosiateNew(associations));
		model.put("organisationDescriptions", userService.getOrganisationDescriptions(authInfo.getUser().getOrganisations(), locale));
		model.put("adminOfOrganisationDescriptions", userService.getOrganisationDescriptions(userInfo.getAdminOfOrganizations(), locale));
		model.put("roleDescriptions", userService.getRoleDescriptions(authInfo, locale));
		model.put("securePortalRoleExpires", userInfo.getSecurePortalUserRoleExpires());
		model.put("logoutUri", "/laji-auth/self/logout");
		model.put("additionalSystems", ServerConstants.ADDITIONAL_SYSTEMS);
		List<PermanentLogin> permanentLogins = authenticatedService.getPermanentLogins(authInfo);
		model.put("permanentLogins", permanentLogins);
		model.put("permanentLoginSystems", getSystemNames(permanentLogins));
		model.put("privacyPolicyUrl", Utils.getPrivacyPolicyUrl(languageService.getLanguage()));
		model.put("helpUrl", Utils.getHelpUrl(languageService.getLanguage()));
		return model;
	}

	private Map<String, String> getSystemNames(List<PermanentLogin> permanentLogins) {
		if (permanentLogins.isEmpty()) return Collections.emptyMap();
		String locale = languageService.getLanguage();
		Map<String, String> names = new HashMap<>();
		for (PermanentLogin permanentLogin : permanentLogins) {
			String target = permanentLogin.getEvent().getTarget();
			names.put(target, getSystemName(target, locale));
		}
		return names;
	}

	private String getSystemName(String target, String locale) {
		try {
			return systemService.getSystemName(target, locale).or(Optional.<String>absent()).or(target);
		} catch (ApiException e) {
			return target;
		}
	}

	private String assosiateNew(Map<String, String> associations) {
		LajiAuthClient client = applicationContext.getBean(LajiAuthClient.class);
		URI uri = client.createLoginUrl(SELF_ASSOCIATE).build();
		UriBuilder b = UriBuilder.fromUri(uri);
		for (String source : associations.keySet()) {
			b.queryParam("associated", source);
		}
		return b.build().toString();
	}

	private Map<String, String> associations(AuthenticationEvent authInfo) throws ApiException {
		// freemarker hack
		Map<String, String> associations = new HashMap<>();
		for (Map.Entry<AuthenticationSource, String> e : userService.getAssociations(authInfo.getUser().getQname().get()).get().entrySet()) {
			associations.put(e.getKey().name(), e.getValue());
		}
		return associations;
	}

	@Path("additionalUserId")
	@RequireAuth
	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response addAdditionalUserId(
			@FormParam("additional-system") String system,
			@FormParam("additional-system-username") String username,
			@FormParam("additional-system-password") String password) throws ApiException, ErrorPageWebApplicationException, NotFoundException {
		AuthenticationEvent authInfo = getCurrentAuthInfo();
		if (authInfo == null) throw wrap(new WebApplicationException("NOT_LOGGED_IN", Response.Status.BAD_REQUEST));

		AdditionalUserIdLinkingResult result = userService.addAdditionalUserIds(authInfo, system, username, password);

		UriBuilder redirect = uriInfo.getBaseUriBuilder().path(SELF)
				.replaceQueryParam("additionalUserIdSubmitted", true)
				.replaceQueryParam("additionalUserIdSuccess", result.isSuccess());
		if (!result.isSuccess()) {
			redirect.replaceQueryParam("additionalUserIdAlreadyLinked", result.isAlreadyLinked());
		} else {
			for (String s : result.getLinkedSystems()) {
				redirect.queryParam("linkedSystems", s);
			}
			redirect.replaceQueryParam("linkedUserIdCount", result.getLinkedUserIdCount());
		}
		return Response.seeOther(redirect.build()).build();
	}

	@Path("name")
	@RequireAuth
	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response changeName(
			@FormParam("preferredName") String preferredName,
			@FormParam("inheritedName") String inheritedName,
			@FormParam("group") String group) throws ApiException, ErrorPageWebApplicationException {

		AuthenticationEvent authInfo = getCurrentAuthInfo();
		if (authInfo == null) throw wrap(new WebApplicationException("NOT_LOGGED_IN", Response.Status.BAD_REQUEST));

		validateName(preferredName, inheritedName);
		if (group != null) {
			group = group.trim();
			if (group.isEmpty()) {
				group = null;
			}
		}

		authInfo.getUser().setName(preferredName + " " + inheritedName);
		authInfo.getUser().setPreferredName(Optional.of(preferredName));
		authInfo.getUser().setInheritedName(Optional.of(inheritedName));
		authInfo.getUser().setGroup(Optional.fromNullable(group));

		userService.changeName(authInfo);

		return selfSaved();
	}

	@Path("yearOfBirth")
	@RequireAuth
	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response changeYearOfBirth(@FormParam("yearOfBirth") String yearOfBirth) throws ApiException, ErrorPageWebApplicationException {
		AuthenticationEvent authInfo = getCurrentAuthInfo();
		if (authInfo == null) throw wrap(new WebApplicationException("NOT_LOGGED_IN", Response.Status.BAD_REQUEST));

		int year = validateYearOfBirth(yearOfBirth);

		authInfo.getUser().setYearOfBirth(Optional.of(year));

		userService.changeYearOfBirth(authInfo);

		return selfSaved();
	}

	@Path("defaultLanguage")
	@RequireAuth
	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response defaultLanguage(@FormParam("defaultLanguage") String defaultLanguage) throws ApiException, ErrorPageWebApplicationException {
		AuthenticationEvent authInfo = getCurrentAuthInfo();
		if (authInfo == null) throw wrap(new WebApplicationException("NOT_LOGGED_IN", Response.Status.BAD_REQUEST));

		defaultLanguage = validateDefaultLanguage(defaultLanguage);

		authInfo.getUser().setDefaultLanguage(Optional.of(defaultLanguage));

		userService.changeDefaultLanguage(authInfo);

		return selfSaved();
	}

	@Path("address")
	@RequireAuth
	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response address(@FormParam("address") String address,
			@FormParam("postCode") String postCode,
			@FormParam("city") String city) throws ApiException, ErrorPageWebApplicationException {
		AuthenticationEvent authInfo = getCurrentAuthInfo();
		if (authInfo == null) throw wrap(new WebApplicationException("NOT_LOGGED_IN", Response.Status.BAD_REQUEST));

		String combinedAddress = combineAddress(address, postCode, city);
		authInfo.getUser().setAddress(Optional.fromNullable(combinedAddress));

		userService.changeAddress(authInfo);

		return selfSaved();
	}

	private String combineAddress(String address, String postCode, String city) {
		if (address == null || postCode == null || city == null) return null;
		address = address.trim();
		postCode = postCode.trim();
		city = city.trim();
		if (address.isEmpty() || postCode.isEmpty() || city.isEmpty()) return null;
		return upperCaseFirst(address) + "\n" + postCode.toUpperCase() + " " + city.toUpperCase();
	}

	private static String upperCaseFirst(String s) {
		if (s == null) return s;
		if (s.isEmpty()) return s;
		if (s.length() == 1) return s.toUpperCase();
		return s.substring(0, 1).toUpperCase() + s.substring(1);
	}

	@Path("changeLocalPassword")
	@RequireAuth
	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response changeLocalPassword(
			@FormParam("passwordCurrent") String passwordCurrent,
			@FormParam("password") String password,
			@FormParam("passwordAgain") String passwordAgain) throws ApiException, ErrorPageWebApplicationException {

		AuthenticationEvent authInfo = getCurrentAuthInfo();
		if (authInfo == null) throw wrap(new WebApplicationException("NOT_LOGGED_IN", Response.Status.BAD_REQUEST));

		if (!password.equals(passwordAgain) || !localUserService.isPasswordStrongEnough(password)) {
			return Response.seeOther(uriInfo.getRequestUri()).build();
		}

		String email = userService.getAssociations(authInfo.getUser().getQname().get()).get().get(AuthenticationSource.LOCAL).toLowerCase();

		try {
			localUserService.login(email, passwordCurrent);
		} catch (NotFoundException | LocalUserService.InvalidCredentialsException e) {
			return Response.seeOther(uriInfo.getBaseUriBuilder().path(SELF).queryParam("invalidPassword", "true").build()).build();
		}

		try {
			localUserService.setPassword(email, password);
		} catch (NotFoundException e) {
			throw wrap(new WebApplicationException("USER_NOT_FOUND", Response.Status.BAD_REQUEST));
		}

		return selfSaved();
	}

	@Path("email")
	@RequireAuth
	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response changeEmail(@FormParam("email") String email) throws ApiException, ErrorPageWebApplicationException {
		AuthenticationEvent authInfo = getCurrentAuthInfo();
		if (authInfo == null) throw wrap(new WebApplicationException("NOT_LOGGED_IN", Response.Status.BAD_REQUEST));

		validateEmail(email);

		if (email != null) email = email.toLowerCase().trim();

		if (userService.findUser(email).isPresent() || localUserService.find(email).isPresent()) {
			return Response.seeOther(uriInfo.getBaseUriBuilder().path(SELF).replaceQueryParam("emailUsed", "true").build()).build();
		}

		String locale = languageService.getLanguage();
		String currentEmail = authInfo.getUser().getEmail().toLowerCase().trim();
		String name = authInfo.getUser().getName();

		authInfo.getUser().setEmail(email);
		String token = authenticatedService.getTokenForEmailChangeLogin(authInfo);
		authInfo.getUser().setEmail(currentEmail);

		URI confirmURI = authenticatedService.getConfirmLinkURI(uriInfo, token);
		notificationService.notifyEmailChangeRequested(currentEmail, name, locale);
		notificationService.notifyEmailConfirmationRequest(email, name, locale, confirmURI);

		return Response.seeOther(selfSavedBase().replaceQueryParam("emailChanged", "true").build()).build();
	}

	private void validateEmail(String email) throws ErrorPageWebApplicationException {
		if (!EmailValidator.validEmail(email)) {
			throw wrap(new WebApplicationException("YEAR_OF_BIRTH", Response.Status.BAD_REQUEST));
		}
	}

	private String validateDefaultLanguage(String defaultLanguage) {
		if (defaultLanguage == null) return "fi";
		if (ServerConstants.ALLOWED_LANGUAGES.contains(defaultLanguage)) return defaultLanguage;
		return "fi";
	}

	private void validateName(String preferredName, String inheritedName) throws ErrorPageWebApplicationException {
		if (preferredName == null || preferredName.length() < 2) {
			throw wrap(new WebApplicationException("PREFERED_NAME", Response.Status.BAD_REQUEST));
		}
		if (inheritedName == null || inheritedName.length() < 2) {
			throw wrap(new WebApplicationException("INHERITED_NAME", Response.Status.BAD_REQUEST));
		}
	}

	private int validateYearOfBirth(String yearOfBirth) throws ErrorPageWebApplicationException {
		int year;
		try {
			year = Integer.valueOf(yearOfBirth);
		} catch  (Exception e) {
			throw wrap(new WebApplicationException("YEAR_OF_BIRTH", Response.Status.BAD_REQUEST));
		}
		int currentYear = Calendar.getInstance().get(Calendar.YEAR);
		if (year < 1900 || year > currentYear) {
			throw wrap(new WebApplicationException("YEAR_OF_BIRTH", Response.Status.BAD_REQUEST));
		}
		return year;
	}

	private Response selfSaved() {
		return Response.seeOther(selfSavedBase().build()).build();
	}

	private UriBuilder selfSavedBase() {
		return uriInfo.getBaseUriBuilder().path(SELF).replaceQueryParam("saved", "true");
	}

	@Path(ASSOCIATE)
	@RequireAuth
	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response associate(@FormParam("token") String token) throws ApiException, NotFoundException {
		AuthenticationEvent currentAuthInfo = getCurrentAuthInfo();
		// user's new authentication (needs to be associated with the current id)
		AuthenticationEvent newAuthToken = authenticatedService.getAuthenticationInfoOrFail(token);

		URI redirect = associateIfValid(currentAuthInfo, newAuthToken);

		return Response.seeOther(redirect).build();
	}

	private URI associateIfValid(AuthenticationEvent currentAuthInfo, AuthenticationEvent newAuthInfo) throws ApiException, NotFoundException {
		String userQname = currentAuthInfo.getUser().getQname().get();
		UriBuilder uriBuilder = uriInfo.getRequestUriBuilder().replaceQuery("");

		if (newAuthInfo.getUser().getQname().isPresent()) { // New login should not be assosiated with any user Qname yet
			if (userQname.equals(newAuthInfo.getUser().getQname().get())) {
				return uriBuilder.path(ALREADY_ASSOCIATED).build(); // Was already assosiated with this same user Qname
			}
			return uriBuilder.path(ASSOCIATED_TO_OTHER).build(); // Was assosiated to some other user Qname
		}

		if (userService.getAssociations(userQname).get().containsKey(newAuthInfo.getSource())) {
			return uriBuilder.path(SOURCE_ALREADY_ASSOCIATED).build(); // This user already has assosiation for this source
		}

		this.userService.associateUser(newAuthInfo.getSource(), newAuthInfo.getUser().getAuthSourceId(), userQname);
		return uriInfo.getBaseUriBuilder().path(SELF).queryParam("newSource", newAuthInfo.getSource()).build();
	}

	@Path("logout")
	@GET
	public Response logout()  {
		sessionService.logoutSelf();
		return Response.seeOther(uriInfo.getBaseUriBuilder().path(SELF).build()).build();
	}

	@Path("associate/" + ALREADY_ASSOCIATED)
	@RequireAuth
	@Produces(MediaType.TEXT_HTML)
	@GET
	public Viewable alreadyAssociated() throws ApiException, ErrorPageWebApplicationException {
		Map<String, Object> model = constructSelfModel();
		model.put("warning", ALREADY_ASSOCIATED.replace('-', '_'));
		return new Viewable("/"+SELF, model);
	}

	@Path("associate/" + ASSOCIATED_TO_OTHER)
	@RequireAuth
	@Produces(MediaType.TEXT_HTML)
	@GET
	public Viewable associatedToOther() throws ApiException, ErrorPageWebApplicationException {
		Map<String, Object> model = constructSelfModel();
		model.put("warning", ASSOCIATED_TO_OTHER.replace('-', '_'));
		return new Viewable("/"+SELF, model);
	}

	@Path("associate/" + SOURCE_ALREADY_ASSOCIATED)
	@RequireAuth
	@Produces(MediaType.TEXT_HTML)
	@GET
	public Viewable sourceAlreadyAssociated() throws ApiException, ErrorPageWebApplicationException {
		Map<String, Object> model = constructSelfModel();
		model.put("warning", SOURCE_ALREADY_ASSOCIATED.replace('-', '_'));
		return new Viewable("/"+SELF, model);
	}

	@Path("removePermanentLogin")
	@RequireAuth
	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response removePermanentLogin(@FormParam("token") String token) throws ErrorPageWebApplicationException {
		AuthenticationEvent currentAuthInfo = getCurrentAuthInfo();
		// authentication to remove
		Optional<AuthenticationEvent> removeAuthInfo = authenticatedService.getAuthenticationInfo(token);

		if (currentAuthInfo == null || !removeAuthInfo.isPresent()) {
			throw wrap(new WebApplicationException("AUTH_INFOS", Response.Status.BAD_REQUEST));
		}
		if (!currentAuthInfo.getUser().getQname().isPresent() || !removeAuthInfo.get().getUser().getQname().isPresent()) {
			throw wrap(new WebApplicationException("AUTH_USERS", Response.Status.BAD_REQUEST));
		}
		if (!currentAuthInfo.getUser().getQname().get().equals(removeAuthInfo.get().getUser().getQname().get())) {
			throw wrap(new WebApplicationException("NON_MATCHING_USERS", Response.Status.BAD_REQUEST));
		}

		authenticatedService.removeLogin(token);

		return Response.seeOther(selfSavedBase().replaceQueryParam("permanentDeleted", true).build()).build();
	}

	private AuthenticationEvent getCurrentAuthInfo() {
		return sessionService.getAuthenticationEventForSelf();
	}

	@Path("deleteAccount")
	@RequireAuth
	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response deleteAccount(@FormParam("confirmation") String confirmation) throws ErrorPageWebApplicationException, ApiException, NotFoundException {
		AuthenticationEvent authInfo = getCurrentAuthInfo();
		if (authInfo == null) throw wrap(new WebApplicationException("NOT_LOGGED_IN", Response.Status.BAD_REQUEST));
		UserAttributes userInfo = userService.getUser(authInfo.getUser().getQname().get()).get();

		if (confirmation != null) confirmation = confirmation.trim();
		if (!userInfo.getName().equalsIgnoreCase(confirmation)) {
			UriBuilder redirect = uriInfo.getBaseUriBuilder().path(SELF).replaceQueryParam("deleteAccountNotConfirmed", true);
			return Response.seeOther(redirect.build()).build();
		}

		userService.deleteAccount(authInfo);
		notificationService.notifyAccountDeleted(userInfo);
		authenticatedService.removeAllLogins(authInfo.getUser().getQname().get());
		sessionService.logoutSelf();
		return Response.seeOther(uriInfo.getBaseUriBuilder().path(SELF).build()).build();
	}

}
