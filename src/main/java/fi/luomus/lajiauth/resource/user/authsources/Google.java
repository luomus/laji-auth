package fi.luomus.lajiauth.resource.user.authsources;

import static fi.luomus.lajiauth.model.Constants.NEXT_PARAMETER;
import static fi.luomus.lajiauth.model.Constants.REDIRECT_METHOD;
import static fi.luomus.lajiauth.model.Constants.TARGET_SYSTEM_PARAMETER;

import java.net.URI;

import javax.inject.Inject;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HttpMethod;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import fi.luomus.lajiauth.Utils;
import fi.luomus.lajiauth.model.AuthenticationSource;
import fi.luomus.lajiauth.model.AuthenticationSourceUser;
import fi.luomus.lajiauth.model.Constants;
import fi.luomus.lajiauth.model.OAuthState;
import fi.luomus.lajiauth.model.ServerConstants;
import fi.luomus.lajiauth.service.GoogleService;
import fi.luomus.lajiauth.service.OAuthStateService;
import fi.luomus.lajiauth.service.UserAuthenticatedService;
import fi.luomus.utils.exceptions.ApiException;

@Path(Constants.AUTH_SOURCES + "/GOOGLE/")
public class Google {

	private final static String OAUTH = "oauth";

	@Inject
	private UriInfo uriInfo;
	@Inject
	private GoogleService googleService;
	@Inject
	private OAuthStateService stateService;
	@Inject
	private UserAuthenticatedService authenticatedService;

	@GET
	public Response get(
			@QueryParam(TARGET_SYSTEM_PARAMETER) String target,
			@QueryParam(NEXT_PARAMETER) String next,
			@QueryParam(REDIRECT_METHOD) @DefaultValue(HttpMethod.POST) String redirectMethod,
			@QueryParam(ServerConstants.PERMANENT) @DefaultValue("false") boolean permanent) {

		URI redirectParam = uriInfo.getBaseUriBuilder().path(Constants.AUTH_SOURCES).path(AuthenticationSource.GOOGLE.toString()).path(OAUTH).build();
		String state = stateService.serializeState(target, next, redirectMethod, permanent);

		URI redirectUri = googleService.getRedirectUri(redirectParam, state);

		return Response.seeOther(redirectUri).build();
	}

	@Path(OAUTH)
	@GET
	public Response oauth(
			@QueryParam("state") String stateValue,
			@QueryParam("code") String code,
			@QueryParam("error") String error) throws ApiException {
		if ("access_denied".equals(error)) {
			return Response.ok(Utils.generic("auth_source_denied", "danger")).build();
		} else if (error != null) {
			throw new IllegalStateException("google oauth gave unknown error " + error);
		}
		AuthenticationSourceUser userDetails = googleService.getUserDetails(code, uriInfo.getRequestUriBuilder().replaceQuery("").build());
		OAuthState state = stateService.parseState(stateValue);

		String token = authenticatedService.getTokenForLogin(
				AuthenticationSource.GOOGLE,
				userDetails,
				state);

		URI uri = authenticatedService.getApprovalRedirect(uriInfo, token); 
		return Response.seeOther(uri).build();
	}

}
