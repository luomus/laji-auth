package fi.luomus.lajiauth.resource.user;

import static fi.luomus.lajiauth.util.ErrorPageWebApplicationException.wrap;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.HttpMethod;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import org.glassfish.jersey.server.mvc.Viewable;

import com.google.common.base.Optional;

import fi.luomus.lajiauth.Utils;
import fi.luomus.lajiauth.model.AuthenticationEvent;
import fi.luomus.lajiauth.model.AuthenticationSource;
import fi.luomus.lajiauth.model.Constants;
import fi.luomus.lajiauth.service.LanguageService;
import fi.luomus.lajiauth.service.LocalUserService;
import fi.luomus.lajiauth.service.LoginLinksService;
import fi.luomus.lajiauth.service.NotificationService;
import fi.luomus.lajiauth.service.SessionService;
import fi.luomus.lajiauth.service.SystemService;
import fi.luomus.lajiauth.service.UserAuthenticatedService;
import fi.luomus.lajiauth.service.UserService;
import fi.luomus.lajiauth.service.UserService.UserAttributes;
import fi.luomus.lajiauth.util.EmailValidator;
import fi.luomus.lajiauth.util.ErrorPageWebApplicationException;
import fi.luomus.lajiauth.util.LajiAuthPreferences;
import fi.luomus.utils.exceptions.ApiException;
import fi.luomus.utils.exceptions.NotFoundException;
import fi.luomus.utils.preferences.LuomusPreferences;

@Path("approval")
public class Approval {

	private static final String REDIRECT_METHOD = "redirectMethod";
	private static final String NEXT_URL = "nextUrl";
	public static final String EMAIL_CONFIRMED = "emailConfirmed";
	public static final String REGISTER_THANKYOU = "register-thankyou";
	public static final String TOKEN = "token";
	private static final String THANK_YOU = "submitted";
	public static final String CONFIRM = "confirm";
	private static final String CONFIRM_EXPIRED = "expired";
	private static final String ALREADY_REGISTERED = "allready-registered";
	private static final String ALREADY_CONFIRMED = "allready-confirmed";
	private static final String MUST_BE_LOGGED_IN = "must-be-logged-in";
	public static final String APPROVAL = "approval";

	@Inject
	private UriInfo uriInfo;
	@Inject
	private UserAuthenticatedService authenticatedService;
	@Inject
	private SystemService systemService;
	@Inject
	private UserService userService;
	@Inject
	private NotificationService notificationService;
	@Inject
	private LanguageService languageService;
	@Inject
	private LoginLinksService LoginLinksService;
	@Inject
	private LuomusPreferences preferences;
	@Inject
	private LocalUserService localUserService;
	@Inject
	private SessionService sessionService;

	@GET
	@Produces(MediaType.TEXT_HTML)
	public Viewable approve(
			@QueryParam(TOKEN) String token,
			@QueryParam(EMAIL_CONFIRMED) @DefaultValue("false") boolean emailConfirmed) throws ApiException, ErrorPageWebApplicationException {

		Optional<AuthenticationEvent> o = authenticatedService.getAuthenticationInfo(token);
		if (!o.isPresent()) {
			return loginExpired();
		}

		AuthenticationEvent authentication = o.get();

		validateUserId(authentication);
		validateTargetSystemExists(authentication);

		Optional<UserAttributes> userInfo = userService.findUser(authentication.getUser().getAuthSourceId(), authentication.getSource());
		if (userInfo.isPresent()) {
			if (!given(userInfo.get().getEmail())) return register(token, authentication);
			return loginSuccess(token, authentication, emailConfirmed);
		}

		if (isForSelfAssociate(authentication)) {
			// Skip registration; this login will be associated with an existing user
			return loginSuccess(token, authentication, false);
		}

		return register(token, authentication);
	}

	private Viewable register(String token, AuthenticationEvent authentication) {
		Map<String, Object> model = new HashMap<>();
		model.put(TOKEN, token);
		model.put("authentication", authentication);
		return new Viewable("/register", model);
	}

	private Viewable loginSuccess(String token, AuthenticationEvent authentication, boolean emailConfirmed) throws ApiException {
		Map<String, Object> model = new HashMap<>();
		model.put(TOKEN, token);

		Optional<URI> targetLoginURI = systemService.getSystemRedirectURI(authentication.getTarget());
		if (!targetLoginURI.isPresent()) {
			throw new WebApplicationException("TARGET_SYSTEM_LOGIN_URL_NOT_FOUND", Response.Status.BAD_REQUEST);
		}

		boolean autosubmit = !emailConfirmed;

		if (isForSelfAssociate(authentication)) {
			model.put(NEXT_URL, systemService.getLajiAuthBaseURI()+authentication.getNext());
			model.put(REDIRECT_METHOD, HttpMethod.POST);
			autosubmit = true;
		} else {
			model.put(NEXT_URL, targetLoginURI.get().toString());
			model.put(REDIRECT_METHOD, authentication.getRedirectMethod());
			model.put(Constants.NEXT_PARAMETER, authentication.getNext());
		}

		model.put(EMAIL_CONFIRMED, emailConfirmed);
		model.put("autosubmit", autosubmit);

		if (!autosubmit) {
			String name = authentication.getUser().getName();
			if (authentication.getUser().getGroup().isPresent()) {
				name += " (" + authentication.getUser().getGroup().get() + ")";
			}
			model.put("person_name", name);
			model.put("person_id", authentication.getUser().getQname().or(""));
			String locale = languageService.getLanguage();
			String systemName = systemService.getSystemName(authentication.getTarget(), locale).get().or(authentication.getTarget());
			model.put("system_name", systemName);
		}

		return new Viewable("/login-success", model);
	}

	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response register(
			@FormParam("email") String email,
			@FormParam("preferredName") String preferredName,
			@FormParam("inheritedName") String inheritedName,
			@FormParam("group") String group,
			@FormParam(TOKEN) String token) throws ApiException, ErrorPageWebApplicationException {

		if (email != null) email = email.trim().toLowerCase();
		if (preferredName != null) preferredName = preferredName.trim();
		if (inheritedName != null) inheritedName = inheritedName.trim();
		if (group != null) {
			group = group.trim();
			if (group.isEmpty()) {
				group = null;
			}
		}

		validateEmail(email);
		validateUserName(preferredName, inheritedName);

		AuthenticationEvent authentication = authenticatedService.getAuthenticationInfoOrFail(token);
		validateUserId(authentication);
		validateTargetSystemExists(authentication);
		validateNotAlreadyAttachedToUser(authentication);

		Optional<UserAttributes> userInfo = userService.findUser(email);
		if (userInfo.isPresent()) {
			if (emailChanged(email, authentication)) {
				authentication.getUser().setEmail(email);
				token = authenticatedService.getTokenForLogin(authentication);
			}
			return Response.seeOther(emailAlreadyUsedURI(token)).build();
		}
		String fullname = preferredName + " " + inheritedName;
		authentication.getUser().setPreferredName(Optional.of(preferredName));
		authentication.getUser().setInheritedName(Optional.of(inheritedName));
		authentication.getUser().setName(fullname);
		authentication.getUser().setEmail(email);
		authentication.getUser().setGroup(Optional.fromNullable(group));
		token = authenticatedService.getTokenForLogin(authentication);

		URI confirmURI = authenticatedService.getConfirmLinkURI(uriInfo, token);
		String locale = languageService.getLanguage();
		notificationService.notifyEmailConfirmationRequest(email, fullname, locale, confirmURI);

		return Response.seeOther(thankYouURI()).build();
	}

	private boolean emailChanged(String email, AuthenticationEvent authentication) {
		return !email.equalsIgnoreCase(authentication.getUser().getEmail());
	}

	private void validateUserId(AuthenticationEvent authentication) throws ErrorPageWebApplicationException {
		if (authentication.getUser().getAuthSourceId() == null) {
			throw wrap(new WebApplicationException("AUTHSOURCE_USER_NOT_GIVEN", Response.Status.BAD_REQUEST));
		}
	}

	private void validateTargetSystemExists(AuthenticationEvent authentication) throws ApiException, ErrorPageWebApplicationException {
		if (!systemService.exists(authentication.getTarget())) {
			throw wrap(new WebApplicationException("TARGET_SYSTEM_NOT_FOUND", Response.Status.BAD_REQUEST));
		}
	}

	private void validateUserName(String preferredName, String inheritedName) throws ErrorPageWebApplicationException {
		if (!given(preferredName) || !given(inheritedName)) {
			throw wrap(new WebApplicationException("BAD_FULLNAME", Response.Status.BAD_REQUEST));
		}
	}

	private void validateEmail(String email) throws ErrorPageWebApplicationException {
		if (!EmailValidator.validEmail(email)) {
			throw wrap(new WebApplicationException("BAD_EMAIL", Response.Status.BAD_REQUEST));
		}
	}

	private void validateNotAlreadyAttachedToUser(AuthenticationEvent authentication) throws ErrorPageWebApplicationException, ApiException {
		boolean isPresent = userService.findUser(authentication.getUser().getAuthSourceId(), authentication.getSource()).isPresent();
		if (isPresent) {
			throw wrap(new WebApplicationException("USER_ALREADY_EXISTS", Response.Status.BAD_REQUEST));
		}
	}

	private URI thankYouURI() {
		return uriInfo.getRequestUriBuilder().replaceQuery("").path(THANK_YOU).build();
	}

	private URI approvalURI(String token, boolean confirmEmailChange) {
		UriBuilder uri = uriInfo.getBaseUriBuilder().path(APPROVAL).replaceQueryParam(TOKEN, token);
		if (confirmEmailChange) {
			uri.replaceQueryParam(EMAIL_CONFIRMED, true);
		}
		return uri.build();
	}

	private URI mustBeLoggedInURI() {
		return uriInfo.getBaseUriBuilder().path(APPROVAL).path(MUST_BE_LOGGED_IN).build();
	}

	private URI alreadyConfirmedURI() {
		return uriInfo.getBaseUriBuilder().path(APPROVAL).path(ALREADY_CONFIRMED).build();
	}

	private URI confirmExpiredURI() {
		return uriInfo.getBaseUriBuilder().path(APPROVAL).path(CONFIRM_EXPIRED).build();
	}

	private URI emailAlreadyUsedURI(String token) {
		return uriInfo.getBaseUriBuilder().path(APPROVAL).path(ALREADY_REGISTERED).replaceQueryParam(TOKEN, token).build();
	}

	@GET
	@Path(CONFIRM_EXPIRED)
	@Produces(MediaType.TEXT_HTML)
	public Viewable confirmExpired() {
		return Utils.generic("confirm-expired", "danger");
	}

	public Viewable alreadyLogged() {
		return Utils.generic("already-logged", "success");
	}

	public Viewable loginExpired() {
		return Utils.generic("login-expired", "danger");
	}

	@GET
	@Path(MUST_BE_LOGGED_IN)
	@Produces(MediaType.TEXT_HTML)
	public Viewable mustBeLoggedIn() {
		return Utils.generic("must-be-logged-in", "danger");
	}

	@GET
	@Path(ALREADY_CONFIRMED)
	@Produces(MediaType.TEXT_HTML)
	public Viewable alreadyConfirmed() {
		return Utils.generic("already-confirmed", "danger");
	}

	@GET
	@Path(THANK_YOU)
	@Produces(MediaType.TEXT_HTML)
	public Viewable thankYou() {
		return Utils.generic(REGISTER_THANKYOU, "success");
	}

	@GET
	@Path(ALREADY_REGISTERED)
	@Produces(MediaType.TEXT_HTML)
	public Viewable alreadyRegistered(@QueryParam(TOKEN) String token) throws ApiException, ErrorPageWebApplicationException {
		AuthenticationEvent authentication = authenticatedService.getAuthenticationInfoOrFail(token);
		Optional<UserAttributes> user = userService.findUser(authentication.getUser().getEmail());
		if (!user.isPresent()) throw wrap(new WebApplicationException("USER_NOT_FOUND_AFTER_ALL", Response.Status.BAD_REQUEST));

		List<AuthenticationSource> associations = getAssociations(user);

		String lajiAuthSystemId = preferences.get(LajiAuthPreferences.LAJI_AUTH_SYSTEM_ID).get();
		Map<String, URI> loginLinks = LoginLinksService.sourceURIs(uriInfo, lajiAuthSystemId, "/"+Self.SELF, "POST", associations);

		Map<String, Object> model = new HashMap<>();
		model.put("authentication", authentication);
		model.put("associations", associations);
		model.put("loginLinks", loginLinks);
		return new Viewable("/registered-already", model);
	}

	private List<AuthenticationSource> getAssociations(Optional<UserAttributes> user) throws ApiException {
		Optional<Map<AuthenticationSource, String>> sourcesMap = userService.getAssociations(user.get().getId());
		List<AuthenticationSource> associations = new ArrayList<>();
		if (sourcesMap.isPresent()) associations.addAll(sourcesMap.get().keySet());
		return associations;
	}

	@GET
	@Path(CONFIRM)
	@Produces(MediaType.TEXT_HTML)
	public Response confirm(@QueryParam(TOKEN) String token) throws ErrorPageWebApplicationException, ApiException, NotFoundException {

		Optional<AuthenticationEvent> o = authenticatedService.getAuthenticationInfo(token);
		if (!o.isPresent()) {
			return Response.seeOther(confirmExpiredURI()).build();
		}
		AuthenticationEvent authentication = o.get();
		validateEmail(authentication.getUser().getEmail());
		validateUserName(authentication.getUser().getPreferredName().or(""), authentication.getUser().getInheritedName().or(""));
		validateUserId(authentication);
		validateTargetSystemExists(authentication);

		Optional<UserAttributes> currentUserAttributes = userService.findUser(authentication.getUser().getAuthSourceId(), authentication.getSource());

		if (currentUserAttributes.isPresent()) {
			String currentEmail = currentUserAttributes.get().getEmail();
			if (!currentEmail.equalsIgnoreCase(authentication.getUser().getEmail()) && authentication.getUser().getQname().isPresent()) {
				// Confirming email change for existing user
				userService.changeEmail(authentication, currentEmail);
				authentication.getUser().getPreviousEmailAddresses().add(currentEmail);
				if (authentication.getSource() == AuthenticationSource.LOCAL) {
					authentication.getUser().setAuthSourceId(authentication.getUser().getEmail().trim().toLowerCase());
				}
				token = authenticatedService.getTokenForLogin(authentication);
				return Response.seeOther(approvalURI(token, true)).build();
			}
			return Response.seeOther(alreadyConfirmedURI()).build();
		}

		if (isConfirmLocalUser(authentication)) {
			localUserService.confirm(authentication.getUser().getEmail());
		}

		if (isForSelfAssociate(authentication)) {
			// Skip creating a new user; this login will be associated with an existing user
			if (notLoggedIn()) {
				return Response.seeOther(mustBeLoggedInURI()).build();
			}
			return Response.seeOther(approvalURI(token, false)).build();
		}

		Optional<UserAttributes> userInfo = userService.findUser(authentication.getUser().getEmail());
		if (userInfo.isPresent()) {
			return Response.seeOther(emailAlreadyUsedURI(token)).build();
		}
		String locale = languageService.getLanguage();
		String id = userService.createUser(authentication, locale);
		authentication.getUser().setQname(Optional.of(id));
		token = authenticatedService.getTokenForLogin(authentication);
		return Response.seeOther(approvalURI(token, false)).build();
	}

	private boolean notLoggedIn() {
		return !sessionService.isAthenticatedForSelf();
	}

	private boolean isConfirmLocalUser(AuthenticationEvent authentication) {
		return authentication.getSource() == AuthenticationSource.LOCAL;
	}

	private boolean isForSelfAssociate(AuthenticationEvent authentication) {
		if (!isForLajiAuth(authentication)) return false;
		String next = authentication.getNext();
		if (next == null) return false;
		return next.contains(Self.SELF_ASSOCIATE);
	}

	private boolean isForLajiAuth(AuthenticationEvent authentication) {
		return authentication.getTarget().equals(preferences.get(LajiAuthPreferences.LAJI_AUTH_SYSTEM_ID).get());
	}

	private boolean given(String email) {
		return email != null && email.length() > 0;
	}

}
