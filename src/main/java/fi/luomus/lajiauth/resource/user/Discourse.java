package fi.luomus.lajiauth.resource.user;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.regex.Pattern;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;
import org.springframework.web.util.UriUtils;

import fi.luomus.lajiauth.Utils;
import fi.luomus.lajiauth.model.AuthenticationEvent;
import fi.luomus.lajiauth.service.LajiAuthClient;
import fi.luomus.lajiauth.service.SystemService;
import fi.luomus.lajiauth.util.LajiAuthPreferences;
import fi.luomus.utils.exceptions.ApiException;
import fi.luomus.utils.preferences.LuomusPreferences;

@Path("discourse")
public class Discourse {

	@Inject
	private LuomusPreferences preferences;
	@Inject
	private SystemService systemService;

	/**
	 * First the resource the user is redirected here by Discourse SSO. 
	 * Redirect the user to Laji-Auth login portal from where the user is redirected
	 * to POST
	 */
	@GET
	public Response first(@QueryParam("sso") String payload, @QueryParam("sig") String sig) {
		String decodedPayload = getDecodedPayload(payload);
		String ourSig = hmacSHA256(payload);

		if (!ourSig.equals(sig)) throw new WebApplicationException(Response.Status.BAD_REQUEST);

		String nonce = parseNonce(decodedPayload);

		return Response.seeOther(createLajiAuthLoginRedirectUrl(nonce)).build();
	}

	private URI createLajiAuthLoginRedirectUrl(String nonce) {
		MultivaluedMap<String, String> params = new MultivaluedHashMap<>();
		params.putSingle("nonce", nonce);
		return getLajiAuthClient().createLoginUrl().nextParameter("", params).build();
	}

	private LajiAuthClient getLajiAuthClient() {
		String systemId = preferences.get(LajiAuthPreferences.DISCOURSE_SYSTEM_ID).get();
		return Utils.getLajiAuthClient(systemId, systemService);
	}

	private String parseNonce(String decodedPayload) {
		return decodedPayload.split(Pattern.quote("&"))[0].replace("nonce=", "");
	}

	private String getDecodedPayload(String payload) {
		try {
			return new String(Base64.decodeBase64(payload), "UTF-8");
		} catch (Exception e) {
			throw new WebApplicationException(Response.Status.BAD_REQUEST);
		}
	}

	/**
	 * After authenticating with Laji-auth redirect the user to discourse sso login page
	 * with her name and email
	 *
	 * @param tokenJSON Laji-auth authentication token
	 * @return Redirect to discourse sso login page
	 */
	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response second(@FormParam("token") String token) throws ApiException, IOException {
		AuthenticationEvent authenticationInfo = getLajiAuthClient().getAndValidateAuthenticationInfo(token);

		boolean userPresent = authenticationInfo.getUser().getQname().isPresent();
		if (!userPresent) {
			throw new WebApplicationException(Response.Status.BAD_REQUEST);
		}

		String payload = generatePayload(authenticationInfo);
		String encodedPayload = Base64.encodeBase64String(payload.getBytes("UTF-8"));
		String signedPayload = hmacSHA256(encodedPayload);

		URI redirectUri = UriBuilder.fromUri(discourseURL())
				.path("session")
				.path("sso_login")
				.queryParam("sso", encodedPayload)
				.queryParam("sig", signedPayload)
				.build();
		return Response.seeOther(redirectUri).build();
	}

	private String discourseURL() {
		return preferences.get(LajiAuthPreferences.DISCOURSE_URL).get();
	}

	private String generatePayload(AuthenticationEvent authenticationInfo) {
		// nonce=cb68251eefb5211e58c00ff1395f0c0b&name=sam&username=samsam&email=test%40test.com&external_id=hello123&require_activation=true
		// see https://meta.discourse.org/t/official-single-sign-on-for-discourse/13045
		String nonce = parseNonceFromNextPath(authenticationInfo.getNext());
		String userId = authenticationInfo.getUser().getQname().get();
		String email = authenticationInfo.getUser().getEmail();
		String fullName = authenticationInfo.getUser().getName();
		String payload = new Payload().add("nonce", nonce).add("external_id", userId).add("email", email).add("name", fullName).build();
		return payload;
	}

	private String hmacSHA256(String base64EncodedPayload) {
		return hmacSHA256(base64EncodedPayload, secret());
	}

	private String secret() {
		return preferences.get(LajiAuthPreferences.DISCOURSE_SSO_TOKEN).get();
	}

	private String hmacSHA256(String data, String key) {
		try {
			SecretKeySpec secretKey = new SecretKeySpec(key.getBytes("UTF-8"), "HmacSHA256");
			Mac mac = Mac.getInstance("HmacSHA256");
			mac.init(secretKey);
			byte[] hmacData = mac.doFinal(data.getBytes("UTF-8"));
			return Hex.encodeHexString(hmacData);
		} catch (Exception e) {
			throw new WebApplicationException(Response.Status.BAD_REQUEST);
		}
	}

	private String parseNonceFromNextPath(String next) {
		// discourse?nonce=ec0e40266f624caxxxx54643697b1316319
		String nonce = next.split(Pattern.quote("?"))[1].replace("nonce=", ""); 
		return nonce;
	}

	@Path("logout")
	@GET
	public Response logout() throws URISyntaxException {
		return Response.seeOther(new URI(discourseURL())).build();
	}

	private static class Payload {
		private final StringBuilder b = new StringBuilder();

		public Payload add(String field, String value) {
			if (value == null || value.trim().length() < 1) return this;
			if (b.length() != 0) b.append("&");
			try {
				b.append(field).append("=").append(UriUtils.encodeQueryParam(value, "UTF-8"));
			} catch (UnsupportedEncodingException e) {
				throw new RuntimeException(e);
			}
			return this;
		}

		public String build() {
			return b.toString();
		}
	}

}
