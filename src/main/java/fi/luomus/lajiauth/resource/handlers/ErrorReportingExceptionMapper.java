package fi.luomus.lajiauth.resource.handlers;

import static org.slf4j.LoggerFactory.getLogger;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

import org.glassfish.hk2.api.ServiceLocator;
import org.glassfish.jersey.server.mvc.Viewable;
import org.glassfish.jersey.spi.ExtendedExceptionMapper;
import org.slf4j.Logger;

import fi.luomus.lajiauth.model.BaseErrorEntity;
import fi.luomus.lajiauth.service.ModelService;
import fi.luomus.lajiauth.util.LajiAuthPreferences;
import fi.luomus.utils.preferences.LuomusPreferences;
import fi.luomus.utils.service.ErrorReporterService;


@Provider
public class ErrorReportingExceptionMapper implements ExtendedExceptionMapper<Exception> {

	@Context
	private HttpServletRequest request;
	@Context
	private ServiceLocator locator;
	@Inject
	private LuomusPreferences preferences;
	@Inject
	private ErrorReporterService errorReporterService;
	@Inject
	private ModelService modelService;
	private Logger logger = getLogger(ErrorReportingExceptionMapper.class);

	@Override
	public Response toResponse(final Exception exception) {
		try {
			errorReporterService.report(exception, request);
		} catch (Exception e) {
			logger.error("unable to report exception", exception);
			logger.error("Caused by", e);
			throw new RuntimeException(e);
		}
		logger.warn("Reported exception", exception);
		Response.ResponseBuilder status = Response.status(Response.Status.INTERNAL_SERVER_ERROR);
		if (locator.getService(ContainerRequestContext.class).getAcceptableMediaTypes().contains(MediaType.TEXT_HTML_TYPE)) {
			status = status.type(MediaType.TEXT_HTML).entity(new Viewable("/error", modelService.getModel("error")));
		} else {
			status = status.type(MediaType.APPLICATION_JSON).entity(new BaseErrorEntity("INTERNAL_ERROR"));
		}

		return status.build();
	}

	@Override
	public boolean isMappable(Exception exception) {
		Boolean devMode = preferences.getBoolean(LajiAuthPreferences.DEV_MODE).or(Boolean.TRUE);

		// skip error reporting if webapplicationexception (e.g. regular 404 error) or in dev mode
		return !(exception instanceof WebApplicationException || devMode);
	}

}
