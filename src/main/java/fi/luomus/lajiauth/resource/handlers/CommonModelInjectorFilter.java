package fi.luomus.lajiauth.resource.handlers;

import java.io.IOException;
import java.util.Map;

import javax.inject.Inject;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.ext.Provider;

import org.glassfish.jersey.server.mvc.Viewable;

import fi.luomus.lajiauth.service.ModelService;

/**
 * Injects template globals using the current request context
 */
@Provider
public class CommonModelInjectorFilter implements ContainerResponseFilter {
	
    @Inject
    private ModelService modelService;

    @Override
    public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext) throws IOException {
        if (responseContext.getEntity() instanceof Viewable) {
            Viewable entity = (Viewable) responseContext.getEntity();
            Map<String, Object> commonModel = modelService.getModel(entity.getTemplateName());
            if (entity.getModel() instanceof Map) {
                @SuppressWarnings("unchecked")
				Map<String, Object> map = (Map<String, Object>) entity.getModel();
                map.putAll(commonModel);
            } else if (entity.getModel() == null) {
                responseContext.setEntity(new Viewable(entity.getTemplateName(), commonModel));
            }
        }
    }
    
}
