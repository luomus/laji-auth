package fi.luomus.lajiauth.resource.handlers;

import java.io.IOException;

import javax.inject.Inject;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.ext.Provider;

import fi.luomus.lajiauth.model.Constants;
import fi.luomus.lajiauth.model.ServerConstants;
import fi.luomus.lajiauth.service.LanguageService;

@Provider
public class LocalizationRequestFilter implements ContainerRequestFilter {

	@Inject 
	private LanguageService languageService; 

	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException {
		String language = requestContext.getUriInfo().getQueryParameters().getFirst(Constants.LANGUAGE_PARAMETER);
		if (valid(language)) {
			languageService.setLanguage(language);
			return;
		}
		language = requestContext.getUriInfo().getQueryParameters().getFirst("locale");
		if (valid(language)) {
			languageService.setLanguage(language);
		}
	}

	private boolean valid(String language) {
		return language != null && ServerConstants.ALLOWED_LANGUAGES.contains(language);
	}

}
