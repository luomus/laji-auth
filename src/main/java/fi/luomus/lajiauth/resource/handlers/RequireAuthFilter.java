package fi.luomus.lajiauth.resource.handlers;

import java.io.IOException;
import java.net.URI;

import javax.annotation.Priority;
import javax.inject.Inject;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.springframework.context.ApplicationContext;

import fi.luomus.lajiauth.service.LajiAuthClient;
import fi.luomus.lajiauth.service.SessionService;
import fi.luomus.lajiauth.service.SystemService;

@RequireAuth
@Priority(Priorities.AUTHENTICATION)
public class RequireAuthFilter implements ContainerRequestFilter {

	@Inject
	private ApplicationContext applicationContext;
	@Inject
	private UriInfo uriInfo;
	@Inject
	private SystemService systemService;
	@Inject
	SessionService sessionService;

	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException {
		if (isAuthenticated()) return;
		String nextParam = uriInfo.getRequestUri().toString().replace(systemService.getLajiAuthBaseURI().toString(), "");
		URI redirect = applicationContext.getBean(LajiAuthClient.class).createLoginUrl(nextParam).build();
		requestContext.abortWith(Response.seeOther(redirect).build());
	}

	private boolean isAuthenticated() {
		return sessionService.isAthenticatedForSelf();
	}

}
