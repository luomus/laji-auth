package fi.luomus.lajiauth.resource.handlers;

import java.util.Map;

import javax.inject.Inject;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.glassfish.jersey.server.mvc.Viewable;

import fi.luomus.lajiauth.service.ModelService;
import fi.luomus.lajiauth.util.ErrorPageWebApplicationException;

@Provider
public class WebApplicationExceptionMapper implements ExceptionMapper<ErrorPageWebApplicationException> {

	@Inject
	private ModelService modelService;

	@Override
	public Response toResponse(ErrorPageWebApplicationException exception) {
		Map<String, Object> viewModel = modelService.getModel("error");
		viewModel.put("model", exception);
		return Response.status(exception.cause.getResponse().getStatus())
				.entity(new Viewable("/error", viewModel)).build();
	}

}
