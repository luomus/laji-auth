package fi.luomus.lajiauth.resource.handlers;

import java.io.IOException;

import javax.annotation.Priority;
import javax.inject.Inject;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;

import fi.luomus.lajiauth.service.SessionService;

@Priority(Priorities.AUTHENTICATION)
public class AuthenticationFilter implements ContainerRequestFilter {

	@Inject 
	private SessionService sessionService;

	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException {
		sessionService.authenticateContainerRequestForSelf(requestContext);
	}

}
