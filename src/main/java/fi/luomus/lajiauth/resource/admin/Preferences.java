package fi.luomus.lajiauth.resource.admin;

import java.lang.reflect.Field;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import org.glassfish.jersey.server.mvc.Viewable;

import com.google.common.base.Optional;

import fi.luomus.lajiauth.util.LajiAuthPreferences;
import fi.luomus.utils.preferences.LuomusPreferences;
import fi.luomus.utils.preferences.Preference;

@Path("admin/preferences")
public class Preferences {

	@Inject
	private UriInfo uriInfo;
	@Inject
	private LuomusPreferences preferences;

	@GET
	@Produces(MediaType.TEXT_HTML)
	public Viewable get(@QueryParam("saved") @DefaultValue("false") Boolean saved) {
		Map<String, Object> modelWithPreferences = getModelWithPreferences();
		modelWithPreferences.put("saved", saved);

		return new Viewable("/preferences", modelWithPreferences);
	}

	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response update(MultivaluedMap<String, String> formParams) {
		for (String paramName : formParams.keySet()) {
			String paramValue = formParams.getFirst(paramName);
			Preference preference = new Preference(paramName);
			if (paramValue.length() > 0) {
				preferences.set(preference, paramValue);
			} else {
				preferences.remove(preference);
			}
		}
		URI redirect = UriBuilder.fromUri(uriInfo.getRequestUri()).queryParam("saved", true).build();

		return Response.seeOther(redirect).build();
	}

	private Map<String, Object> getModelWithPreferences() {
		Map<String, Object> model = new HashMap<>();
		Map<String, String> result = new TreeMap<>();

		for (Field field : LajiAuthPreferences.class.getDeclaredFields()) {
			try {
				Object fieldValue = field.get(null);
				if (fieldValue instanceof Preference) {
					Preference preference = (Preference) fieldValue;
					Optional<String> value = preferences.get(preference);
					result.put(preference.key, value.isPresent() ? value.get() : "");
				}
			} catch (IllegalAccessException e) {
				throw new RuntimeException(e);
			}
			model.put("preferences", result);
		}

		return model;
	}

}
