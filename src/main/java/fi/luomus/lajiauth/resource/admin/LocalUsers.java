package fi.luomus.lajiauth.resource.admin;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.glassfish.jersey.server.mvc.Viewable;

import fi.luomus.lajiauth.model.LocalUser;
import fi.luomus.lajiauth.repository.LocalUserRepository;
import fi.luomus.lajiauth.resource.handlers.RequireAuth;
import fi.luomus.lajiauth.service.SessionService;

@Path("/admin/users")
public class LocalUsers {

	private final static String DELETE = "delete";

	@Inject
	private UriInfo uriInfo;
	@Inject
	private LocalUserRepository localUserRepository;
	@Inject
	private SessionService sessionService;

	@GET
	@Produces(MediaType.TEXT_HTML)
	@RequireAuth
	@RolesAllowed("MA.admin")
	public Response listUsers() {
		Map<String, Object> model = new HashMap<>();
		model.put("users", localUserRepository.getAll());
		model.put("deleteAction", DELETE);
		model.put("logoutUri", "/laji-auth/admin/users/logout");

		return Response.ok(new Viewable("/local/users", model)).build();
	}

	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@RequireAuth
	@RolesAllowed("MA.admin")
	public Response listUsers(
			@FormParam("email") String email,
			@FormParam("action") String action) {
		if (email == null || action == null) {
			throw new WebApplicationException(Response.Status.BAD_REQUEST);
		}
		email = email.toLowerCase().trim();
		LocalUser localUser = localUserRepository.find(email);
		if (localUser == null) {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}

		URI uri;
		switch (action) {
		case DELETE:
			localUserRepository.delete(email);
			uri = uriInfo.getRequestUriBuilder().replaceQueryParam("deleted", email).build();
			break;
		default:
			throw new RuntimeException(String.format("unknown action %s", action));
		}
		return Response.seeOther(uri).build();
	}

	@Path("logout")
	@GET
	public Response logout()  {
		sessionService.logoutSelf();
		return Response.seeOther(uriInfo.getBaseUriBuilder().path("admin").path("users").build()).build();
	}

}
