package fi.luomus.lajiauth.resource.admin;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import fi.luomus.lajiauth.service.EmailStore;
import fi.luomus.lajiauth.util.LajiAuthPreferences;
import fi.luomus.utils.preferences.LuomusPreferences;

@Path("admin/emails")
public class Emails {

	@Inject
	private EmailStore emailStore;
	@Inject
	private LuomusPreferences luomusPreferences;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<EmailStore.Email> emails() {
		if (luomusPreferences.getBoolean(LajiAuthPreferences.DEV_MODE).or(false)) {
			return emailStore.getEmails();
		}
		throw new WebApplicationException(Response.Status.NOT_FOUND);
	}

}
