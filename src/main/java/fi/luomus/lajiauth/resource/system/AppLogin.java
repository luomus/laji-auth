package fi.luomus.lajiauth.resource.system;

import static fi.luomus.lajiauth.util.ErrorPageWebApplicationException.wrap;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.glassfish.jersey.server.mvc.Viewable;

import com.google.common.base.Optional;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;

import fi.luomus.lajiauth.Utils;
import fi.luomus.lajiauth.model.AuthenticationEvent;
import fi.luomus.lajiauth.model.BaseErrorEntity;
import fi.luomus.lajiauth.service.AccessTokenService;
import fi.luomus.lajiauth.service.LajiAuthClient;
import fi.luomus.lajiauth.service.SystemService;
import fi.luomus.lajiauth.service.UserAuthenticatedService;
import fi.luomus.lajiauth.util.ErrorPageWebApplicationException;
import fi.luomus.utils.exceptions.ApiException;

@Path(AppLogin.APP_LOGIN)
public class AppLogin {

	private static final String SUCCESS = "success";

	public class TokenAndEvent {
		private final String token;
		private final AuthenticationEvent event;
		public TokenAndEvent(String token, AuthenticationEvent event) {
			this.token = token;
			this.event = event;
		}
	}

	protected static final String APP_LOGIN = "app-login";
	public static final String TMP_TOKEN = "tmpToken";

	@Inject
	private AccessTokenService accessTokenService;
	@Inject
	private UserAuthenticatedService authenticatedService;
	@Inject
	private SystemService systemService;
	@Inject
	private UriInfo uriInfo;

	private final static Cache<String, TokenAndEvent> logEventCache = CacheBuilder.newBuilder().expireAfterWrite(3, TimeUnit.MINUTES).build();
	private final static Cache<String, String> generatedTokensCache = CacheBuilder.newBuilder().expireAfterWrite(30, TimeUnit.MINUTES).build();
	private final static Cache<String, Boolean> recentlyApprovedTokes = CacheBuilder.newBuilder().expireAfterWrite(30, TimeUnit.MINUTES).build();

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response first(@QueryParam("access_token") String accessToken) throws ApiException {
		String systemId = getAndValidateSystemId(accessToken);

		String tmpToken = "tmp_" + Utils.generateToken();
		URI loginUri = createLajiAuthLoginRedirectUrl(tmpToken, systemId);
		generatedTokensCache.put(tmpToken, systemId);

		Map<String, String> response = new HashMap<>();
		response.put(TMP_TOKEN, tmpToken);
		response.put("loginURL", loginUri.toString());
		return Response.status(Response.Status.OK).entity(response).build();
	}

	private URI createLajiAuthLoginRedirectUrl(String tmpToken, String systemId) {
		MultivaluedMap<String, String> params = new MultivaluedHashMap<>();
		params.putSingle(TMP_TOKEN, tmpToken);
		LajiAuthClient client = getLajiAuthClient(systemId);
		return client.createLoginUrl().nextParameter("", params).build();
	}

	/**
	 * After authenticating with Laji-auth store auth info for tmpToken and
	 * with her name and email
	 *
	 * @param tokenJSON Laji-auth authentication token
	 * @return Redirect to discourse sso login page
	 * @throws ErrorPageWebApplicationException
	 */
	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.TEXT_HTML)
	public Response second(@FormParam("token") String token) throws ErrorPageWebApplicationException {
		if (notGiven(token)) {
			throw wrap(new WebApplicationException("TOKEN MISSING", Response.Status.BAD_REQUEST));
		}

		if (Boolean.TRUE.equals(recentlyApprovedTokes.getIfPresent(token))) {
			return success();
		}

		AuthenticationEvent authenticationEvent = null;
		try {
			authenticationEvent = authenticatedService.getAuthenticationInfoOrFail(token);
		} catch (Exception e) {
			throw error("INVALID TOKEN");
		}

		boolean userPresent = authenticationEvent.getUser().getQname().isPresent();
		if (!userPresent) {
			throw error("USER NOT PRESENT");
		}

		String tmpToken = parseTempToken(authenticationEvent);
		if (notGiven(tmpToken)) {
			throw error("TMP TOKEN MISSING");
		}

		if (Boolean.TRUE.equals(recentlyApprovedTokes.getIfPresent(tmpToken))) {
			return success();
		}

		String tokenSystemId = generatedTokensCache.getIfPresent(tmpToken);
		if (tokenSystemId == null) {
			throw error("INVALID TMP TOKEN");
		}
		if (!authenticationEvent.getTarget().equals(tokenSystemId)) {
			throw error("WRONG TARGET");
		}

		generatedTokensCache.invalidate(tmpToken);

		authenticationEvent.setNext("");
		authenticationEvent.setRedirectMethod("");
		authenticationEvent.setPermanent(true);
		String actualToken = authenticatedService.getTokenForLogin(authenticationEvent);

		logEventCache.put(tmpToken, new TokenAndEvent(actualToken, authenticationEvent));

		authenticatedService.removeLogin(token);

		recentlyApprovedTokes.put(token, true);
		recentlyApprovedTokes.put(tmpToken, true);

		return success();
	}

	private ErrorPageWebApplicationException error(String message) {
		return wrap(new WebApplicationException(message, Response.Status.BAD_REQUEST));
	}

	private Response success() {
		return Response.seeOther(uriInfo.getBaseUriBuilder().path(APP_LOGIN).path(SUCCESS).build()).build();
	}

	@GET
	@Path(SUCCESS)
	@Produces(MediaType.TEXT_HTML)
	public Viewable successPage() {
		return Utils.generic("app-login-success", "success");
	}

	private String parseTempToken(AuthenticationEvent authenticationEvent) {
		String next = authenticationEvent.getNext();
		return parseTempToken(next);
	}

	public static String parseTempToken(String next) {
		if (next == null) return "";
		String param = TMP_TOKEN+"=";
		if (!next.contains(param)) return "";
		next = next.substring(next.indexOf(param)+param.length());
		if (next.contains("&")) next = next.substring(0, next.indexOf("&"));
		return next;
	}

	@POST
	@Path("check")
	@Produces(MediaType.APPLICATION_JSON)
	public Response third(@QueryParam("access_token") String accessToken, @QueryParam("tmpToken") String tmpToken) throws ApiException {
		String systemId = getAndValidateSystemId(accessToken);

		if (notGiven(tmpToken)) {
			throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST)
					.entity(new BaseErrorEntity("TMP_TOKEN_REQUIRED")).build());
		}

		TokenAndEvent data = logEventCache.getIfPresent(tmpToken);

		if (data == null) {
			return Response.status(404).entity(new BaseErrorEntity("NO_SUCCESFUL_LOGIN_YET")).build();
		}

		logEventCache.invalidate(tmpToken);
		AuthenticationEvent event = data.event;

		if (!systemId.equals(event.getTarget())) {
			authenticatedService.removeLogin(data.token);
			throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST)
					.entity(new BaseErrorEntity("SYSTEM_MISSMATCH")).build());
		}

		Map<String, String> map = new HashMap<>();
		map.put("token", data.token);
		return Response.ok().entity(map).build();
	}

	private String getAndValidateSystemId(String accessToken) throws ApiException {
		if (notGiven(accessToken)) {
			throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST)
					.entity(new BaseErrorEntity("ACCESS_TOKEN_REQUIRED")).build());
		}

		String systemId = accessTokenService.getSystemId(accessToken);
		if (notGiven(systemId)) {
			throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST)
					.entity(new BaseErrorEntity("ACCESS_TOKEN_NOT_LINKED_TO_SYSTEM")).build());
		}

		Optional<URI> uri = systemService.getSystemRedirectURI(systemId);
		if (!uri.isPresent()) {
			throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST)
					.entity(new BaseErrorEntity("REDIRECT_URI_NOT_DEFINED")).build());
		}

		if (!uri.get().toString().endsWith(APP_LOGIN)) {
			throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST)
					.entity(new BaseErrorEntity("REDIRECT_URI_NOT_DEFINED_TO_APP_LOGIN")).build());
		}

		return systemId;
	}

	private LajiAuthClient getLajiAuthClient(String systemId) {
		return Utils.getLajiAuthClient(systemId, systemService);
	}

	private boolean notGiven(String s) {
		return s == null || s.trim().isEmpty();
	}

}
