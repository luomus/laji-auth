package fi.luomus.lajiauth.resource.system;

import static org.slf4j.LoggerFactory.getLogger;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;

import fi.luomus.lajiauth.model.AuthenticationEvent;
import fi.luomus.lajiauth.model.BaseErrorEntity;
import fi.luomus.lajiauth.service.UserAuthenticatedService;
import fi.luomus.utils.service.ErrorReporterService;

@Path("token")
public class Token {

	@Inject
	private UserAuthenticatedService userAuthenticatedService;
	@Inject
	private ErrorReporterService errorReporterService;
	@Context
	private HttpServletRequest request;

	private final Logger logger = getLogger(Token.class);

	@GET
	@Path("{token}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response get(@PathParam("token") String token) throws WebApplicationException {

		removeExpiredLogins();

		if (notGiven(token)) {
			throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST)
					.entity(new BaseErrorEntity("SIGNATURE_REQUIRED")).build());
		}

		AuthenticationEvent authenticationInfo = null;
		try {
			authenticationInfo = userAuthenticatedService.getAuthenticationInfoOrFail(token);
		} catch (Exception e) {
			throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST)
					.entity(new BaseErrorEntity("INVALID TOKEN")).build());
		}

		return Response.status(Response.Status.OK).entity(authenticationInfo).build();
	}

	private static int expiredLoginTimer = 0;

	private void removeExpiredLogins() {
		// do every 100th validation request
		if (expiredLoginTimer++ > 0) {
			if (expiredLoginTimer > 100) expiredLoginTimer = 0;
			return;
		}
		try {
			userAuthenticatedService.removeExpiredLogins();
		} catch (Exception e) {
			try {
				errorReporterService.report(new Exception("Removing expired logins failed", e), request);
			} catch (Exception reportingException) {
				logger.error("unable to report exception", e);
				logger.error("Caused by", reportingException);
			}
		}
	}

	@DELETE
	@Path("{token}")
	@Produces(MediaType.TEXT_PLAIN)
	public Response invalidate(@PathParam("token") String token) throws WebApplicationException {
		if (notGiven(token)) {
			throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST).entity(new BaseErrorEntity("SIGNATURE_REQUIRED")).build());
		}

		try {
			userAuthenticatedService.removeLogin(token);
		} catch (Exception e) {
			throw new WebApplicationException(Response.status(Response.Status.SERVICE_UNAVAILABLE).entity(e.getMessage()).build());
		}

		return Response.status(Response.Status.OK).entity("ok").build();
	}

	private boolean notGiven(String token) {
		return token == null || token.trim().length() < 1;
	}

}
