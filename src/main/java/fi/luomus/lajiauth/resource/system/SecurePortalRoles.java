package fi.luomus.lajiauth.resource.system;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import javax.inject.Inject;
import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.joda.time.DateTime;

import com.google.common.base.Optional;
import com.google.common.collect.ImmutableSet;

import fi.luomus.lajiauth.model.AuthenticationEvent;
import fi.luomus.lajiauth.model.BaseErrorEntity;
import fi.luomus.lajiauth.service.NotificationService;
import fi.luomus.lajiauth.service.UserAuthenticatedService;
import fi.luomus.lajiauth.service.UserService;
import fi.luomus.lajiauth.service.UserService.UserAttributes;
import fi.luomus.lajiauth.util.LajiAuthPreferences;
import fi.luomus.utils.exceptions.ApiException;
import fi.luomus.utils.exceptions.NotFoundException;
import fi.luomus.utils.preferences.LuomusPreferences;

@Path("secureportal-roles")
public class SecurePortalRoles {

	private static final Set<String> PROD_PORTAL = ImmutableSet.of("KE.601");
	private static final Set<String> DEV_PORTAL = ImmutableSet.of("KE.601", "KE.963", "KE.381");

	@Inject
	private LuomusPreferences preferences;
	@Inject
	private UserAuthenticatedService userAuthenticatedService;
	@Inject
	private UserService userService;
	@Inject
	private NotificationService notificationService;

	@POST
	@Path("{targetUserId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response grantAccess(
			@PathParam("targetUserId") String targetUserId,
			@QueryParam("personToken") String personToken,
			@QueryParam("organisations") Set<String> organisations,
			@QueryParam("expireDate") String expireDate) throws WebApplicationException, ApiException, NotFoundException {

		if (notGiven(personToken) || notGiven(targetUserId) || notGiven(organisations)) {
			badRequest("Missing parameters");
		}

		organisations = parseOrganisationIds(organisations);

		validateIsAdminOfOrganisations(personToken, organisations);
		UserAttributes targetUser = validateExists(targetUserId);
		userService.addSecurePortalRoleAndOrganisations(targetUserId, organisations, parseExpireDate(expireDate));
		if (didNotHaveAccess(targetUser)) {
			notificationService.notifySecurePortalAccessGranted(targetUser);
		}
		return ok();
	}

	private boolean didNotHaveAccess(UserAttributes targetUser) {
		return !userService.hasSecurePortalRole(targetUser);
	}

	@PUT
	@Path("{targetUserIds}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response renewAccess(
			@PathParam("targetUserIds") String targetUserIds,
			@QueryParam("personToken") String personToken) throws WebApplicationException, ApiException, NotFoundException {

		if (notGiven(personToken) || notGiven(targetUserIds)) {
			badRequest("Missing parameters");
		}

		List<UserAttributes> targetUsers = getAndValidateTargetUsers(targetUserIds);
		UserAttributes adminUser = getAndValidateAdminUser(personToken);

		validateIsAdminOf(targetUsers, adminUser);

		DateTime expireDate = maxAllowedExpireDate();
		for (UserAttributes targetUser : targetUsers) {
			userService.addSecurePortalRoleAndOrganisations(targetUser.getId(), new HashSet<String>(), expireDate);
			notificationService.notifySecurePortalAccessRenewed(targetUser, expireDate);
		}

		return ok();
	}

	@DELETE
	@Path("{targetUserIds}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response revokeAccess(
			@PathParam("targetUserIds") String targetUserIds,
			@QueryParam("personToken") String personToken) throws WebApplicationException, ApiException, NotFoundException {

		if (notGiven(personToken) || notGiven(targetUserIds)) {
			badRequest("Missing parameters");
		}

		List<UserAttributes> targetUsers = getAndValidateTargetUsers(targetUserIds);
		UserAttributes adminUser = getAndValidateAdminUser(personToken);

		validateIsAdminOf(targetUsers, adminUser);

		for (UserAttributes targetUser : targetUsers) {
			userService.removeSecurePortalRoleAndOrganisations(targetUser.getId(), adminUser.getAdminOfOrganizations());
		}

		return ok();
	}

	private UserAttributes getAndValidateAdminUser(String personToken) throws ApiException {
		Optional<AuthenticationEvent> personAuthentication = userAuthenticatedService.getAuthenticationInfo(personToken);
		if (!personAuthentication.isPresent()) {
			forbidden("Invalid personToken");
		}
		AuthenticationEvent event = personAuthentication.get();
		if (!isForSecurePortal(event)) {
			forbidden("Invalid personToken target");
		}
		if (!event.getUser().getQname().isPresent()) {
			forbidden("Unknown user");
		}
		Optional<UserAttributes> userInfo = userService.getUser(event.getUser().getQname().get());
		if (!userInfo.isPresent()) {
			forbidden("Unknown user qname");
		}
		return userInfo.get();
	}

	private Response ok() {
		Map<String, String> map = new HashMap<>();
		map.put("ok", "ok");
		return Response.ok().entity(map).build();
	}

	private void forbidden(String message) {
		throw new WebApplicationException(Response.status(Response.Status.FORBIDDEN).entity(new BaseErrorEntity(message)).build());
	}

	private void badRequest(String message) {
		throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST).entity(new BaseErrorEntity(message)).build());
	}

	private UserAttributes validateExists(String userId) throws ApiException {
		Optional<UserAttributes> userInfo = userService.getUser(userId);
		if (!userInfo.isPresent()) {
			throw new WebApplicationException("Unknown target user", Response.Status.BAD_REQUEST);
		}
		return userInfo.get();
	}

	private void validateIsAdminOf(List<UserAttributes> targetUsers, UserAttributes adminUser) {
		for (UserAttributes targetUser : targetUsers) {
			if (!isAdminOf(adminUser, targetUser)) {
				forbidden("Admin user is not owner of any organisation of " + targetUser.getId());
			}
		}
	}

	private boolean isAdminOf(UserAttributes adminUser, UserAttributes targetUser) {
		for (String organisationId : targetUser.getOrganizations()) {
			if (adminUser.getAdminOfOrganizations().contains(organisationId)) {
				return true;
			}
		}
		return false;
	}

	private List<UserAttributes> getAndValidateTargetUsers(String targetUserIds) throws ApiException {
		List<UserAttributes> l = new ArrayList<>();
		for (String targetUserId : targetUserIds.split(Pattern.quote("+"))) {
			l.add(getUser(targetUserId));
		}
		return l;
	}

	private UserAttributes getUser(String userId) throws ApiException {
		Optional<UserAttributes> userInfo = userService.getUser(userId);
		if (!userInfo.isPresent()) {
			badRequest("Unknown target user");
		}
		return userInfo.get();
	}

	private DateTime parseExpireDate(String expireDate) {
		if (notGiven(expireDate)) return maxAllowedExpireDate();
		try {
			DateTime date = DateTime.parse(expireDate);
			if (date.isAfter(maxAllowedExpireDate())) return maxAllowedExpireDate();
			return date;
		} catch (Exception e) {
			return maxAllowedExpireDate();
		}
	}

	private Set<String> parseOrganisationIds(Set<String> params) {
		Set<String> organisationIds = new HashSet<>();
		for (String param : params) {
			if (param.contains("+")) {
				for (String organisationId : param.split(Pattern.quote("+"))) {
					organisationIds.add(organisationId);
				}
			}
			else if (param.contains(" ")) {
				for (String organisationId : param.split(Pattern.quote(" "))) {
					organisationIds.add(organisationId);
				}
			} else {
				organisationIds.add(param);
			}
		}
		return organisationIds;
	}

	private void validateIsAdminOfOrganisations(String personToken, Set<String> organisations) throws ApiException {
		UserAttributes adminUser = getAndValidateAdminUser(personToken);

		Set<String> adminOrganisations = adminUser.getAdminOfOrganizations();
		if (!adminOrganisations.containsAll(organisations)) {
			forbidden("Not admin of " + intersection(organisations, adminOrganisations));
		}
	}

	private Set<String> intersection(Set<String> organisations, Set<String> adminOrganisations) {
		Set<String> intersection = new HashSet<>(organisations);
		intersection.removeAll(adminOrganisations);
		return intersection;
	}

	private DateTime maxAllowedExpireDate() {
		DateTime now = DateTime.now();
		if (now.getMonthOfYear() < 7) return new DateTime(now.getYear()+1, 1, 31, 12, 0);
		return new DateTime(now.getYear()+2, 1, 31, 12, 0);
	}

	private boolean isForSecurePortal(AuthenticationEvent personAuthentication) {
		return securePortal().contains(personAuthentication.getTarget());
	}

	private boolean isProduction() {
		if (preferences.getBoolean(LajiAuthPreferences.DEV_MODE).or(false)) {
			return false;
		}
		if (preferences.getBoolean(LajiAuthPreferences.STAGING_MODE).or(false)) {
			return false;
		}
		return true;
	}

	private Set<String> securePortal() {
		return isProduction() ? PROD_PORTAL : DEV_PORTAL;
	}

	private boolean notGiven(String token) {
		return token == null || token.trim().length() < 1;
	}

	private boolean notGiven(Collection<?> c) {
		return c == null || c.isEmpty();
	}

}
