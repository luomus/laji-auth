package fi.luomus.lajiauth.resource.system;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.joda.time.DateTime;

import fi.luomus.lajiauth.service.LocalizationService;

@Path("ping")
public class Ping {
	
    @Inject
    private LocalizationService localizationService;
    private static DateTime lastThrown;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Map<String, String> ping(@QueryParam("exception") String exception) {
        Map<String, String> response = new HashMap<>();
        response.put("response", "pong");
        response.put("localized", localizationService.getMessage("hello", "hello"));
        if (exception != null && exception.length() > 0) {
            causeException(exception);
        }

        return response;
    }

    private synchronized static void causeException(String exception) {
        DateTime now = DateTime.now();
        if (lastThrown == null || lastThrown.plusSeconds(30).isBefore(now)) {
            lastThrown = now;
            throw new RuntimeException(exception);
        }
    }
    
}
