package fi.luomus.lajiauth;

import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.glassfish.jersey.logging.LoggingFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.filter.RolesAllowedDynamicFeature;
import org.glassfish.jersey.server.mvc.freemarker.FreemarkerMvcFeature;
import org.glassfish.jersey.servlet.ServletProperties;

import fi.luomus.lajiauth.resource.handlers.AuthenticationFilter;
import fi.luomus.lajiauth.resource.handlers.CommonModelInjectorFilter;
import fi.luomus.lajiauth.resource.handlers.LocalizationRequestFilter;
import fi.luomus.lajiauth.resource.handlers.RequireAuthFilter;
import fi.luomus.lajiauth.service.ModelService;
import fi.luomus.lajiauth.service.SessionService;
import fi.luomus.lajiauth.service.UserAuthenticatedService;
import fi.luomus.lajiauth.service.additional.UserIdResolverService;

public class Application extends ResourceConfig {

	public Application() {
		packages("fi.luomus.lajiauth.resource");

		registerClasses(
				FreemarkerMvcFeature.class, LoggingFeature.class, LocalizationRequestFilter.class,
				CommonModelInjectorFilter.class, AuthenticationFilter.class, RequireAuthFilter.class,
				RolesAllowedDynamicFeature.class);

		register(new AbstractBinder() {
			@Override
			protected void configure() {
				bind(ModelService.class).to(ModelService.class);
				bind(UserAuthenticatedService.class).to(UserAuthenticatedService.class);
				bind(UserIdResolverService.class).to(UserIdResolverService.class);
				bind(SessionService.class).to(SessionService.class);
			}
		});

		property(FreemarkerMvcFeature.TEMPLATE_BASE_PATH, "/WEB-INF/template");
		property(FreemarkerMvcFeature.ENCODING, "UTF-8");
		property(ServletProperties.FILTER_STATIC_CONTENT_REGEX, "/(static|webjars)/.*|/csrfguard");
	}

}
