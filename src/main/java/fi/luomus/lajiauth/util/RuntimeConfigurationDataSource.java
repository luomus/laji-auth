package fi.luomus.lajiauth.util;

import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.sql.DataSource;

import org.springframework.jdbc.datasource.LazyConnectionDataSourceProxy;

import com.zaxxer.hikari.HikariDataSource;

import fi.luomus.utils.preferences.LuomusPreferences;

/**
 * Used to delay datasource initialization in case of no configured data source
 * parameters
 */
public class RuntimeConfigurationDataSource implements DataSource {

	private final LuomusPreferences preferences;
	private HikariDataSource hikariDataSource;
	private final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(RuntimeConfigurationDataSource.class);

	@Inject
	public RuntimeConfigurationDataSource(LuomusPreferences luomusPreferences) {
		this.preferences = luomusPreferences;
	}

	@Override
	public Connection getConnection() throws SQLException {
		return actual().getConnection();
	}

	@Override
	public Connection getConnection(String username, String password) throws SQLException {
		return actual().getConnection(username, password);
	}

	@Override
	public PrintWriter getLogWriter() throws SQLException {
		return actual().getLogWriter();
	}

	@Override
	public void setLogWriter(PrintWriter out) throws SQLException {
		actual().setLogWriter(out);
	}

	@Override
	public void setLoginTimeout(int seconds) throws SQLException {
		actual().setLoginTimeout(seconds);
	}

	@Override
	public int getLoginTimeout() throws SQLException {
		return actual().getLoginTimeout();
	}

	@Override
	public Logger getParentLogger() throws SQLFeatureNotSupportedException {
		return actual().getParentLogger();
	}

	@Override
	public <T> T unwrap(Class<T> iface) throws SQLException {
		return actual().unwrap(iface);
	}

	@Override
	public boolean isWrapperFor(Class<?> iface) throws SQLException {
		return iface == com.zaxxer.hikari.HikariDataSource.class || hikariDataSource.isWrapperFor(iface);
	}

	private synchronized DataSource actual() {
		if (this.hikariDataSource == null && preferences.get(LajiAuthPreferences.DB_URL).isPresent()) {
			this.hikariDataSource = DataSourceDefinition.initDataSource(preferences);
			logger.info("datasource configured");
		}
		if (this.hikariDataSource == null) {
			logger.warn("Datasource configuration (for example " + LajiAuthPreferences.DB_URL.key + " ) not found in LajiAuth Preferences, db operations will most certainly fail!");
		}
		return (this.hikariDataSource == null) ? new LazyConnectionDataSourceProxy() : this.hikariDataSource;
	}

}
