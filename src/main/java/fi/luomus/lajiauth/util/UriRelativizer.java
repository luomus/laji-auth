package fi.luomus.lajiauth.util;

import java.net.URI;
import java.util.Arrays;

import javax.ws.rs.core.UriBuilder;

public final class UriRelativizer {

	private UriRelativizer() {}

	/**
	 * Turns an absolute URI into a relative one.
	 *
	 * @param current    The absolute uri that has a common part with the relative one
	 * @param relativeTo The uri the result should be relative to
	 * @return A relative uri
	 */
	public static URI relativePath(URI current, URI relativeTo) {
		SplitPath currentSplit = SplitPath.create(current.getPath());
		SplitPath relativeSplit = SplitPath.create(relativeTo.getPath());

		int commonCount = 0;
		for (int i = 0; i < Math.min(currentSplit.pathSegments.length, relativeSplit.pathSegments.length); i++) {
			if (currentSplit.pathSegments[i].equals(relativeSplit.pathSegments[i])) {
				commonCount++;
			} else {
				break;
			}
		}
		UriBuilder builder = null;
		if (commonCount == currentSplit.pathSegments.length && commonCount == relativeSplit.pathSegments.length) {
			builder = UriBuilder.fromPath("./");
		} else {
			for (int i = 0; i < currentSplit.pathSegments.length - commonCount; i++) {
				builder = builder == null ? UriBuilder.fromPath("../") : builder.path("../");
			}
			for (int i = commonCount; i < relativeSplit.pathSegments.length; i++) {
				String pathSegment = relativeSplit.pathSegments[i];
				builder = builder == null ? UriBuilder.fromPath(pathSegment) : builder.path(pathSegment);
			}
		}
		if (builder == null) throw new IllegalStateException();
		builder = builder.path(relativeSplit.file);
		if (relativeTo.getQuery() != null) {
			builder.replaceQuery(relativeTo.getRawQuery());
		}
		return builder.build();
	}

	private static class SplitPath {
		public final String[] pathSegments;
		public final String file;

		private SplitPath(String[] pathSegments, String file) {
			this.pathSegments = pathSegments;
			this.file = file;
		}

		public static SplitPath create(String uriPath) {
			String[] split = uriPath.split("/");
			if (uriPath.charAt(uriPath.length() - 1) != '/') {
				String filename = split[split.length - 1];
				split = Arrays.copyOfRange(split, 0, split.length - 1);
				return new SplitPath(split, filename);
			}
			return new SplitPath(split, "");
		}
	}

}
