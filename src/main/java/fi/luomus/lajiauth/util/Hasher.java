package fi.luomus.lajiauth.util;

import java.nio.charset.Charset;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;

import com.google.common.base.Charsets;

public class Hasher {

	private final String key;
	private final Charset CHAR_SET = Charsets.UTF_8;
	private static final String ALGORITHM_NAME = "HmacSHA512";

	public Hasher(String key) {
		this.key = key;
	}

	public String hash(String seed) {
		return Base64.encodeBase64URLSafeString(getBytes(seed));
	}

	public boolean check(String expected, String hash) {
		byte[] expectedHash = getBytes(expected);
		String s = Base64.encodeBase64URLSafeString(expectedHash);
		return hash.equals(s);
	}

	private byte[] getBytes(String seed) {
		Mac sha512_HMAC;
		try {
			sha512_HMAC = Mac.getInstance(ALGORITHM_NAME);
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException(e);
		}

		SecretKeySpec secretkey = new SecretKeySpec(key.getBytes(CHAR_SET), ALGORITHM_NAME);
		try {
			sha512_HMAC.init(secretkey);
		} catch (InvalidKeyException e) {
			throw new RuntimeException(e);
		}

		return sha512_HMAC.doFinal(seed.getBytes(CHAR_SET));
	}

}
