package fi.luomus.lajiauth.util;

import java.util.concurrent.TimeUnit;

import javax.servlet.http.HttpServletRequest;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;

public class IPUtil {

	private static Cache<String, Integer> requestCounts = CacheBuilder.newBuilder()
			.expireAfterWrite(10, TimeUnit.MINUTES)
			.build();

	public static boolean shouldBlock(HttpServletRequest request, String username) {
		String key = getClientIpAddr(request)+username;
		Integer requestCount = requestCounts.getIfPresent(key);
		if (requestCount == null) {
			requestCounts.put(key, 1);
			return false;
		}
		requestCounts.put(key, requestCount + 1);
		return requestCount > 15;
	}

	public static void reportOk(HttpServletRequest request, String username) {
		String key = getClientIpAddr(request)+username;
		requestCounts.invalidate(key);
	}
	
	private static String getClientIpAddr(HttpServletRequest request) {  
		String ip = request.getHeader("X-Forwarded-For");  
		if (given(ip)) return ip;  

		ip = request.getHeader("Proxy-Client-IP");  
		if (given(ip)) return ip;

		ip = request.getHeader("WL-Proxy-Client-IP");  
		if (given(ip)) return ip;

		ip = request.getHeader("HTTP_CLIENT_IP");  
		if (given(ip)) return ip;

		ip = request.getHeader("HTTP_X_FORWARDED_FOR");  
		if (given(ip)) return ip;

		return request.getRemoteAddr();  
	}

	private static boolean given(String ip) {
		return ip != null && !ip.isEmpty() && !"unknown".equalsIgnoreCase(ip);
	} 

}
