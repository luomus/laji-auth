package fi.luomus.lajiauth.util;

import java.net.URI;
import java.net.URISyntaxException;

import javax.inject.Inject;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

import com.fasterxml.jackson.databind.ObjectMapper;

import fi.luomus.lajiauth.service.LajiAuthClient;
import fi.luomus.lajiauth.service.SystemService;
import fi.luomus.triplestore.client.HTTPTriplestoreClientImpl;
import fi.luomus.triplestore.client.TriplestoreClient;
import fi.luomus.triplestore.client.TriplestorePreferences;
import fi.luomus.utils.preferences.LuomusPreferences;
import fi.luomus.utils.service.ErrorReporterService;
import fi.luomus.utils.service.ErrorReporterServiceFactory;
import fi.luomus.utils.service.JavaMailErrorReportMailer;

@Configuration
public class BeanConfiguration {

	@Inject
	private LuomusPreferences preferences;
	@Bean
	public ObjectMapper objectMapper() {
		return new ObjectMapper();
	}

	@Bean
	public TriplestoreClient triplestoreClient() {
		return new HTTPTriplestoreClientImpl(new TriplestorePreferences() {
			@Override
			public URI getBaseUri() {
				try {
					return new URI(preferences.get(LajiAuthPreferences.TRIPLESTORE_URL).get());
				} catch (URISyntaxException e) {
					throw new RuntimeException("unable to parse triplestore address", e);
				}
			}

			@Override
			public String getUsername() {
				return preferences.get(LajiAuthPreferences.TRIPLESTORE_USERNAME).get();
			}

			@Override
			public String getPassword() {
				return preferences.get(LajiAuthPreferences.TRIPLESTORE_PASSWORD).get();
			}
		});
	}

	@Bean
	public ErrorReporterService errorReporterService() {
		return ErrorReporterServiceFactory
				.reporter(new JavaMailErrorReportMailer.JavaMailErrorReportMailerConfiguration() {
					@Override
					public String getHost() {
						return preferences.get(LajiAuthPreferences.EMAIL_HOST).get();
					}

					@Override
					public String getSubject() {
						return "!!! Laji-auth exception !!! mode: " + mode();
					}

					private String mode() {
						return preferences.get(LajiAuthPreferences.DB_USERNAME).or(" UNKNOWN ");
					}

					@Override
					public String[] getRecipients() {
						return preferences.getMulti(LajiAuthPreferences.ADMIN_EMAIL).toArray(new String[]{});
					}
				});
	}

	@Bean
	@Lazy
	public LajiAuthClient lajiAuthClient(SystemService systemService) {
		String systemId = preferences.get(LajiAuthPreferences.LAJI_AUTH_SYSTEM_ID).get();
		return new LajiAuthClient(systemId, systemService.getLajiAuthBaseURI());
	}

}
