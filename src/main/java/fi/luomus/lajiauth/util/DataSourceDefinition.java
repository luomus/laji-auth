package fi.luomus.lajiauth.util;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import fi.luomus.utils.preferences.LuomusPreferences;

public class DataSourceDefinition {

	public static HikariDataSource initDataSource(LuomusPreferences preferences) {
		HikariConfig config = new HikariConfig();
		config.setJdbcUrl(preferences.get(LajiAuthPreferences.DB_URL).get());
		config.setUsername(preferences.get(LajiAuthPreferences.DB_USERNAME).get());
		config.setPassword(preferences.get(LajiAuthPreferences.DB_PASSWORD).get());
		config.setDriverClassName("oracle.jdbc.OracleDriver");

		config.setAutoCommit(true); // non-transaction mode: connection will be set to transaction mode if updates are done

		config.setConnectionTimeout(15000); // 15 seconds
		config.setMaximumPoolSize(80);
		config.setIdleTimeout(60000); // 1 minute
		config.setMaxLifetime(60000); // 1 minute -- the longest allowed query

		return new HikariDataSource(config);
	}

}

