package fi.luomus.lajiauth.util;

import java.util.regex.Pattern;

public class EmailValidator {
	
	public static boolean validEmail(String email) {
		if (!given(email)) return false;
		if (!email.contains(".") || !email.contains("@") || email.contains(" ")) {
			return false;
		}
		String[] parts = email.split(Pattern.quote("@"));
		if (parts.length != 2) return false;
		String localPart = parts[0];
		String domain = parts[1];
		if (localPart.length() < 1 || domain.length() < 4) {
			return false;
		}
		if (localPart.startsWith(".") || localPart.endsWith(".") || domain.startsWith(".") || domain.endsWith(".")) {
			return false;
		}
		if (!domain.contains(".")) {
			return false;
		}
		return true;
	}

	private static boolean given(String email) {
		return email != null && email.length() > 0;
	}
	
}
