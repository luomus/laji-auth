package fi.luomus.lajiauth.util;

import javax.ws.rs.WebApplicationException;

public class ErrorPageWebApplicationException extends Exception {

	private static final long serialVersionUID = 8774166687232506554L;
	public final WebApplicationException cause;

	private ErrorPageWebApplicationException(WebApplicationException exception) {
		super(exception.getMessage());
		this.cause = exception;
	}

	public static ErrorPageWebApplicationException wrap(WebApplicationException exception) {
		return new ErrorPageWebApplicationException(exception);
	}

}
