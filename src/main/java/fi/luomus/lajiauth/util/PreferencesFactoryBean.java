package fi.luomus.lajiauth.util;

import java.util.prefs.Preferences;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.FactoryBean;

import fi.luomus.lajiauth.Application;

/**
 * Used to support multiple preference profiles
 */
public class PreferencesFactoryBean implements FactoryBean<Preferences> {

	@Override
	public Preferences getObject() throws Exception {
		Preferences basePreferences = Preferences.userNodeForPackage(Application.class);
		String profile = System.getProperty("preferencesProfile");
		Logger logger = LoggerFactory.getLogger(this.getClass());
		if (profile != null) {
			logger.info("Using preferences profile " + profile);
			return basePreferences.node(profile);
		}
		logger.info("Using base preferences");
		return basePreferences;
	}

	@Override
	public Class<?> getObjectType() {
		return Preferences.class;
	}

	@Override
	public boolean isSingleton() {
		return true;
	}

}
