package fi.luomus.lajiauth.util;

import fi.luomus.utils.preferences.Preference;

public class LajiAuthPreferences {

	private LajiAuthPreferences() {}

	/**
	 * When in dev mode for example emails are not send etc
	 */
	public final static Preference DEV_MODE = new Preference("DEV_MODE");

	/**
	 * Only effects header logo (at the moment)
	 */
	public final static Preference STAGING_MODE = new Preference("STAGING_MODE");

	/**
	 * Secret for hasher used to generate LOCAL user passwords
	 */
	public final static Preference SECRET_KEY = new Preference("SECRET_KEY");

	/**
	 * Qname of Laji-auth (different for staging and production)
	 */
	public final static Preference LAJI_AUTH_SYSTEM_ID = new Preference("LAJI_AUTH_SYSTEM_ID");

	public final static Preference TRIPLESTORE_URL = new Preference("TRIPLESTORE_URL");
	public final static Preference TRIPLESTORE_USERNAME = new Preference("TRIPLESTORE_USERNAME");
	public final static Preference TRIPLESTORE_PASSWORD = new Preference("TRIPLESTORE_PASSWORD");

	public final static Preference EMAIL_HOST = new Preference("EMAIL_HOST");

	/**
	 * Where to send error reports
	 */
	public final static Preference ADMIN_EMAIL = new Preference("ADMIN_EMAIL");

	public final static Preference DISCOURSE_URL = new Preference("DISCOURSE_URL");
	public final static Preference DISCOURSE_SSO_TOKEN = new Preference("DISCOURSE_SSO_TOKEN");
	public final static Preference DISCOURSE_SYSTEM_ID = new Preference("DISCOURSE_SYSTEM_ID");

	public final static Preference INATURALIST_APP_ID = new Preference("INATURALIST_APP_ID");
	public final static Preference INATURALIST_SECRET = new Preference("INATURALIST_SECRET");

	public final static Preference OMARIISTA_APP_ID = new Preference("OMARIISTA_APP_ID");
	public final static Preference OMARIISTA_SECRET = new Preference("OMARIISTA_SECRET");

	public final static Preference FACEBOOK_APP_ID = new Preference("FACEBOOK_APP_ID");
	public final static Preference FACEBOOK_SECRET = new Preference("FACEBOOK_SECRET");

	public final static Preference GOOGLE_CLIENT_ID = new Preference("GOOGLE_CLIENT_ID");
	public final static Preference GOOGLE_SECRET = new Preference("GOOGLE_SECRET");

	public final static Preference APPLE_TEAM_ID = new Preference("APPLE_TEAM_ID");
	public final static Preference APPLE_KEY_ID = new Preference("APPLE_KEY_ID");
	public final static Preference APPLE_PRIVATE_KEY = new Preference("APPLE_PRIVATE_KEY");

	public final static Preference DB_URL = new Preference("DB_URL");
	public final static Preference DB_USERNAME = new Preference("DB_USERNAME");
	public final static Preference DB_PASSWORD = new Preference("DB_PASSWORD");

	public final static Preference OLD_HATIKKA_SALT = new Preference("OLD_HATIKKA_SALT");

	public static final Preference LAJI_API_ACCESS_TOKEN = new Preference("LAJI_API_ACCESS_TOKEN");
	public static final Preference LAJI_API_URL = new Preference("LAJI_API_URL");

}