package fi.luomus.lajiauth.util;

import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;

import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class JWT {

	public static String generateJWT(String keyId, String issuer, String audience, String subject, String privateKey)  {
		Map<String,Object> header = new HashMap<>();
		header.put("alg", "ES256");
		header.put("kid", keyId);

		int initialTime = ((int) (System.currentTimeMillis() / 1000)) - 36000;
		int expireTime = initialTime + 72000;

		Map<String,Object> payload = new HashMap<>();
		payload.put("iss", issuer);
		payload.put("sub", subject);
		payload.put("aud", audience);
		payload.put("exp", expireTime);
		payload.put("nbf", initialTime);
		payload.put("iat", initialTime);

		JwtBuilder builder = Jwts.builder()
				.setHeaderParams(header)
				.setClaims(payload)
				.signWith(SignatureAlgorithm.ES256, getPrivateKey(privateKey));

		return builder.compact();
	}

	private static PrivateKey getPrivateKey(String key) {
		try {
			return KeyFactory.getInstance("EC").generatePrivate(new PKCS8EncodedKeySpec(Base64.decodeBase64(key)));
		} catch (Exception e) {
			throw new IllegalStateException("Invalid private key");
		}
	}

}
