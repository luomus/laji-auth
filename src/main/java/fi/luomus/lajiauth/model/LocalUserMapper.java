package fi.luomus.lajiauth.model;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

public class LocalUserMapper implements ResultSetMapper<LocalUser> {

	@Override
	public LocalUser map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
		LocalUser localUser = new LocalUser();
		localUser.setEmail(resultSet.getString("email").toLowerCase());
		localUser.setFullName(resultSet.getString("name"));
		localUser.setPassword(resultSet.getString("password"));
		localUser.setNonce(resultSet.getString("nonce"));
		localUser.setConfirmed(resultSet.getBoolean("confirmed"));
		return localUser;
	}

}
