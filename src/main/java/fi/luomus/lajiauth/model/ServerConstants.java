package fi.luomus.lajiauth.model;

import java.util.Map;
import java.util.Set;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;

public final class ServerConstants {

	private ServerConstants() {}

	public final static String PERMANENT = "permanent";

	public final static Set<String> ALLOWED_LANGUAGES = ImmutableSet.<String>builder()
			.add("fi")
			.add("sv")
			.add("en")
			.build();

	public static final String ADDITIONAL_SYSTEM_LINTUVAARA = "LINTUVAARA";
	public static final String ADDITIONAL_SYSTEM_HYONTEISTIETOKANTA = "HYONTEISTIETOKANTA";
	public static final String ADDITIONAL_SYSTEM_HATIKKA = "HATIKKA";
	public static final String ADDITIONAL_SYSTEM_OLD_HATIKKA = "VANHA_HATIKKA";

	public final static Map<String, String> ADDITIONAL_SYSTEMS_MAP = ImmutableMap.<String, String>builder()
			.put("MA.fieldjournalLoginName", ADDITIONAL_SYSTEM_HATIKKA)
			.put("MA.hatikkaLoginName", ADDITIONAL_SYSTEM_OLD_HATIKKA)
			.put("MA.insectDatabaseLoginName", ADDITIONAL_SYSTEM_HYONTEISTIETOKANTA)
			.put("MA.lintuvaaraLoginName", ADDITIONAL_SYSTEM_LINTUVAARA)
			.build();

	public final static Set<String> ADDITIONAL_SYSTEMS;
	static {
		ImmutableSet.Builder<String> b = ImmutableSet.builder();
		for (String system : ADDITIONAL_SYSTEMS_MAP.values()) {
			b = b.add(system);
		}
		ADDITIONAL_SYSTEMS = b.build();
	}

}
