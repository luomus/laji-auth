package fi.luomus.lajiauth.model;

public class OAuthState {

	private final String target;
	private final String next;
	private final String redirectMethod;
	private final boolean permanent;

	public OAuthState(String target, String next, String redirectMethod, boolean permanent) {
		super();
		this.target = target;
		this.next = next;
		this.redirectMethod = redirectMethod;
		this.permanent = permanent;
	}

	public String getTarget() {
		return target;
	}

	public String getNext() {
		return next;
	}

	public String getRedirectMethod() {
		return redirectMethod;
	}

	public boolean isPermanent() {
		return permanent;
	}


}
