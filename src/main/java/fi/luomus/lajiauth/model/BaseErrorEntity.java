package fi.luomus.lajiauth.model;

/**
 * The base error entity that should be returned to the user
 * in case of an error. Should be subclassed if any additional
 * fields are required
 */
public class BaseErrorEntity {

	public final String code;

	public BaseErrorEntity(String code) {
		this.code = code;
	}

}
