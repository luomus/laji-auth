package fi.luomus.lajiauth.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

/**
 * Used to store sent emails in-memory for debugging purposes
 */
@Service
@Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
public class EmailStore {
	
    private final List<Email> emails = new ArrayList<>();

    public List<Email> getEmails() {
        return emails;
    }

    public static class Email {
        private String from;
        private List<String> to;
        private String subject;
        private String body;

        public String getFrom() {
            return from;
        }

        public void setFrom(String from) {
            this.from = from;
        }

        public List<String> getTo() {
            return to;
        }

        public void setTo(List<String> to) {
            this.to = to;
        }

        public String getSubject() {
            return subject;
        }

        public void setSubject(String subject) {
            this.subject = subject;
        }

        public String getBody() {
            return body;
        }

        public void setBody(String body) {
            this.body = body;
        }
    }
    
}
