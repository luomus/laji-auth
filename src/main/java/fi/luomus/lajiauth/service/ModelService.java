package fi.luomus.lajiauth.service;

import java.net.URI;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.MissingResourceException;

import javax.inject.Inject;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import org.springframework.stereotype.Service;

import fi.luomus.lajiauth.model.Constants;
import fi.luomus.lajiauth.model.ServerConstants;
import fi.luomus.lajiauth.util.LajiAuthPreferences;
import fi.luomus.lajiauth.util.UriRelativizer;
import fi.luomus.utils.preferences.LuomusPreferences;

/**
 * Used to inject global attributes into models
 */
@Service
public class ModelService {

	@Context
	private UriInfo uriInfo;
	@Context
	private LocalizationService localizationService;
	@Context
	private LanguageService languageService;
	@Context
	private LuomusPreferences luomusPreferences;
	@Inject
	private SessionService sessionService;

	/**
	 * Get a model with global attributes and localized strings for the bundle name
	 *
	 * @param bundleName Name of the bundle that should be used for the localized strings
	 * @return The model
	 */
	public Map<String, Object> getModel(String bundleName) {
		Map<String, Object> model = baseModel(uriInfo);
		model.put("messages", new BundleWrapper(localizationService, bundleName));
		model.put("parameters", uriInfo.getQueryParameters());
		model.put("devMode", luomusPreferences.getBoolean(LajiAuthPreferences.DEV_MODE).or(Boolean.TRUE));
		model.put("stagingMode", luomusPreferences.getBoolean(LajiAuthPreferences.STAGING_MODE).or(Boolean.FALSE));
		model.put("loggedIn", isLoggedIn());
		return model;
	}

	private boolean isLoggedIn() {
		return sessionService.isAthenticatedForSelf();
	}

	private static final String deployTimestamp = Long.toString(new Date().getTime()/1000);

	private Map<String, Object> baseModel(UriInfo uriInfo) {
		Map<String, Object> model = new HashMap<>();
		model.put("timestamp", deployTimestamp);
		model.put("baseUri", UriRelativizer.relativePath(uriInfo.getRequestUri(), uriInfo.getBaseUri()));
		model.put("staticDir", UriRelativizer.relativePath(uriInfo.getRequestUri(), uriInfo.getBaseUriBuilder().path("static").build()));
		model.put("language", languageService.getLanguage());
		Map<String, String> languageLinks = new HashMap<>();
		for (String language : ServerConstants.ALLOWED_LANGUAGES) {
			URI link = UriBuilder.fromUri(uriInfo.getRequestUri()).replaceQueryParam(Constants.LANGUAGE_PARAMETER,language).build();
			languageLinks.put(language, UriRelativizer.relativePath(uriInfo.getRequestUri(), link).toString());
		}
		model.put("languages", languageLinks);

		return model;
	}

	public static class BundleWrapper {
		private final LocalizationService localizationService;
		private final String bundleName;

		private BundleWrapper(LocalizationService localizationService, String bundleName) {
			this.localizationService = localizationService;
			this.bundleName = bundleName;
		}

		public String get(String key, String... args) {
			try {
				return localizationService.getMessage(bundleName, key, args);
			} catch (MissingResourceException e) {
				try {
					return localizationService.getMessage("common", key, args);
				} catch (MissingResourceException e1) {
					return "???" + key + "???";
				}
			}
		}
	}

}
