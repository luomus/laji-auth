package fi.luomus.lajiauth.service;


import java.io.IOException;
import java.net.URI;
import java.util.Arrays;
import java.util.regex.Pattern;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.plus.Plus;
import com.google.api.services.plus.PlusScopes;
import com.google.api.services.plus.model.Person;

import fi.luomus.lajiauth.model.AuthenticationSourceUser;
import fi.luomus.lajiauth.util.LajiAuthPreferences;
import fi.luomus.utils.preferences.LuomusPreferences;

@Service
public class GoogleService {

	private final LuomusPreferences luomusPreferences;

	// these should be thread safe..
	private static final JacksonFactory jacksonFactory = JacksonFactory.getDefaultInstance();
	private static final NetHttpTransport transport = new NetHttpTransport();

	@Inject
	public GoogleService(LuomusPreferences luomusPreferences) {
		this.luomusPreferences = luomusPreferences;
	}

	/**
	 * Create a google redirect uri that the user should redirected for authentication
	 *
	 * @param redirectUri The uri the user should be redirected to after successfully authenticating with
	 *                    google
	 * @param state       A state parameter that will be passed on to the controller responding at redirect uri
	 * @return An uri pointing to google's authentication service
	 */
	public URI getRedirectUri(URI redirectUri, String state) {
		if (redirectUri.getQuery() != null && !redirectUri.getQuery().equals("")) {
			throw new IllegalStateException("redirect uri may not contain a query");
		}

		return URI.create(flow().newAuthorizationUrl()
				.setRedirectUri(redirectUri.toString())
				.setState(state)
				.build());
	}


	/**
	 * Use the received access code to retrieve an access token and in turn use that to return user's relevant
	 * details
	 *
	 * @param code             The access code received from the authenticated user
	 * @param originalRedirect The redirect uri that was originally used (for verification)
	 * @return The user's details
	 */
	public AuthenticationSourceUser getUserDetails(String code, URI originalRedirect) {
		GoogleAuthorizationCodeFlow flow = flow();
		try {
			Credential andStoreCredential = flow.createAndStoreCredential(flow.newTokenRequest(code)
					.setRedirectUri(originalRedirect.toString()).execute(), null);
			Person me = new Plus(new NetHttpTransport(), JacksonFactory.getDefaultInstance(),
					andStoreCredential).people().get("me").execute();

			AuthenticationSourceUser result = new AuthenticationSourceUser();
			result.setId(me.getId());
			result.setFullName(getFullName(me));
			result.setPreferredName(getPreferredName(me));
			result.setInheritedName(getInheritedName(me));
			result.setEmail(getEmail(me));
			return result;
		} catch (IOException e) {
			// should not happen since were not storing credentials..
			throw new RuntimeException(e);
		}
	}

	private String getEmail(Person me) {
		if (me.getEmails().size() > 0) {
			return me.getEmails().get(0).getValue();
		}
		return null;
	}

	private String getInheritedName(Person me) {
		if (me.getName() == null) return "";
		String familyName = me.getName().getFamilyName();
		if (familyName == null) return "";
		return familyName;
	}

	private String getPreferredName(Person me) {
		if (me.getName() == null) return getNickName(me);
		String givenName = me.getName().getGivenName();
		if (!given(givenName)) return getNickName(me);
		return givenName;
	}

	private String getNickName(Person me) {
		String nickname = me.getNickname();
		if (nickname == null) return "";
		return nickname;
	}

	private String getFullName(Person me) {
		if (given(me.getDisplayName())) return me.getDisplayName();

		String fullName = parseFullNameFromGivenAndFamilyName(me);
		if (given(fullName)) return fullName;

		if (given(me.getNickname())) return me.getNickname();

		String email = getEmail(me);
		if (given(email)) {
			return email.split(Pattern.quote("@"))[0];
		}

		return me.getId();
	}

	private String parseFullNameFromGivenAndFamilyName(Person me) {
		if (me.getName() == null) return "";
		StringBuilder b = new StringBuilder();
		if (given(me.getName().getGivenName())) b.append(me.getName().getGivenName());
		if (given(b.toString())) b.append(" ");
		if (given(me.getName().getFamilyName())) b.append(me.getName().getFamilyName());
		return b.toString();
	}

	private boolean given(String s) {
		return s != null && s.trim().length() > 0;
	}

	private GoogleAuthorizationCodeFlow flow() {
		String clientId = luomusPreferences.get(LajiAuthPreferences.GOOGLE_CLIENT_ID).get();
		String secret = luomusPreferences.get(LajiAuthPreferences.GOOGLE_SECRET).get();
		return new GoogleAuthorizationCodeFlow.Builder(transport, jacksonFactory, clientId, secret,
				Arrays.asList(PlusScopes.PLUS_ME, PlusScopes.USERINFO_EMAIL)).build();
	}

}
