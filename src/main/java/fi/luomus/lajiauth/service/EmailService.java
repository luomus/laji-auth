package fi.luomus.lajiauth.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import javax.inject.Inject;
import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.stereotype.Service;

import fi.luomus.lajiauth.util.LajiAuthPreferences;
import fi.luomus.utils.preferences.LuomusPreferences;

@Service
public class EmailService {
	
    private final LuomusPreferences luomusPreferences;
    private final EmailStore emailStore;

    @Inject
    public EmailService(LuomusPreferences luomusPreferences, EmailStore emailStore) {
        this.luomusPreferences = luomusPreferences;
        this.emailStore = emailStore;
    }

    public EmailBuilder email() {
        return new EmailBuilder() {
            private Address from;
            private final Set<Address> to = new HashSet<>();
            private String subject;
            private String body;

            @Override
            public EmailBuilder from(String from) {
                try {
                    this.from = new InternetAddress(from);
                } catch (AddressException e) {
                    throw new RuntimeException("unable to parse sender address: " + from, e);
                }
                return this;
            }

            @Override
            public EmailBuilder to(String to) {
                try {
                    this.to.add(new InternetAddress(to));
                } catch (AddressException e) {
                    throw new RuntimeException("unable to parse recipient address " + to, e);
                }

                return this;
            }

            @Override
            public EmailBuilder to(Collection<String> to) {
                for (String address : to) {
                    try {
                        this.to.add(new InternetAddress(address));
                    } catch (AddressException e) {
                        throw new RuntimeException("unable to parse recipient address " + address, e);
                    }
                }

                return this;
            }

            @Override
            public EmailBuilder subject(String subject) {
                this.subject = subject;

                return this;
            }

            @Override
            public EmailBuilder body(String body) {
                this.body = body;

                return this;
            }

            @Override
            public void send() throws MessagingException {
                if (devMode()) {
                    storeEmailInMemory();
                } else {
                    sendEmail();
                }
            }

			private void sendEmail() throws MessagingException {
				Properties props = new Properties();
				props.put("mail.smtp.host", luomusPreferences.get(LajiAuthPreferences.EMAIL_HOST).get());
				Session session = Session.getInstance(props);
				Message message = new MimeMessage(session);
				message.setFrom(from);
				message.setRecipients(Message.RecipientType.TO, to.toArray(new Address[]{}));
				message.setSubject(subject);
				message.setSentDate(new Date());
				message.setText(body);
				Transport.send(message);
			}

			private void storeEmailInMemory() {
				EmailStore.Email email = new EmailStore.Email();
				email.setFrom(from.toString());
				List<String> toStr = new ArrayList<>();
				for (Address recipient : to) {
				    toStr.add(recipient.toString());
				}
				email.setTo(toStr);
				email.setSubject(subject);
				email.setBody(body);
				emailStore.getEmails().add(email);
			}

			private Boolean devMode() {
				return luomusPreferences.getBoolean(LajiAuthPreferences.DEV_MODE).get();
			}
        };
    }

    public interface EmailBuilder {
        EmailBuilder from(String from);
        EmailBuilder to(String to);
        EmailBuilder to(Collection<String> to);
        EmailBuilder subject(String subject);
        EmailBuilder body(String body);
        void send() throws MessagingException;
    }
    
}
