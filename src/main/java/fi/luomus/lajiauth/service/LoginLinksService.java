package fi.luomus.lajiauth.service;

import static fi.luomus.lajiauth.model.AuthenticationSource.HAKA;
import static fi.luomus.lajiauth.model.AuthenticationSource.VIRTU;
import static fi.luomus.lajiauth.util.ErrorPageWebApplicationException.wrap;
import static fi.luomus.lajiauth.util.UriRelativizer.relativePath;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import org.springframework.stereotype.Service;

import com.google.common.base.Optional;

import fi.luomus.lajiauth.Utils;
import fi.luomus.lajiauth.model.AuthenticationSource;
import fi.luomus.lajiauth.model.Constants;
import fi.luomus.lajiauth.util.ErrorPageWebApplicationException;
import fi.luomus.utils.exceptions.ApiException;

@Service
public class LoginLinksService {

	@Inject
	LanguageService languageService;
	@Inject
	SystemService systemService;

	public Map<String, Object> initCommonLoginModel(String targetSystemId) throws ApiException, ErrorPageWebApplicationException {
		String language = languageService.getLanguage();
		Optional<Optional<String>> systemName = systemService.getSystemName(targetSystemId, language);
		if (!systemName.isPresent()) {
			throw wrap(new WebApplicationException("SYSTEM_NOT_FOUND", Response.Status.NOT_FOUND));
		}
		Map<String, Object> model = new HashMap<>();
		model.put("systemName", systemName.get().isPresent() ? systemName.get().get() : targetSystemId);
		model.put("privacyPolicyUrl", Utils.getPrivacyPolicyUrl(languageService.getLanguage()));
		model.put("helpUrl", Utils.getHelpUrl(languageService.getLanguage()));
		return model;
	}

	public Map<String, URI> sourceURIs(UriInfo uriInfo, String targetSystemId, String next, String redirectMethod, Collection<AuthenticationSource> sources) {
		Map<String, URI> sourceUrls = new HashMap<>();
		for (AuthenticationSource source : sources) {
			URI uri = authSourcePath(uriInfo, source, targetSystemId, next, redirectMethod);
			sourceUrls.put(source.name().toLowerCase() + "Url", uri);
		}
		return sourceUrls;
	}

	public Map<String, URI> allSourceURIs(UriInfo uriInfo, String targetSystemId, String next, String redirectMethod) {
		return sourceURIs(uriInfo, targetSystemId, next, redirectMethod, allSources());
	}

	private List<AuthenticationSource> allSources() {
		List<AuthenticationSource> sources = new ArrayList<>();
		for (AuthenticationSource source : AuthenticationSource.values()) {
			sources.add(source);
		}
		return sources;
	}

	private URI authSourcePath(UriInfo uriInfo, AuthenticationSource type, String targetSystem, String next, String redirectMethod) {
		UriBuilder builder = uriInfo.getBaseUriBuilder().path(Constants.AUTH_SOURCES);
		if (type == HAKA || type == VIRTU) {
			builder = builder.path("shibboleth");
		}
		URI destination = builder.path(type.toString()).queryParam(Constants.TARGET_SYSTEM_PARAMETER, targetSystem)
				.queryParam(Constants.NEXT_PARAMETER, next)
				.queryParam(Constants.REDIRECT_METHOD, redirectMethod)
				.build();

		return relativePath(uriInfo.getRequestUri(), destination);
	}

}
