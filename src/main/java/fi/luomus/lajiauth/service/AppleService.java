package fi.luomus.lajiauth.service;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLEncoder;
import java.util.Map;
import java.util.regex.Pattern;

import javax.inject.Inject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import org.apache.commons.codec.binary.Base64;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import fi.luomus.lajiauth.model.AuthenticationSource;
import fi.luomus.lajiauth.model.AuthenticationSourceUser;
import fi.luomus.lajiauth.util.JWT;
import fi.luomus.lajiauth.util.LajiAuthPreferences;
import fi.luomus.utils.exceptions.ApiException;
import fi.luomus.utils.preferences.LuomusPreferences;

@Service
public class AppleService {

	private final LuomusPreferences preferences;
	private final Client client;
	private final ObjectMapper objectMapper;

	@Inject
	public AppleService(LuomusPreferences luomusPreferences, ObjectMapper objectMapper) {
		this.preferences = luomusPreferences;
		this.objectMapper = objectMapper;
		this.client = ClientBuilder.newClient();
	}

	/**
	 * Get an uri the users should be redirected to for authentication
	 *
	 * @param redirectUri The uri the user will be redirected to after authentication
	 * @param state State containing our forwarding params
	 * @return The redirect uri
	 */
	public URI getRedirectUri(URI redirectUri, String state) {
		try {
			return UriBuilder.fromPath("https://appleid.apple.com/auth/authorize")
					.queryParam("client_id", "fi.laji.auth")
					.queryParam("redirect_uri", redirectUri)
					.queryParam("response_type", "code id_token")
					.queryParam("response_mode", "form_post")
					.queryParam("scope", "name email")
					.queryParam("state", URLEncoder.encode(state, "UTF-8"))
					.build();
		} catch (Exception e) {
			throw new RuntimeException("failed to encode state", e);
		}
	}

	/**
	 * Get the user's details from auth response
	 *
	 * @param redirectUri The original redirect uri parameter for verification
	 * @param code The received access code
	 * @param user
	 * @return User's details
	 * @throws ApiException
	 */
	public AuthenticationSourceUser getUserDetails(URI redirectURI, String code, String user) throws ApiException {
		String idToken = getIdToken(redirectURI, code);
		return toAuthSourceUser(idToken, user);
	}

	/**
	 * Redeem the given code for an API access token
	 *
	 * @param redirectUri The original redirect uri parameter for verification
	 * @param code The received access code
	 * @return access token
	 * @throws ApiException
	 */
	private String getIdToken(URI redirectUri, String code) throws ApiException {
		Response response = client.target("https://appleid.apple.com/auth/token").request().post(Entity.form(new Form()
				.param("client_id", "fi.laji.auth")
				.param("client_secret", generateSecret())
				.param("code", code)
				.param("grant_type", "authorization_code")
				.param("redirect_uri", redirectUri.toString())
				));
		raiseIfNotSuccessful(response, "appleid.apple.com/auth/token");
		return response.readEntity(new GenericType<Map<String, String>>(){}).get("id_token");
	}

	private String generateSecret() {
		String token = JWT.generateJWT(
				preferences.get(LajiAuthPreferences.APPLE_KEY_ID).get(), // kid - keyId -- id ouf our private key
				preferences.get(LajiAuthPreferences.APPLE_TEAM_ID).get(), // iss - issuer -- our team id
				"https://appleid.apple.com", // aud - audience
				"fi.laji.auth", // sub - subject
				preferences.get(LajiAuthPreferences.APPLE_PRIVATE_KEY).get()); // our private key
		return token;
	}

	private AuthenticationSourceUser toAuthSourceUser(String jwtToken, String userJson) {
		try {
			AuthenticationSourceUser user = toUser(userJson);
			user.setId(parseSub(jwtToken));
			return user;
		} catch (IOException e) {
			throw new RuntimeException("cannot deserialize id_token", e);
		} catch (JSONException e) {
			throw new RuntimeException("cannot deserialize user json", e);
		}
	}

	private String parseSub(String jwtToken) throws UnsupportedEncodingException, IOException, JsonParseException, JsonMappingException {
		String[] chunks = jwtToken.split(Pattern.quote("."));
		String payload = chunks[1];
		String json = new String(Base64.decodeBase64(payload), "UTF-8");
		Map<String, String> map = objectMapper.readValue(json, new TypeReference<Map<String, String>>() {});
		return map.get("sub");
	}

	private AuthenticationSourceUser toUser(String userJson) throws JSONException {
		AuthenticationSourceUser user = new AuthenticationSourceUser();
		if (!given(userJson)) return user;

		JSONObject json = new JSONObject(userJson);
		if (json.has("email")) {
			user.setEmail(json.getString("email"));
		}
		if (json.has("name")) {
			JSONObject name = json.getJSONObject("name");
			if (name.has("firstName")) {
				user.setPreferredName(name.getString("firstName"));
			}
			if (name.has("lastName")) {
				user.setInheritedName(name.getString("lastName"));
			}
		}
		return user;
	}

	private boolean given(String s) {
		return s != null && !s.isEmpty();
	}

	private void raiseIfNotSuccessful(Response response, String request) throws ApiException {
		if (!response.getStatusInfo().getFamily().equals(Response.Status.Family.SUCCESSFUL)) {
			throw ApiException.builder().code(String.valueOf(response.getStatus())).source(AuthenticationSource.APPLE.name()).details(getDetails(response, request)).build();
		}
	}

	private String getDetails(Response response, String request) {
		try {
			return request + ": " + response.readEntity(String.class);
		} catch (Exception e) {
			return "Failed to read error details: " + e.getMessage();
		}
	}

}
