package fi.luomus.lajiauth.service;

import java.security.Principal;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.SecurityContext;

import org.springframework.stereotype.Service;

import fi.luomus.lajiauth.model.AuthenticationEvent;

@Service
public class SessionService {

	private static final int ONE_HOUR = 60*60;
	private final static String AUTH_TOKEN = "authToken";
	private final static String AUTH_INFO = "authenticationInfo";

	private final UserAuthenticatedService userAuthenticatedService;

	@Inject
	private HttpServletRequest request;

	@Inject
	public SessionService(UserAuthenticatedService userAuthenticatedService) {
		this.userAuthenticatedService = userAuthenticatedService;
	}

	public void authenticateSelf(String token, AuthenticationEvent authenticationInfo) {
		HttpSession session = getSession(true);
		session.setAttribute(AUTH_TOKEN, token);
		session.setAttribute(AUTH_INFO, authenticationInfo);
	}

	public void logoutSelf() {
		HttpSession session = getSession(false);
		if (session == null) return;

		Object tokenAttributeValue = session.getAttribute(AUTH_TOKEN);
		if (tokenAttributeValue == null) return;
		if (!(tokenAttributeValue instanceof String)) return;
		String token = (String) tokenAttributeValue;

		session.setAttribute(AUTH_TOKEN, null);
		session.setAttribute(AUTH_INFO, null);
		userAuthenticatedService.removeLogin(token);
	}

	public void authenticateContainerRequestForSelf(ContainerRequestContext requestContext) {
		String token = getTokenForSelf();
		if (token == null) return;
		final AuthenticationEvent authenticationInfo = getAuthenticationEventForSelf();
		requestContext.setProperty(AUTH_TOKEN, token);
		requestContext.setProperty(AUTH_INFO, authenticationInfo);
		requestContext.setSecurityContext(new SecurityContext() {
			@Override
			public Principal getUserPrincipal() {
				return new Principal() {
					@Override
					public String getName() {
						if (authenticationInfo.getUser().getQname().isPresent()) {
							return authenticationInfo.getUser().getQname().get();
						}
						return null;
					}
				};
			}

			@Override
			public boolean isUserInRole(String role) {
				return authenticationInfo.getUser().getRoles().contains(role);
			}

			@Override
			public boolean isSecure() {
				return true;
			}

			@Override
			public String getAuthenticationScheme() {
				return "laji-auth";
			}
		});

	}

	public boolean isAthenticatedForSelf() {
		AuthenticationEvent auth = getAuthenticationEventForSelf();
		if (auth == null) return false;
		return auth.getUser().getQname().isPresent();
	}

	private String getTokenForSelf() {
		HttpSession session = getSession(false);
		if (session == null) return null;
		Object tokenAttributeValue = session.getAttribute(AUTH_TOKEN);
		if (tokenAttributeValue == null) return null;
		if (!(tokenAttributeValue instanceof String)) return null;
		return (String) tokenAttributeValue;
	}

	public AuthenticationEvent getAuthenticationEventForSelf() {
		HttpSession session = getSession(false);
		if (session == null) return null;
		Object attributeValue = session.getAttribute(AUTH_INFO);
		if (attributeValue == null) return null;
		if (!(attributeValue instanceof AuthenticationEvent)) return null;
		return (AuthenticationEvent) attributeValue;
	}

	HttpSession getSession(boolean create) {
		HttpSession session = request.getSession(create);
		if (session != null) {
			session.setMaxInactiveInterval(ONE_HOUR);
		}
		return session;
	}

}
