package fi.luomus.lajiauth.service;

import java.net.URI;
import java.net.URLEncoder;
import java.util.Map;

import javax.inject.Inject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import org.springframework.stereotype.Service;

import fi.luomus.lajiauth.model.AuthenticationSource;
import fi.luomus.lajiauth.model.AuthenticationSourceUser;
import fi.luomus.lajiauth.util.LajiAuthPreferences;
import fi.luomus.utils.exceptions.ApiException;
import fi.luomus.utils.preferences.LuomusPreferences;

@Service
public class InaturalistService {

	private final LuomusPreferences preferences;
	private final Client client;

	@Inject
	public InaturalistService(LuomusPreferences luomusPreferences) {
		this.preferences = luomusPreferences;
		this.client = ClientBuilder.newClient();
	}

	/**
	 * Get an uri the users should be redirected to for authentication
	 *
	 * @param redirectUri The uri the user will be redirected to after authentication
	 * @param state State containing our forwarding params
	 * @return The redirect uri
	 */
	public URI getRedirectUri(String redirectUri, String state) {
		try {
			return UriBuilder.fromPath("https://www.inaturalist.org/oauth/authorize")
					.queryParam("client_id", preferences.get(LajiAuthPreferences.INATURALIST_APP_ID).get())
					.queryParam("redirect_uri", redirectUri)
					.queryParam("state", URLEncoder.encode(state, "UTF-8"))
					.queryParam("response_type", "code")
					.build();
		} catch (Exception e) {
			throw new RuntimeException("failed to encode state", e);
		}
	}

	/**
	 * Redeem the given code for an API access token
	 *
	 * @param redirectUri The original redirect uri parameter for verification
	 * @param code The received access code
	 * @return access token
	 * @throws ApiException
	 */
	private String getAccessToken(String redirectUri, String code) throws ApiException {
		Response response = client.target("https://www.inaturalist.org/oauth/token").request().post(
				Entity.form(new Form()
						.param("client_id", preferences.get(LajiAuthPreferences.INATURALIST_APP_ID).get())
						.param("client_secret", preferences.get(LajiAuthPreferences.INATURALIST_SECRET).get())
						.param("code", code)
						.param("redirect_uri", redirectUri.toString())
						.param("grant_type", "authorization_code")));
		raiseIfNotSuccessful(response, "www.inaturalist.org/oauth/token");

		return response.readEntity(new GenericType<Map<String, String>>(){}).get("access_token");
	}

	/**
	 * Get the user's details from auth response
	 *
	 * @param redirectUri The original redirect uri parameter for verification
	 * @param code The received access code
	 * @return User's details
	 * @throws ApiException
	 */
	public AuthenticationSourceUser getUserDetails(String redirectUri, String code) throws ApiException {
		String accessToken = getAccessToken(redirectUri, code);
		Response response = ClientBuilder.newClient().target("https://www.inaturalist.org/users/edit.json")
				.request().header("Authorization", "Bearer "+accessToken).get();

		raiseIfNotSuccessful(response, "www.inaturalist.org/users/edit.json");

		Map<String, String> userInfo = response.readEntity(new GenericType<Map<String, String>>(){});
		return toAuthSourceUser(userInfo);
	}

	private AuthenticationSourceUser toAuthSourceUser(Map<String, String> userInfo) {
		AuthenticationSourceUser user = new AuthenticationSourceUser();
		user.setId(userInfo.get("id"));
		user.setEmail(userInfo.get("email"));
		user.setFullName(userInfo.get("name"));
		user.setGroup(userInfo.get("login"));
		return user;
	}

	private void raiseIfNotSuccessful(Response response, String request) throws ApiException {
		if (!response.getStatusInfo().getFamily().equals(Response.Status.Family.SUCCESSFUL)) {
			throw ApiException.builder().code(String.valueOf(response.getStatus())).source(AuthenticationSource.INATURALIST.name()).details(getDetails(response, request)).build();
		}
	}

	private String getDetails(Response response, String request) {
		try {
			return request + ": " + response.readEntity(String.class);
		} catch (Exception e) {
			return "Failed to read error details: " + e.getMessage();
		}
	}

}
