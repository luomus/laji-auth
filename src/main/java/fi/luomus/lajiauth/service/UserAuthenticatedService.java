package fi.luomus.lajiauth.service;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.core.UriInfo;

import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Optional;

import fi.luomus.lajiauth.Utils;
import fi.luomus.lajiauth.model.AuthenticationEvent;
import fi.luomus.lajiauth.model.AuthenticationSource;
import fi.luomus.lajiauth.model.AuthenticationSourceUser;
import fi.luomus.lajiauth.model.OAuthState;
import fi.luomus.lajiauth.model.UserDetails;
import fi.luomus.lajiauth.repository.ValidLoginsRepository;
import fi.luomus.lajiauth.resource.user.Approval;
import fi.luomus.lajiauth.service.UserService.UserAttributes;
import fi.luomus.lajiauth.util.LajiAuthPreferences;
import fi.luomus.utils.exceptions.ApiException;
import fi.luomus.utils.preferences.LuomusPreferences;

@Service
public class UserAuthenticatedService {

	private final UserService userService;
	private final ObjectMapper objectMapper;
	private final ValidLoginsRepository validLoginsRepository;
	private final LuomusPreferences preferences;

	@Inject
	public UserAuthenticatedService(ValidLoginsRepository validLoginsRepository, UserService userService, ObjectMapper objectMapper, LuomusPreferences preferences) {
		this.validLoginsRepository = validLoginsRepository;
		this.userService = userService;
		this.objectMapper = objectMapper;
		this.preferences = preferences;
	}

	/**
	 * Generates an authentication event token and saves and logs the event.
	 * @param source
	 * @param user
	 * @param targetSystemId
	 * @param nextPath
	 * @param redirectMethod
	 * @param permanent
	 * @return
	 * @throws ApiException
	 */
	public String getTokenForLogin(AuthenticationSource source, AuthenticationSourceUser user, String targetSystemId, String nextPath, String redirectMethod, boolean permanent) throws ApiException {
		AuthenticationEvent authenticationEvent = buildEvent(source, user, targetSystemId, nextPath, redirectMethod, permanent);
		return getTokenForLogin(authenticationEvent);
	}

	public String getTokenForLogin(AuthenticationSource source, AuthenticationSourceUser user, OAuthState state) throws ApiException {
		AuthenticationEvent authenticationEvent = buildEvent(source, user, state.getTarget(), state.getNext(), state.getRedirectMethod(), state.isPermanent());
		return getTokenForLogin(authenticationEvent);
	}

	/**
	 * Generates an authentication event token and saves and logs the event.
	 * @param authenticationEvent
	 * @return
	 */
	public String getTokenForLogin(AuthenticationEvent authenticationEvent) {
		return getTokenForLogin(authenticationEvent, false);
	}

	/**
	 * Generaters an authentication event token for email change and saves and logs the event.
	 * @param authenticationEvent
	 * @return
	 */
	public String getTokenForEmailChangeLogin(AuthenticationEvent authenticationEvent) {
		return getTokenForLogin(authenticationEvent, true);
	}

	public void updateNext(String token, AuthenticationEvent event, String next) {
		if (next.equals(event.getNext())) return;
		event.setNext(next);
		String serialized = serialize(event);
		validLoginsRepository.updateValidLogin(token, serialized);
	}

	private String getTokenForLogin(AuthenticationEvent authenticationEvent, boolean emailChange) {
		String serialized = serialize(authenticationEvent);
		String token = Utils.generateToken();
		String userQname = authenticationEvent.getUser().getQname().orNull();
		String target = authenticationEvent.getTarget();
		boolean permanent = authenticationEvent.isPermanent() && authenticationEvent.getUser().getQname().isPresent();
		validLoginsRepository.addValidLogin(token, serialized, userQname, target, emailChange, permanent);
		log(authenticationEvent, token, serialized);
		return token;
	}

	private String serialize(AuthenticationEvent authenticationEvent) {
		try {
			return objectMapper.writeValueAsString(authenticationEvent);
		} catch (JsonProcessingException e) {
			throw new RuntimeException("Unable to serialize authentication", e);
		}
	}

	private AuthenticationEvent buildEvent(AuthenticationSource source, AuthenticationSourceUser user, String targetSystemId, String nextPath, String redirectMethod, boolean permanent) throws ApiException {
		AuthenticationEvent authenticationEvent = new AuthenticationEvent();
		authenticationEvent.setTarget(targetSystemId);
		authenticationEvent.setNext(nextPath);
		authenticationEvent.setSource(source);
		authenticationEvent.setPermanent(permanent);
		Optional<UserDetails> userDetails = userDetails(user.getId(), source);
		if (userDetails.isPresent()) {
			authenticationEvent.setUser(userDetails.get());
		} else {
			authenticationEvent.setUser(sourceUserToUser(user));
		}
		authenticationEvent.setRedirectMethod(redirectMethod);
		return authenticationEvent;
	}

	/**
	 * Get redirect URI where the token should be send for approval with confirmation screen shown to the user
	 * @param uriInfo
	 * @param token
	 * @return
	 */
	public URI getApprovalRedirect(UriInfo uriInfo, String token) {
		return uriInfo.getBaseUriBuilder()
				.path(Approval.APPROVAL)
				.queryParam(Approval.TOKEN, token).build();
	}

	private void log(AuthenticationEvent authenticationEvent, String token, String json) {
		try {
			validLoginsRepository.log(authenticationEvent, token, json);
		} catch (Exception e) {
			try {
				LoggerFactory.getLogger(ValidLoginsRepository.class).error("Logging a login failed", e);
			} catch (Exception logE) {}
		}
	}

	private Optional<UserDetails> userDetails(String authSourceUserId, AuthenticationSource authSource) throws ApiException {
		Optional<UserService.UserAttributes> userAttribs = userService.findUser(authSourceUserId, authSource);
		if (userAttribs.isPresent()) {
			UserAttributes attributes = userAttribs.get();
			UserDetails details = new UserDetails();
			details.setAuthSourceId(authSourceUserId);
			details.setQname(Optional.of(attributes.getId()));
			details.setEmail(attributes.getEmail());
			details.setGroup(Optional.fromNullable(attributes.getGroup()));
			details.setFirstJoined(Optional.fromNullable(attributes.getFirstJoined()));
			details.setName(attributes.getName());
			details.setInheritedName(Optional.fromNullable(attributes.getInheritedName()));
			details.setPreferredName(Optional.fromNullable(attributes.getPreferredName()));
			details.setYearOfBirth(Optional.fromNullable(attributes.getYearOfBirth()));
			details.getRoles().addAll(attributes.getRoles());
			details.setOrganisations(attributes.getOrganizations());
			details.setPreviousEmailAddresses(attributes.getPreviousEmailAddresses());
			details.setDefaultLanguage(Optional.fromNullable(attributes.getDefaultLanguage()));
			details.setAdditionalUserIds(attributes.getAdditionalUserIds());
			details.setAddress(Optional.fromNullable(attributes.getAddress()));
			return Optional.of(details);
		}
		return Optional.absent();
	}

	private UserDetails sourceUserToUser(AuthenticationSourceUser user) {
		UserDetails userDetails = new UserDetails();
		userDetails.setAuthSourceId(user.getId());
		userDetails.setInheritedName(Optional.fromNullable(user.getInheritedName()));
		userDetails.setPreferredName(Optional.fromNullable(user.getPreferredName()));
		userDetails.setName(user.getFullName());
		userDetails.setEmail(user.getEmail());
		userDetails.setQname(Optional.<String>absent());
		userDetails.setGroup(Optional.fromNullable(user.getGroup()));
		return userDetails;
	}

	/**
	 * Get authentication event using token
	 * @param token
	 * @return the event
	 * @throws IllegalStateException if no event found with the token
	 */
	public Optional<AuthenticationEvent> getAuthenticationInfo(String token) {
		String authenticationInfo = validLoginsRepository.getSerializedLoginInfo(token);
		if (authenticationInfo == null) {
			return Optional.absent();
		}
		try {
			return Optional.of(objectMapper.readValue(authenticationInfo, AuthenticationEvent.class));
		} catch (IOException e) {
			throw new RuntimeException("Unable to deserialize authentication", e);
		}
	}

	/**
	 * Get authentication event using token or fail
	 * @param token
	 * @return
	 * @throws IllegalStateException if no valid token
	 */
	public AuthenticationEvent getAuthenticationInfoOrFail(String token) throws IllegalStateException {
		Optional<AuthenticationEvent> event = getAuthenticationInfo(token);
		if (event.isPresent()) return event.get();
		throw new IllegalStateException("Non-valid token " + token);
	}

	/**
	 * Remove expired authentication events (14 days)
	 */
	public void removeExpiredLogins() {
		validLoginsRepository.removeExpiredLogins();
	}

	/**
	 * Remove all valid authentication events for the user
	 * @param userQname
	 */
	public void removeAllLogins(String userQname) {
		validLoginsRepository.removeLogins(userQname);
	}

	/**
	 * Remove authentication event using token of the event
	 * @param token
	 */
	public void removeLogin(String token) {
		removeAllLoginsToLajiAuthForUser(token);
		validLoginsRepository.removeLogin(token);
	}

	private void removeAllLoginsToLajiAuthForUser(String token) {
		try {
			Optional<AuthenticationEvent> event = getAuthenticationInfo(token);
			if (!event.isPresent()) return;
			if (event.get().getUser().getQname().isPresent()) {
				String userQname = event.get().getUser().getQname().get();
				String lajiAuthSystemQname = preferences.get(LajiAuthPreferences.LAJI_AUTH_SYSTEM_ID).get();
				validLoginsRepository.removeLogins(userQname, lajiAuthSystemQname);
			}
		} catch (Exception e) {
			// most likely token no longer exists
		}
	}

	/**
	 * Get URI where the user should go to confirm her email address
	 * @param uriInfo
	 * @param token
	 * @return
	 */
	public URI getConfirmLinkURI(UriInfo uriInfo, String token) {
		return uriInfo.getBaseUriBuilder().replaceQuery("").path(Approval.APPROVAL).path(Approval.CONFIRM).replaceQueryParam(Approval.TOKEN, token).build();
	}

	public static class PermanentLogin {
		private final String token;
		private final AuthenticationEvent event;
		private final Date lastUsed;
		public PermanentLogin(String token, AuthenticationEvent event, Date lastUsed) {
			this.token = token;
			this.event = event;
			this.lastUsed = lastUsed;
		}
		public AuthenticationEvent getEvent() {
			return event;
		}
		public Date getLastUsed() {
			return lastUsed;
		}
		public String getToken() {
			return token;
		}
	}

	public List<PermanentLogin> getPermanentLogins(AuthenticationEvent authInfo) {
		List<fi.luomus.lajiauth.repository.ValidLoginsRepository.PermanentLogin> datas = validLoginsRepository.getPermanentLogins(authInfo.getUser().getQname().get());
		if (datas.isEmpty()) return Collections.emptyList();

		List<PermanentLogin> logins = new ArrayList<>();
		for (fi.luomus.lajiauth.repository.ValidLoginsRepository.PermanentLogin data : datas) {
			try {
				AuthenticationEvent event = objectMapper.readValue(data.getJson(), AuthenticationEvent.class);
				logins.add(new PermanentLogin(data.getToken(), event, data.getLastUsed()));
			} catch (IOException e) {}
		}
		return logins;
	}

}
