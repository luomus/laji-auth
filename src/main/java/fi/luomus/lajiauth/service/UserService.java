package fi.luomus.lajiauth.service;

import static fi.luomus.triplestore.client.model.Constants.DEFAULT_NS_URI;
import static org.apache.jena.rdf.model.ResourceFactory.createProperty;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.inject.Inject;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.ResIterator;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;
import org.apache.jena.vocabulary.RDF;
import org.joda.time.DateTime;
import org.springframework.stereotype.Service;

import com.google.common.base.Optional;
import com.google.common.collect.ImmutableSet;

import fi.luomus.lajiauth.model.AuthenticationEvent;
import fi.luomus.lajiauth.model.AuthenticationSource;
import fi.luomus.lajiauth.model.ServerConstants;
import fi.luomus.lajiauth.model.UserDetails;
import fi.luomus.lajiauth.service.additional.ResolvedUser;
import fi.luomus.lajiauth.service.additional.UnresolvedUser;
import fi.luomus.lajiauth.service.additional.UserIdResolver;
import fi.luomus.lajiauth.service.additional.UserIdResolverService;
import fi.luomus.triplestore.client.TriplestoreClient;
import fi.luomus.triplestore.client.model.Constants;
import fi.luomus.triplestore.client.util.PredicateUpdateOperation;
import fi.luomus.utils.exceptions.ApiException;
import fi.luomus.utils.exceptions.NotFoundException;
import fi.luomus.utils.model.LocalizedString;

@Service
public class UserService {

	private static final String YEAR_OF_BIRTH_PREDICATE = "MA.yearOfBirth";
	private static final String PREFERRED_NAME_PREDICATE = "MA.preferredName";
	private static final String INHERITED_NAME_PREDICATE = "MA.inheritedName";
	private static final String ROLE_PREDICATE = "MA.role";
	private static final String ORGANISATION_PREDICATE = "MA.organisation";
	private static final String ORGANISATION_ADMIN_PREDICATE = "MA.organisationAdmin";
	private static final String PERSON_CLASS = "MA.person";
	private static final String EMAIL_ADDRESS_PREDICATE = "MA.emailAddress";
	private static final String GROUP_PREDICATE = "MA.group";
	private static final String SECURE_PORTAL_EXPIRES_PREDICATE = "MA.securePortalUserRoleExpires";
	private static final String SECURE_PORTAL_ROLE = "MA.securePortalUser";
	private static final String PREVIOUS_EMAIL_ADDRESS_PREDICATE = "MA.previousEmailAddress";
	private static final String FULL_NAME_PREDICATE = "MA.fullName";
	private static final String FIRST_JOINED_PREDICATE = "MA.firstJoined";
	private static final String DEFAULT_LANGUAGE_PREDICATE = "MA.defaultLanguage";
	private static final String ADDRESS_PREDICATE = "MA.address";
	private static final String RDFS_LABEL = "http://www.w3.org/2000/01/rdf-schema#label";
	private static final String ORGANISATION_LEVEL_1_PREDICATE = "MOS.organizationLevel1";
	private static final String ORGANISATION_LEVEL_2_PREDICATE = "MOS.organizationLevel2";
	private static final String ORGANISATION_LEVEL_3_PREDICATE = "MOS.organizationLevel3";
	private static final String ORGANISATION_LEVEL_4_PREDICATE = "MOS.organizationLevel4";
	private static final List<Property> ORGANISATION_LEVELS;
	static {
		ORGANISATION_LEVELS = new ArrayList<>();
		ORGANISATION_LEVELS.add(createProperty(DEFAULT_NS_URI, ORGANISATION_LEVEL_1_PREDICATE));
		ORGANISATION_LEVELS.add(createProperty(DEFAULT_NS_URI, ORGANISATION_LEVEL_2_PREDICATE));
		ORGANISATION_LEVELS.add(createProperty(DEFAULT_NS_URI, ORGANISATION_LEVEL_3_PREDICATE));
		ORGANISATION_LEVELS.add(createProperty(DEFAULT_NS_URI, ORGANISATION_LEVEL_4_PREDICATE));
	}

	private final TriplestoreClient triplestoreClient;
	private final LocalUserService localUserService;
	private final UserIdResolverService userIdResolverService;

	private static final Map<AuthenticationSource, String> SOURCE_TO_PREDICATE_MAPPING;
	static {
		SOURCE_TO_PREDICATE_MAPPING = new HashMap<>();
		SOURCE_TO_PREDICATE_MAPPING.put(AuthenticationSource.HAKA, "MA.hakaLoginName");
		SOURCE_TO_PREDICATE_MAPPING.put(AuthenticationSource.VIRTU, "MA.virtuLoginName");
		SOURCE_TO_PREDICATE_MAPPING.put(AuthenticationSource.FACEBOOK, "MA.facebookLoginName");
		SOURCE_TO_PREDICATE_MAPPING.put(AuthenticationSource.GOOGLE, "MA.googleLoginName");
		SOURCE_TO_PREDICATE_MAPPING.put(AuthenticationSource.LOCAL, "MA.lajiAuthLoginName");
		SOURCE_TO_PREDICATE_MAPPING.put(AuthenticationSource.INATURALIST, "MA.inaturalistLoginName");
		SOURCE_TO_PREDICATE_MAPPING.put(AuthenticationSource.OMARIISTA, "MA.omariistaLoginName");
		SOURCE_TO_PREDICATE_MAPPING.put(AuthenticationSource.APPLE, "MA.appleLoginName");
	}

	@Inject
	public UserService(TriplestoreClient triplestoreClient, LocalUserService localUserService, UserIdResolverService userIdResolverService) {
		this.triplestoreClient = triplestoreClient;
		this.localUserService = localUserService;
		this.userIdResolverService = userIdResolverService;
	}

	/**
	 *Get user with user id
	 *
	 * @param authSourceUserId The unique id used in the authentication source system (e.g. "teppo@yliopisto.fi"
	 * @param authSource       The source for the authentication (e.g. "HAKA")
	 * @return An optional containing the stored user attributes if a corresponding user is present
	 * @throws ApiException          An error occurred while communicating with the back-end
	 * @throws IllegalStateException Thrown if multiple users are associated with the authentication
	 */
	public Optional<UserAttributes> getUser(String userId) throws ApiException {
		if (userId == null) {
			throw new IllegalArgumentException("no nulls allowed");
		}
		return get(userId);
	}

	/**
	 * Find the user for the given authentication source user id and authentication source
	 *
	 * @param authSourceUserId The unique id used in the authentication source system (e.g. "teppo@yliopisto.fi"
	 * @param authSource       The source for the authentication (e.g. "HAKA")
	 * @return An optional containing the stored user attributes if a corresponding user is present
	 * @throws ApiException          An error occurred while communicating with the back-end
	 * @throws IllegalStateException Thrown if multiple users are associated with the authentication
	 */
	public Optional<UserAttributes> findUser(String authSourceUserId, AuthenticationSource authSource) throws ApiException {
		if (authSourceUserId == null || authSource == null) {
			throw new IllegalArgumentException("no nulls allowed");
		}
		String predicateName = getUsernamePredicateFor(authSource);
		return search(predicateName, authSourceUserId);
	}

	public Optional<UserAttributes> findUser(String email) throws ApiException {
		if (email == null) {
			throw new IllegalArgumentException("no nulls allowed");
		}
		email += "%"; // This causes case insensitive search
		return search(EMAIL_ADDRESS_PREDICATE, email);
	}

	private Optional<UserAttributes> get(String subject) throws ApiException {
		Optional<Model> search = triplestoreClient.getResource(subject);
		if (search.isPresent()) {
			return Optional.of(getAttributes(getResource(subject, search.get())));
		}
		return Optional.absent();
	}

	private Optional<UserAttributes> search(String predicateName, String value) throws ApiException {
		Model search = triplestoreClient.searchLiteral().predicate(predicateName).object(value).offset(0).limit(1000).execute();
		Property property = createProperty(Constants.DEFAULT_NS_URI, predicateName);
		ResIterator resIterator = search.listResourcesWithProperty(property);

		value = value.replace("%", ""); // remove wildcards again

		List<UserAttributes> results = new ArrayList<>();
		while (resIterator.hasNext()) {
			Resource next = resIterator.next();
			if (value.equalsIgnoreCase(next.getProperty(property).getString())) {
				results.add(getAttributes(next));
			}
		}

		if (results.size() == 1) {
			return Optional.of(results.get(0));
		} else if (results.size() > 1) {
			throw new IllegalStateException(String.format("multiple resources specify value %s for predicate %s", value, predicateName));
		} else {
			return Optional.absent();
		}
	}

	/**
	 * Get all authentication sources and source user id's using the users qname
	 * @param userQname
	 * @return
	 * @throws ApiException
	 */
	public Optional<Map<AuthenticationSource, String>> getAssociations(String userQname) throws ApiException {
		Optional<Model> resource = triplestoreClient.getResource(userQname);
		if (resource.isPresent()) {
			return Optional.of(getAssociations(resource.get().getResource(DEFAULT_NS_URI + userQname)));
		}
		return Optional.absent();
	}

	private Map<AuthenticationSource, String> getAssociations(Resource userResource) {
		Map<AuthenticationSource, String> associations = new HashMap<>();
		for (AuthenticationSource source : AuthenticationSource.values()) {
			Property sourceProperty = createAuthSourceProperty(source);
			if (userResource.hasProperty(sourceProperty)) {
				associations.put(source, userResource.getProperty(sourceProperty).getString());
			}
		}

		return associations;
	}

	/**
	 * Create a new user for the given authentication source user id, authentication source and name and email the user or source has provided
	 * @param locale
	 *
	 * @param user The paramaters of the user to be created
	 * @return The identifier for the newly created user
	 * @throws ApiException               An error occurred while communicating with the back-end
	 */
	public String createUser(AuthenticationEvent authenticationEvent, String locale) throws ApiException {
		authenticationEvent.getUser().setFirstJoined(Optional.of(now()));
		String personQname = triplestoreClient.getSequence("MA");
		Model model = ModelFactory.createDefaultModel();
		Resource resource = model.createResource(DEFAULT_NS_URI + personQname);
		resource.addProperty(RDF.type, model.createResource(DEFAULT_NS_URI + PERSON_CLASS));
		Property authSourceProperty = createAuthSourceProperty(authenticationEvent.getSource());
		resource.addProperty(authSourceProperty, authenticationEvent.getUser().getAuthSourceId());
		resource.addProperty(createProperty(DEFAULT_NS_URI, EMAIL_ADDRESS_PREDICATE), authenticationEvent.getUser().getEmail());
		resource.addProperty(createProperty(DEFAULT_NS_URI, FULL_NAME_PREDICATE), authenticationEvent.getUser().getName());
		resource.addProperty(createProperty(DEFAULT_NS_URI, PREFERRED_NAME_PREDICATE), authenticationEvent.getUser().getPreferredName().or(""));
		resource.addProperty(createProperty(DEFAULT_NS_URI, INHERITED_NAME_PREDICATE), authenticationEvent.getUser().getInheritedName().or(""));
		resource.addProperty(createProperty(DEFAULT_NS_URI, GROUP_PREDICATE), authenticationEvent.getUser().getGroup().or(""));
		resource.addProperty(createProperty(DEFAULT_NS_URI, FIRST_JOINED_PREDICATE), authenticationEvent.getUser().getFirstJoined().get());
		if (locale != null && locale.length() > 0) {
			resource.addProperty(createProperty(DEFAULT_NS_URI, DEFAULT_LANGUAGE_PREDICATE), locale);
			authenticationEvent.getUser().setDefaultLanguage(Optional.of(locale));
		}
		triplestoreClient.upsertResource(personQname, model);
		return personQname;
	}

	private String now() {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		return df.format(new Date());
	}

	private Property createAuthSourceProperty(AuthenticationSource source) {
		return createProperty(DEFAULT_NS_URI, getUsernamePredicateFor(source));
	}

	private String getUsernamePredicateFor(AuthenticationSource authSource) {
		String predicate = SOURCE_TO_PREDICATE_MAPPING.get(authSource);
		if (predicate != null) return predicate;
		throw new IllegalStateException("Invalid auth source " + authSource);
	}

	/**
	 * Associate user identified by qname with some source and source user id
	 * @param source
	 * @param authSourceUserId
	 * @param userQname
	 * @throws ApiException
	 * @throws NotFoundException
	 */
	public void associateUser(AuthenticationSource source, String authSourceUserId, String userQname) throws ApiException, NotFoundException {
		requireUserIdNotAlreadyAssociatedForThisSource(source, authSourceUserId);
		Model model = getModel(userQname);
		Property usernameProperyForSource = createAuthSourceProperty(source);
		requireNoOtherUserIdAlreadyAssociatedForThisSource(source, userQname, usernameProperyForSource, model);
		Resource resource = getResource(userQname, model);
		resource.addProperty(usernameProperyForSource, authSourceUserId);
		triplestoreClient.upsertResource(userQname, model);
	}

	private Model getModel(String userId) throws ApiException, NotFoundException {
		Optional<Model> personOptional = triplestoreClient.getResource(userId);
		if (!personOptional.isPresent()) {
			throw new NotFoundException(String.format("User qname %s does not exist", userId));
		}
		return personOptional.get();
	}

	private void requireUserIdNotAlreadyAssociatedForThisSource(AuthenticationSource source, String authSourceUserId) throws ApiException {
		if (this.findUser(authSourceUserId, source).isPresent()) {
			String errorMessage = String.format("User id \"%s\" from authentication system %s already exists", authSourceUserId, source.name());
			throw new UserAlreadyExistsException(errorMessage);
		}
	}

	private void requireNoOtherUserIdAlreadyAssociatedForThisSource(AuthenticationSource source, String userQname, Property usernameProperyForSource, Model model) {
		StmtIterator i = model.listStatements();
		while (i.hasNext()) {
			Statement s = i.next();
			if (s.getPredicate().getURI().equals(usernameProperyForSource.getURI())) {
				String existingUsername = s.getLiteral().getString();
				String errorMessage = String.format("User %s already has authentication for system %s: \"%s\"", userQname, source.name(), existingUsername);
				throw new UserAlreadyExistsException(errorMessage);
			}
		}
	}

	public static class UserAlreadyExistsException extends RuntimeException {
		private static final long serialVersionUID = 3724719976264383538L;
		public UserAlreadyExistsException(String msg) {
			super(msg);
		}
	}

	private UserAttributes getAttributes(Resource resource) {
		final String id = resource.getLocalName();
		final String name = readProperty(resource, FULL_NAME_PREDICATE);
		final String email = readProperty(resource, EMAIL_ADDRESS_PREDICATE);
		final String group = readProperty(resource, GROUP_PREDICATE);
		final String securePortalUserRoleExpires = readProperty(resource, SECURE_PORTAL_EXPIRES_PREDICATE);
		final String firstJoined = readProperty(resource, FIRST_JOINED_PREDICATE);
		final String inheritedName = readProperty(resource, INHERITED_NAME_PREDICATE);
		final String preferredName = readProperty(resource, PREFERRED_NAME_PREDICATE);
		final Integer yearOfBirth = readIntegerProperty(resource, YEAR_OF_BIRTH_PREDICATE);
		final Set<String> roles = setOf(resource, ROLE_PREDICATE);
		final Set<String> organisations = setOf(resource, ORGANISATION_PREDICATE);
		final Set<String> adminOfOrganisations = setOf(resource, ORGANISATION_ADMIN_PREDICATE);
		final Set<String> previousEmails = previousEmails(resource);
		final String defaultLanguage = readProperty(resource, DEFAULT_LANGUAGE_PREDICATE);
		final String address = readProperty(resource, ADDRESS_PREDICATE);
		final Map<String, List<String>> additionalUserIds = readAdditionalUserIds(resource);
		return new UserAttributes() {
			@Override
			public String getId() {
				return id;
			}

			@Override
			public String getName() {
				return name;
			}

			@Override
			public String getEmail() {
				return email == null ? null : email.toLowerCase().trim();
			}

			@Override
			public String getGroup() {
				return group;
			}

			@Override
			public Set<String> getRoles() {
				return roles;
			}

			@Override
			public String getFirstJoined() {
				return firstJoined;
			}

			@Override
			public String getInheritedName() {
				return inheritedName;
			}

			@Override
			public String getPreferredName() {
				return preferredName;
			}

			@Override
			public Integer getYearOfBirth() {
				return yearOfBirth;
			}

			@Override
			public Set<String> getOrganizations() {
				return organisations;
			}

			@Override
			public Set<String> getPreviousEmailAddresses() {
				return previousEmails;
			}

			@Override
			public String getDefaultLanguage() {
				return defaultLanguage;
			}

			@Override
			public Map<String, List<String>> getAdditionalUserIds() {
				return additionalUserIds;
			}

			@Override
			public String getAddress() {
				return address;
			}

			@Override
			public String getSecurePortalUserRoleExpires() {
				return securePortalUserRoleExpires;
			}

			@Override
			public Set<String> getAdminOfOrganizations() {
				return adminOfOrganisations;
			}
		};
	}

	private Map<String, List<String>> readAdditionalUserIds(Resource resource) {
		Map<String, List<String>> map = new LinkedHashMap<>();
		for (Map.Entry<String, String> e : ServerConstants.ADDITIONAL_SYSTEMS_MAP.entrySet()) {
			String propertyName = e.getKey();
			String system = e.getValue();
			StmtIterator iterator = resource.listProperties(createProperty(DEFAULT_NS_URI, propertyName));
			while (iterator.hasNext()) {
				if (!map.containsKey(system)) {
					map.put(system, new ArrayList<String>());
				}
				map.get(system).add(iterator.next().getString());
			}
		}
		return map;
	}

	private Set<String> previousEmails(Resource resource) {
		ImmutableSet.Builder<String> builder = ImmutableSet.builder();
		StmtIterator iterator = resource.listProperties(createProperty(DEFAULT_NS_URI, PREVIOUS_EMAIL_ADDRESS_PREDICATE));
		while (iterator.hasNext()) {
			builder = builder.add(iterator.next().getString());
		}
		return builder.build();
	}

	private Set<String> setOf(Resource resource, String predicate) {
		ImmutableSet.Builder<String> builder = ImmutableSet.builder();
		StmtIterator iterator = resource.listProperties(createProperty(DEFAULT_NS_URI, predicate));
		while (iterator.hasNext()) {
			Statement s = iterator.next();
			if (s.getObject().isResource()) {
				builder.add(s.getObject().asResource().getLocalName());
			} else {
				builder.add(s.getLiteral().getString());
			}
		}
		return builder.build();
	}

	private String readProperty(Resource resource, String propertyName) {
		Property property = createProperty(Constants.DEFAULT_NS_URI, propertyName);
		return resource.hasProperty(property) ? resource.getProperty(property).getString() : null;
	}

	private Integer readIntegerProperty(Resource resource, String propertyName) {
		String s = readProperty(resource, propertyName);
		if (s == null) return null;
		try {
			return Integer.valueOf(s);
		} catch (NumberFormatException e) {
			return null;
		}
	}

	public interface UserAttributes {
		String getId();
		String getName();
		String getEmail();
		String getGroup();
		Set<String> getRoles();
		String getSecurePortalUserRoleExpires();
		String getFirstJoined();
		String getInheritedName();
		String getPreferredName();
		Integer getYearOfBirth();
		Set<String> getOrganizations();
		Set<String> getAdminOfOrganizations();
		Set<String> getPreviousEmailAddresses();
		String getDefaultLanguage();
		String getAddress();
		Map<String, List<String>> getAdditionalUserIds();
	}

	public Map<String, String> getRoleDescriptions(AuthenticationEvent authInfo, String locale) throws ApiException {
		if (authInfo.getUser().getRoles().isEmpty()) return null;
		Map<String, String> descriptions = new HashMap<>();
		for (String role : authInfo.getUser().getRoles()) {
			descriptions.put(role, getRoleDescription(role, locale));
		}
		return descriptions;
	}

	public Map<String, String> getOrganisationDescriptions(Set<String> organisationIds, String locale) throws ApiException {
		Map<String, String> descriptions = new HashMap<>();
		if (organisationIds == null) return descriptions;
		for (String organisation : organisationIds) {
			descriptions.put(organisation, getOrganisationDescription(organisation, locale));
		}
		return descriptions;
	}

	private String getOrganisationDescription(String organisationQname, String locale) throws ApiException {
		Optional<Model> model = triplestoreClient.getResource(organisationQname);
		if (!model.isPresent()) return organisationQname;
		Resource resource = model.get().getResource(DEFAULT_NS_URI + organisationQname);
		String description = getOrganisationDescription(resource, locale);
		if (!description.isEmpty()) return description;
		description = getOrganisationDescription(resource, "en");
		if (!description.isEmpty()) return description;
		return organisationQname;
	}

	private String getOrganisationDescription(Resource resource, String locale) {
		StringBuilder b = new StringBuilder();
		for (Property p : ORGANISATION_LEVELS) {
			String desc = getDescription(resource, p, locale);
			if (!desc.isEmpty()) {
				if (b.length() > 0) {
					b.append(", ");
				}
				b.append(desc);
			}
		}
		return b.toString();
	}

	private String getDescription(Resource resource, Property p, String locale) {
		if (!resource.hasProperty(p)) return "";
		StmtIterator i = resource.listProperties(p);
		while (i.hasNext()) {
			Statement s = i.nextStatement();
			try {
				if (locale.equals(s.getLanguage())) {
					String desc = s.getString();
					if (desc != null && !desc.isEmpty()) {
						return desc;
					}
				}
			} catch (Exception e) {
				// non literal... (should be literal)
			}
		}
		return "";
	}

	private String getRoleDescription(String roleQname, String locale) throws ApiException {
		Optional<Model> model = triplestoreClient.getResource(roleQname);
		if (!model.isPresent()) return roleQname;
		Resource resource = getResource(roleQname, model.get());
		String description = getLabel(resource, locale);
		if (!description.isEmpty()) return description;
		description = getLabel(resource, "en");
		if (!description.isEmpty()) return description;
		return roleQname;
	}

	private String getLabel(Resource resource, String locale) {
		return getDescription(resource, createProperty(RDFS_LABEL), locale);
	}

	public void changeName(AuthenticationEvent authInfo) throws ApiException {
		UserDetails user = authInfo.getUser();
		String userQname = user.getQname().get();
		LocalizedString fullname = createLocalizedStringWithNullLocale(user.getName());
		LocalizedString inheritedName = createLocalizedStringWithNullLocale(user.getInheritedName().get());
		LocalizedString preferredName = createLocalizedStringWithNullLocale(user.getPreferredName().get());
		LocalizedString group = createLocalizedStringWithNullLocale(user.getGroup().or(""));

		triplestoreClient.updatePredicate(
				PredicateUpdateOperation.builder()
				.withResource(userQname)
				.withPredicate(FULL_NAME_PREDICATE)
				.withValue(fullname).build());

		triplestoreClient.updatePredicate(
				PredicateUpdateOperation.builder()
				.withResource(userQname)
				.withPredicate(INHERITED_NAME_PREDICATE)
				.withValue(inheritedName).build());

		triplestoreClient.updatePredicate(
				PredicateUpdateOperation.builder()
				.withResource(userQname)
				.withPredicate(PREFERRED_NAME_PREDICATE)
				.withValue(preferredName).build());

		triplestoreClient.updatePredicate(
				PredicateUpdateOperation.builder()
				.withResource(userQname)
				.withPredicate(GROUP_PREDICATE)
				.withValue(group).build());
	}

	private LocalizedString createLocalizedStringWithNullLocale(String string) {
		return LocalizedString.localizedString().with(null, string).get();
	}

	public void changeYearOfBirth(AuthenticationEvent authInfo) throws ApiException {
		UserDetails user = authInfo.getUser();
		String userQname = user.getQname().get();
		LocalizedString yearOfBirth = createLocalizedStringWithNullLocale(user.getYearOfBirth().get().toString());

		triplestoreClient.updatePredicate(
				PredicateUpdateOperation.builder()
				.withResource(userQname)
				.withPredicate(YEAR_OF_BIRTH_PREDICATE)
				.withValue(yearOfBirth).build());
	}

	public void changeEmail(AuthenticationEvent authInfo, String currentEmail) throws ApiException, NotFoundException {
		String newEmail = authInfo.getUser().getEmail().toLowerCase().trim();
		String userQname = authInfo.getUser().getQname().get();

		Model model = getModel(userQname);
		Resource resource = getResource(userQname, model);

		Property prevousAddressesPredicate = createProperty(DEFAULT_NS_URI, PREVIOUS_EMAIL_ADDRESS_PREDICATE);
		Property emailAddressPredicate = createProperty(DEFAULT_NS_URI, EMAIL_ADDRESS_PREDICATE);
		Property localLoginPredicate = createAuthSourceProperty(AuthenticationSource.LOCAL);

		resource.addProperty(prevousAddressesPredicate, currentEmail);
		resource.removeAll(emailAddressPredicate);
		resource.addProperty(emailAddressPredicate, newEmail);

		if (resource.hasProperty(localLoginPredicate)) {
			String oldLocalUsername = resource.getProperty(localLoginPredicate).getString();
			localUserService.setEmail(oldLocalUsername, newEmail);
			resource.removeAll(localLoginPredicate);
			resource.addLiteral(localLoginPredicate, newEmail);
		}

		triplestoreClient.upsertResource(userQname, model);
	}

	public void changeDefaultLanguage(AuthenticationEvent authInfo) throws ApiException {
		UserDetails user = authInfo.getUser();
		String userQname = user.getQname().get();
		LocalizedString defaultLanguage = createLocalizedStringWithNullLocale(user.getDefaultLanguage().get());

		triplestoreClient.updatePredicate(
				PredicateUpdateOperation.builder()
				.withResource(userQname)
				.withPredicate(DEFAULT_LANGUAGE_PREDICATE)
				.withValue(defaultLanguage).build());
	}

	public void changeAddress(AuthenticationEvent authInfo) throws ApiException {
		UserDetails user = authInfo.getUser();
		String userQname = user.getQname().get();
		LocalizedString address = createLocalizedStringWithNullLocale(user.getAddress().or(""));

		triplestoreClient.updatePredicate(
				PredicateUpdateOperation.builder()
				.withResource(userQname)
				.withPredicate(ADDRESS_PREDICATE)
				.withValue(address).build());
	}

	public static class AdditionalUserIdLinkingResult {
		private final boolean success;
		private final boolean alreadyLinked;
		private final Set<String> linkedSystems;
		private final int linkedUserIdCount;
		private AdditionalUserIdLinkingResult(Set<String> linkedSystems, int linkedUserIdCount) {
			this.success = true;
			this.alreadyLinked = false;
			this.linkedSystems = linkedSystems;
			this.linkedUserIdCount = linkedUserIdCount;
		}
		private AdditionalUserIdLinkingResult(boolean alreadyLinked) {
			this.success = false;
			this.alreadyLinked = alreadyLinked;
			this.linkedSystems = Collections.emptySet();
			this.linkedUserIdCount = 0;
		}
		private static AdditionalUserIdLinkingResult success(Set<String> linkedSystems, int linkedUserIdCount) {
			return new AdditionalUserIdLinkingResult(linkedSystems, linkedUserIdCount);
		}
		private static AdditionalUserIdLinkingResult failed() {
			return new AdditionalUserIdLinkingResult(false);
		}
		private static AdditionalUserIdLinkingResult failedAlreadyLinked() {
			return new AdditionalUserIdLinkingResult(true);
		}
		public boolean isSuccess() {
			return success;
		}
		public boolean isAlreadyLinked() {
			return alreadyLinked;
		}
		public Set<String> getLinkedSystems() {
			return linkedSystems;
		}
		public int getLinkedUserIdCount() {
			return linkedUserIdCount;
		}
	}

	public AdditionalUserIdLinkingResult addAdditionalUserIds(AuthenticationEvent authInfo, String system, String username, String password) throws ApiException, NotFoundException {
		String userQname = authInfo.getUser().getQname().get();

		UnresolvedUser userInformation = new UnresolvedUser(system, username, password);
		UserIdResolver resolver = userIdResolverService.getResolver(authInfo).resolve(userInformation);

		if (userInformation.isUnresolved()) {
			return AdditionalUserIdLinkingResult.failed();
		}

		if (linkedToOtherUser(userInformation.getResolved(), userQname)) {
			return AdditionalUserIdLinkingResult.failedAlreadyLinked();
		}

		if (resolver.getNewResolved().isEmpty()) {
			return AdditionalUserIdLinkingResult.failedAlreadyLinked();
		}

		Model model = getModel(userQname);
		Resource resource = getResource(userQname, model);
		Set<String> successfulSystems = new HashSet<>();
		int userIdCount = 0;

		for (ResolvedUser resolved : resolver.getNewResolved()) {
			if (linkedToOtherUser(resolved, userQname)) {
				continue;
			}
			resource.addLiteral(createAdditionalSystemProperty(resolved.getSystem()), resolved.getUsername());
			addToCurrentAuthentication(authInfo, resolved);
			successfulSystems.add(resolved.getSystem());
			userIdCount++;
		}

		triplestoreClient.upsertResource(userQname, model);

		return AdditionalUserIdLinkingResult.success(successfulSystems, userIdCount);
	}

	private Resource getResource(String subject, Model model) {
		return model.getResource(DEFAULT_NS_URI + subject);
	}

	private void addToCurrentAuthentication(AuthenticationEvent authInfo, ResolvedUser resolved) {
		Map<String, List<String>> ids = authInfo.getUser().getAdditionalUserIds();
		if (!ids.containsKey(resolved.getSystem())) {
			ids.put(resolved.getSystem(), new ArrayList<String>());
		}
		ids.get(resolved.getSystem()).add(resolved.getUsername());
	}

	private boolean linkedToOtherUser(ResolvedUser resolved, String userQname) throws ApiException {
		Model search = triplestoreClient.searchLiteral().predicate(getAdditionalSystemPredicate(resolved.getSystem())).object(resolved.getUsername()).offset(0).limit(1000).execute();
		StmtIterator iter = search.listStatements();
		String userURI = DEFAULT_NS_URI + userQname;
		while (iter.hasNext()) {
			Resource subject = iter.next().getSubject();
			if (!userURI.equals(subject.getURI())) {
				return true;
			}
		}
		return false;
	}

	private Property createAdditionalSystemProperty(String system) {
		return createProperty(DEFAULT_NS_URI, getAdditionalSystemPredicate(system));
	}

	private String getAdditionalSystemPredicate(String system) {
		for (Map.Entry<String, String> e : ServerConstants.ADDITIONAL_SYSTEMS_MAP.entrySet()) {
			if (system.equals(e.getValue())) {
				return e.getKey();
			}
		}
		throw new IllegalStateException("Unknown system " + system);
	}

	public void addSecurePortalRoleAndOrganisations(String targetUserId, Set<String> organisations, DateTime expireDate) throws ApiException, NotFoundException {
		Model model = getModel(targetUserId);
		Resource resource = getResource(targetUserId, model);

		addSecurePortalRole(model, resource);
		removeSecurePortalRoleExpireDate(resource);
		addSecurePortalExpireDate(expireDate, resource);
		addOrganisations(organisations, model, resource);

		triplestoreClient.upsertResource(targetUserId, model);
	}

	private void addOrganisations(Set<String> organisations, Model model, Resource resource) {
		for (String org : organisations) {
			resource.addProperty(createProperty(DEFAULT_NS_URI, ORGANISATION_PREDICATE), model.createResource(DEFAULT_NS_URI + org));
		}
	}

	private void addSecurePortalExpireDate(DateTime expireDate, Resource resource) {
		resource.addProperty(createProperty(DEFAULT_NS_URI, SECURE_PORTAL_EXPIRES_PREDICATE), toIso(expireDate));
	}

	private void removeSecurePortalRoleExpireDate(Resource resource) {
		resource.removeAll(createProperty(DEFAULT_NS_URI, SECURE_PORTAL_EXPIRES_PREDICATE));
	}

	private void addSecurePortalRole(Model model, Resource resource) {
		resource.addProperty(createProperty(DEFAULT_NS_URI, ROLE_PREDICATE), model.createResource(DEFAULT_NS_URI + SECURE_PORTAL_ROLE));
	}

	private String toIso(DateTime expireDate) {
		return expireDate.toString("yyyy-MM-dd");
	}

	public void removeSecurePortalRoleAndOrganisations(String targetUserId, Set<String> adminOfOrganizations) throws ApiException, NotFoundException {
		Model model = getModel(targetUserId);
		Resource resource = getResource(targetUserId, model);

		removeSecurePortalRole(resource);
		removeSecurePortalRoleExpireDate(resource);
		removeOrganisations(adminOfOrganizations, resource);

		triplestoreClient.upsertResource(targetUserId, model);
	}

	private void removeSecurePortalRole(Resource resource) {
		StmtIterator i = resource.listProperties(createProperty(DEFAULT_NS_URI, ROLE_PREDICATE));
		while (i.hasNext()) {
			if (SECURE_PORTAL_ROLE.equals(i.next().getObject().asResource().getLocalName())) {
				i.remove();
			}
		}
	}

	private void removeOrganisations(Set<String> organisations, Resource resource) {
		StmtIterator i = resource.listProperties(createProperty(DEFAULT_NS_URI, ORGANISATION_PREDICATE));
		while (i.hasNext()) {
			String organisationId = i.next().getObject().asResource().getLocalName();
			if (organisations.contains(organisationId)) {
				i.remove();
			}
		}
	}

	public boolean hasSecurePortalRole(UserAttributes user) {
		if (user.getRoles() == null) return false;
		return user.getRoles().contains(SECURE_PORTAL_ROLE);
	}

	public void deleteAccount(AuthenticationEvent authInfo) throws ApiException, NotFoundException {
		authInfo.getUser().setName("Poistettu / Removed");
		authInfo.getUser().setPreferredName(Optional.of("Poistettu / Removed"));
		authInfo.getUser().setInheritedName(Optional.of("Poistettu / Removed"));
		authInfo.getUser().setGroup(Optional.fromNullable(""));

		changeName(authInfo);

		String userQname = authInfo.getUser().getQname().get();
		Model model = getModel(userQname);
		Resource resource = getResource(userQname, model);
		for (String userIdPredicate : SOURCE_TO_PREDICATE_MAPPING.values()) {
			resource.removeAll(createProperty(DEFAULT_NS_URI, userIdPredicate));
		}
		triplestoreClient.upsertResource(userQname, model);
	}

}
