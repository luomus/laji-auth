package fi.luomus.lajiauth.service;

import static org.apache.jena.rdf.model.ResourceFactory.createProperty;

import java.net.URI;
import java.net.URISyntaxException;

import javax.inject.Inject;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;
import org.springframework.stereotype.Service;

import com.google.common.base.Optional;

import fi.luomus.lajiauth.resource.user.SelfAuthenticate;
import fi.luomus.lajiauth.util.LajiAuthPreferences;
import fi.luomus.triplestore.client.TriplestoreClient;
import fi.luomus.triplestore.client.model.Constants;
import fi.luomus.utils.exceptions.ApiException;
import fi.luomus.utils.preferences.LuomusPreferences;

@Service
public class SystemService {

	private final TriplestoreClient triplestoreClient;
	private final LuomusPreferences luomusPreferences;

	@Inject
	public SystemService(TriplestoreClient triplestoreClient, LuomusPreferences luomusPreferences) {
		this.triplestoreClient = triplestoreClient;
		this.luomusPreferences = luomusPreferences;
	}

	/**
	 * Get login redirect URI for the given system
	 * @param systemQname qname of the system
	 * @return The uri
	 * @throws ApiException An error occured while communicating with Triplestore
	 */
	public Optional<URI> getSystemRedirectURI(String systemQname) throws ApiException {
		Optional<Model> resource = triplestoreClient.getResource(systemQname);
		if (resource.isPresent()) {
			StmtIterator uriStmts = resource.get().getResource(Constants.DEFAULT_NS_URI + systemQname)
					.listProperties(createProperty(Constants.DEFAULT_NS_URI, "KE.lajiAuthLoginRedirectURI"));
			if (!uriStmts.hasNext()) {
				return Optional.absent();
			}
			try {
				String uri = uriStmts.next().getString();
				return Optional.of(new URI(uri));
			} catch (URISyntaxException e) {
				return Optional.absent();
			}
		}
		return Optional.absent();
	}

	private URI lajiAuthBaseURI = null;

	/**
	 * Get the external uri of the current laji-auth instance
	 * @return The external URI
	 * @throws ApiException An error occurred while communicating with Triplestore
	 */
	public URI getLajiAuthBaseURI() {
		if (lajiAuthBaseURI != null) return lajiAuthBaseURI;
		try {
			String s = getSystemRedirectURI(luomusPreferences.get(LajiAuthPreferences.LAJI_AUTH_SYSTEM_ID).get()).get().toString();
			s = s.replace("/" + SelfAuthenticate.AUTHENTICATE, "");
			lajiAuthBaseURI = new URI(s);
			return lajiAuthBaseURI;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Get the name of the system that corresponds to the given system id
	 * @param systemQname Qname of the system
	 * @param language
	 * @return An absent optional if the system does not exist, an optional containing an absent optional if the
	 * system exists but doesn't have a name, an optional containing an optional containing the name of the system
	 * if the system exists and it has a name
	 * @throws ApiException An error occurred while communicating with Triplestore
	 */
	public Optional<Optional<String>> getSystemName(String systemQname, String forLanguage) throws ApiException {
		Optional<Model> resource = triplestoreClient.getResource(systemQname);
		if (!resource.isPresent()) return Optional.absent();

		StmtIterator nameStmts = resource.get().getResource(Constants.DEFAULT_NS_URI + systemQname)
				.listProperties(createProperty(Constants.DEFAULT_NS_URI, "KE.name"));
		String nameForNoLanguage = null;
		String nameForSomeLanguage = null;
		while (nameStmts.hasNext()) {
			Statement name = nameStmts.next();
			if (name.getLanguage() == null) {
				nameForNoLanguage = name.getString();
			} else if (name.getLanguage().equals(forLanguage)) {
				return Optional.of(Optional.of(name.getString()));
			} else {
				nameForSomeLanguage = name.getString();
			}
		}
		if (nameForNoLanguage != null) {
			return Optional.of(Optional.of(nameForNoLanguage));
		}
		if (nameForSomeLanguage != null) {
			return Optional.of(Optional.of(nameForSomeLanguage));
		}
		return Optional.of(Optional.<String>absent());
	}

	/**
	 * Tells if system with the given ID exists
	 * @param systemId Qname of the system
	 * @return true if exists, false if not
	 * @throws ApiException An error occurred while communicating with the Triplestore
	 */
	public boolean exists(String systemId) throws ApiException {
		Optional<URI> redirectURI = getSystemRedirectURI(systemId);
		return redirectURI.isPresent();
	}

}
