package fi.luomus.lajiauth.service;

import java.net.URI;
import java.net.URLEncoder;
import java.util.Map;

import javax.inject.Inject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import org.springframework.stereotype.Service;

import fi.luomus.lajiauth.model.AuthenticationSource;
import fi.luomus.lajiauth.model.AuthenticationSourceUser;
import fi.luomus.lajiauth.util.LajiAuthPreferences;
import fi.luomus.utils.exceptions.ApiException;
import fi.luomus.utils.preferences.LuomusPreferences;

@Service
public class OmariistaService {

	private final LuomusPreferences preferences;
	private final Client client;

	@Inject
	public OmariistaService(LuomusPreferences luomusPreferences) {
		this.preferences = luomusPreferences;
		HttpAuthenticationFeature feature = HttpAuthenticationFeature.basic(
				preferences.get(LajiAuthPreferences.OMARIISTA_APP_ID).or("missing"),
				preferences.get(LajiAuthPreferences.OMARIISTA_SECRET).or("missing"));
		this.client = ClientBuilder.newClient().register(feature);
	}

	/**
	 * Get an uri the users should be redirected to for authentication
	 *
	 * @param redirectUri The uri the user will be redirected to after authentication
	 * @param state State containing our forwarding params
	 * @return The redirect uri
	 */
	public URI getRedirectUri(String redirectUri, String state) {
		try {
			return UriBuilder.fromPath("https://id.oma.riista.fi/oauth/authorize")
					.queryParam("scope", "openid")
					.queryParam("response_type", "code")
					.queryParam("client_id", preferences.get(LajiAuthPreferences.OMARIISTA_APP_ID).get())
					.queryParam("redirect_uri", redirectUri)
					.queryParam("state", URLEncoder.encode(state, "UTF-8"))
					.build();
		} catch (Exception e) {
			throw new RuntimeException("failed to encode state", e);
		}
	}

	/**
	 * Redeem the given code for an API access token
	 *
	 * @param redirectUri The original redirect uri parameter for verification
	 * @param code The received access code
	 * @return access token
	 * @throws ApiException
	 */
	private String getAccessToken(String redirectUri, String code) throws ApiException {
		Response response = client.target("https://id.oma.riista.fi/oauth/token").request().post(
				Entity.form(new Form()
						.param("grant_type", "authorization_code")
						.param("code", code)
						.param("redirect_uri", redirectUri.toString())));

		raiseIfNotSuccessful(response, "id.omariista.fi/oauth/token");

		return response.readEntity(new GenericType<Map<String, String>>(){}).get("access_token");
	}

	/**
	 * Get the user's details from auth response
	 *
	 * @param redirectUri The original redirect uri parameter for verification
	 * @param code The received access code
	 * @return User's details
	 * @throws ApiException
	 */
	public AuthenticationSourceUser getUserDetails(String redirectUri, String code) throws ApiException {
		String accessToken = getAccessToken(redirectUri, code);
		Response response = ClientBuilder.newClient().target("https://id.oma.riista.fi/me")
				.request().header("Authorization", "Bearer "+accessToken).get();

		raiseIfNotSuccessful(response, "id.omariista.fi/me");

		Map<String, String> userInfo = response.readEntity(new GenericType<Map<String, String>>(){});
		return toAuthSourceUser(userInfo);
	}

	private AuthenticationSourceUser toAuthSourceUser(Map<String, String> userInfo) {
		AuthenticationSourceUser user = new AuthenticationSourceUser();
		user.setId(userInfo.get("personId"));
		user.setEmail(userInfo.get("email"));
		String preferredName = userInfo.get("byName");
		if (preferredName == null || preferredName.isEmpty()) {
			preferredName = userInfo.get("firstName");
		}
		user.setPreferredName(preferredName);
		user.setInheritedName(userInfo.get("lastName"));
		return user;
	}

	private void raiseIfNotSuccessful(Response response, String request) throws ApiException {
		if (!response.getStatusInfo().getFamily().equals(Response.Status.Family.SUCCESSFUL)) {
			throw ApiException.builder().code(String.valueOf(response.getStatus())).source(AuthenticationSource.OMARIISTA.name()).details(getDetails(response, request)).build();
		}
	}

	private String getDetails(Response response, String request) {
		try {
			return request + ": " + response.readEntity(String.class);
		} catch (Exception e) {
			return "Failed to read error details: " + e.getMessage();
		}
	}

}
