package fi.luomus.lajiauth.service;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Service;

import fi.luomus.lajiauth.model.Constants;

@Service
public class LanguageService {

	private final static String SESSION_LANGUAGE = Constants.LANGUAGE_PARAMETER;
	private final static String DEFAULT_LANGUAGE = "fi";

	@Inject
	private SessionService sessionService;

	/**
	 * Returns the current language of the user, looking first at language session setting
	 * and if not present, the DEFAULT_LANGUAGE (Finnish)
	 *
	 * @return The current language of the locale
	 */
	public String getLanguage() {
		HttpSession session = sessionService.getSession(false);
		if (session == null) return DEFAULT_LANGUAGE;
		Object sessionLanguage = session.getAttribute(SESSION_LANGUAGE);
		if (sessionLanguage != null) {
			return sessionLanguage.toString();
		}
		return DEFAULT_LANGUAGE; 
	}

	public void setLanguage(String language) {
		sessionService.getSession(true).setAttribute(SESSION_LANGUAGE, language);
	}

}
