package fi.luomus.lajiauth.service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.regex.Pattern;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.google.common.base.Optional;

import fi.luomus.lajiauth.model.AuthenticationSourceUser;
import fi.luomus.lajiauth.model.LocalUser;
import fi.luomus.lajiauth.repository.LocalUserRepository;
import fi.luomus.utils.exceptions.NotFoundException;

@Service
public class LocalUserService {

	private final LocalUserRepository localUserRepository;
	private final HasherService hasher;

	private static final Pattern lowercaseLetter = Pattern.compile("\\p{Ll}");
	private static final Pattern digitOrSpecial = Pattern.compile("(\\d|\\W)");
	private static final Pattern uppercaseLetter = Pattern.compile("\\p{Lu}");

	@Inject
	public LocalUserService(LocalUserRepository localUserRepository, HasherService hasher) {
		this.localUserRepository = localUserRepository;
		this.hasher = hasher;
	}

	/**
	 * Try to log in an user with the given credentials
	 *
	 * @param email    The e-mail of the user
	 * @param password The password of the user
	 * @throws NotFoundException           An user with the given e-mail address does not exist in the repository
	 * @throws InvalidCredentialsException The user does not have a password or the password does not match
	 */
	public void login(String email, String password) throws NotFoundException, InvalidCredentialsException {
		if (email == null) throw new InvalidCredentialsException();
		LocalUser localUser = localUserRepository.find(email);
		if (localUser == null) {
			throw new NotFoundException();
		}
		if (localUser.getConfirmed() == false) {
			throw new InvalidCredentialsException();
		}
		if (!hasher.getHasher().check(hashKey(password, localUser.getNonce()), localUser.getPassword())) {
			throw new InvalidCredentialsException();
		}
	}

	/**
	 * Create a new user who's email has not yet been confirmed
	 * @param email
	 * @param name
	 * @param password
	 */
	public void createNewUnconfirmedUser(String email, String preferredName, String inheritedName, String password) {
		LocalUser localUser = new LocalUser();
		localUser.setConfirmed(false);
		localUser.setEmail(email);
		localUser.setPreferredName(preferredName);
		localUser.setInheritedName(inheritedName);
		localUser.setFullName(preferredName + " " + inheritedName);
		String nonce = UUID.randomUUID().toString();
		String hashedPassword = hasher.getHasher().hash(hashKey(password, nonce));
		localUser.setNonce(nonce);
		localUser.setPassword(hashedPassword);

		localUserRepository.insert(localUser);
	}

	/**
	 * Does the password meet security requirements?
	 *
	 * @param password The password
	 * @return True if the password meets the requirements, false if not
	 */
	public boolean isPasswordStrongEnough(String password) {
		return password.length() >= 10 &&
				lowercaseLetter.matcher(password).find() &&
				uppercaseLetter.matcher(password).find() &&
				digitOrSpecial.matcher(password).find();
	}

	/**
	 * Find an user with the given e-mail address
	 *
	 * @param email The user's e-mail address
	 * @return An optional containing the user details if the user exists, absent otherwise
	 */
	public Optional<AuthenticationSourceUser> find(String email) {
		if (email == null) return Optional.absent();
		return convertUser(localUserRepository.find(email));
	}

	/**
	 * Get all users in the repository
	 *
	 * @return All users
	 */
	public List<AuthenticationSourceUser> getAll() {
		List<AuthenticationSourceUser> result = new ArrayList<>();
		for (LocalUser luser : localUserRepository.getAll()) {
			result.add(convertUser(luser).get());
		}

		return result;
	}

	private Optional<AuthenticationSourceUser> convertUser(LocalUser localUser) {
		if (localUser != null) {
			AuthenticationSourceUser user = new AuthenticationSourceUser();
			user.setEmail(localUser.getEmail());
			user.setId(localUser.getEmail());
			user.setFullName(localUser.getFullName());
			return Optional.of(user);
		}
		return Optional.absent();
	}

	/**
	 * Sets the user's password
	 *
	 * @param email    The e-mail address of the user
	 * @param password The new password
	 * @throws NotFoundException     There is no user with the given e-mail address
	 * @throws IllegalStateException Thrown if the password is not strong enough
	 */
	public void setPassword(String email, String password) throws NotFoundException {
		String nonce = UUID.randomUUID().toString();
		String hashedPassword = hasher.getHasher().hash(hashKey(password, nonce));
		if (localUserRepository.updatePassword(email, hashedPassword, nonce) == 0) {
			throw new NotFoundException();
		}
	}

	private String hashKey(String password, String nonce) {
		return String.format("%s;%s", password, nonce);
	}

	public static class InvalidCredentialsException extends Exception {
		private static final long serialVersionUID = 1L;
	}

	public static class UserAlreadyExistsException extends IllegalStateException {
		private static final long serialVersionUID = 7746815542674147702L;
		public UserAlreadyExistsException(String s) {
			super(s);
		}
	}

	/**
	 * Mark this user's email confirmed
	 * @param email
	 */
	public void confirm(String email) {
		localUserRepository.confirm(email);
	}

	/**
	 * Create a password reset hash for the given local user email
	 * @param email The email of the local
	 * @return The hash with the nonce
	 */
	public HashWithNonce hashLocalUser(String email) {
		String nonce = UUID.randomUUID().toString();
		String hash = hasher.getHasher().hash(String.format("%s;%s", email.trim().toLowerCase(), nonce));
		return new HashWithNonce(hash, nonce);
	}

	/**
	 * Validate a password reset set hash for the given local user email
	 * @param email The email of the local user
	 * @param hashWithNonce The hash with the nonce
	 * @return True if the hash and the nonce are valid
	 */
	public boolean isValidLocalUser(String email, HashWithNonce hashWithNonce) {
		String expected = String.format("%s;%s", email.trim().toLowerCase(), hashWithNonce.nonce);
		return hasher.getHasher().check(expected, hashWithNonce.hash);
	}

	public static class HashWithNonce {
		public final String hash;
		public final String nonce;

		public HashWithNonce(String hash, String nonce) {
			if (hash == null || hash.length() == 0 || nonce == null || nonce.length() == 0) {
				throw new IllegalStateException("both hash and nonce must be present");
			}
			this.hash = hash;
			this.nonce = nonce;
		}
	}

	public void setEmail(String currentEmail, String newEmail) {
		localUserRepository.updateEmail(currentEmail, newEmail);
	}

}
