package fi.luomus.lajiauth.service;

import java.util.Map;

import javax.inject.Inject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;

import org.springframework.stereotype.Service;

import fi.luomus.lajiauth.util.LajiAuthPreferences;
import fi.luomus.utils.exceptions.ApiException;
import fi.luomus.utils.preferences.LuomusPreferences;

@Service
public class AccessTokenService {
	
    private final LuomusPreferences luomusPreferences;
    private final Client client;
    private String ourApiToken;
    private String apiUrl;
    
    @Inject
    public AccessTokenService(LuomusPreferences luomusPreferences) {
        this.luomusPreferences = luomusPreferences;
        this.client = ClientBuilder.newClient();
    }

    public String getSystemId(String accessToken) throws ApiException {
    	Response response = client.target(apiUrl())
				.queryParam("accessToken", accessToken)
				.queryParam("access_token", ourApiToken())
				.request().get();
    	if (response.getStatusInfo().getStatusCode() == 404) return null;
		raiseIfNotSuccessful(response);

		return response.readEntity(new GenericType<Map<String, String>>(){}).get("systemID");
	}
    
    private void raiseIfNotSuccessful(Response response) throws ApiException {
		if (!response.getStatusInfo().getFamily().equals(Response.Status.Family.SUCCESSFUL)) {
			throw ApiException.builder().code(String.valueOf(response.getStatus())).source("LAJI-API").details(getDetails(response)).build();
		}
	}
    
    private String getDetails(Response response) {
		try {
			return response.readEntity(String.class);
		} catch (Exception e) {
			return "Failed to read error details: " + e.getMessage();
		}
	}
    
	private String ourApiToken() {
		if (ourApiToken == null) {
			ourApiToken = luomusPreferences.get(LajiAuthPreferences.LAJI_API_ACCESS_TOKEN).get();
		}
		return ourApiToken;
	}
	
	private String apiUrl() {
		if (apiUrl == null) {
			apiUrl = luomusPreferences.get(LajiAuthPreferences.LAJI_API_URL).get() + "/api-users";
		}
		return apiUrl;
	}
	
	
}
