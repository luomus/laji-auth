package fi.luomus.lajiauth.service;

import java.util.ResourceBundle;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

@Service
public class LocalizationService {

	@Inject
	private LanguageService languageService;

	public String getMessage(String bundleName, String key, String... arguments) {
		ResourceBundle bundle = ResourceBundle.getBundle("localization." + bundleName);
		String message = tryMessage(bundle, key);
		if (arguments.length > 0) {
			return html(String.format(message, (Object[]) arguments));
		}
		return html(message);
	}

	private String html(String message) {
		return com.google.common.html.HtmlEscapers.htmlEscaper().escape(message);
	}

	private String tryMessage(ResourceBundle bundle, String key) {
		return tryMessage(bundle, key, languageService.getLanguage());
	}

	private String tryMessage(ResourceBundle bundle, String key, String language) {
		return bundle.getString(key + "_" + language);
	}

}
