package fi.luomus.lajiauth.service;

import java.net.URI;
import java.net.URLEncoder;
import java.util.Map;

import javax.inject.Inject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import org.springframework.stereotype.Service;

import fi.luomus.lajiauth.model.AuthenticationSource;
import fi.luomus.lajiauth.model.AuthenticationSourceUser;
import fi.luomus.lajiauth.util.LajiAuthPreferences;
import fi.luomus.utils.exceptions.ApiException;
import fi.luomus.utils.preferences.LuomusPreferences;

@Service
public class FacebookService {

	private final LuomusPreferences preferences;
	private final Client client;

	@Inject
	public FacebookService(LuomusPreferences luomusPreferences) {
		this.preferences = luomusPreferences;
		this.client = ClientBuilder.newClient();
	}

	/**
	 * Get an uri the users should be redirected to for Facebook authentication
	 *
	 * @param redirectUri The uri the user will be redirected to after authentication
	 * @param state State containing our forwarding params
	 * @return The redirect uri
	 */
	public URI getRedirectUri(URI redirectUri, String state) {
		try {
			return UriBuilder.fromPath("https://www.facebook.com/dialog/oauth")
					.queryParam("client_id", preferences.get(LajiAuthPreferences.FACEBOOK_APP_ID).get())
					.queryParam("redirect_uri", redirectUri)
					.queryParam("scope", "public_profile,email")
					.queryParam("state", URLEncoder.encode(state, "UTF-8"))
					.build();
		} catch (Exception e) {
			throw new RuntimeException("failed to encode state", e);
		}
	}

	/**
	 * Redeem the given code for an API access token
	 *
	 * @param redirectUri The original redirect uri parameter for verification
	 * @param code The received access code
	 * @return access token
	 * @throws ApiException
	 */
	private String getAccessToken(URI redirectUri, String code) throws ApiException {
		Response response = client.target("https://graph.facebook.com/v2.3/oauth/access_token")
				.queryParam("client_id", preferences.get(LajiAuthPreferences.FACEBOOK_APP_ID).get())
				.queryParam("redirect_uri", redirectUri)
				.queryParam("client_secret", preferences.get(LajiAuthPreferences.FACEBOOK_SECRET).get())
				.queryParam("code", code)
				.request().get();
		raiseIfNotSuccessful(response, "graph.facebook.com/v2.3/oauth/access_token");

		return response.readEntity(new GenericType<Map<String, String>>(){}).get("access_token");
	}

	/**
	 * Get the user's details from auth response
	 *
	 * @param redirectUri The original redirect uri parameter for verification
	 * @param code The received access code
	 * @return User's details
	 * @throws ApiException
	 */
	public AuthenticationSourceUser getUserDetails(URI redirectUri, String code) throws ApiException {
		String accessToken = getAccessToken(redirectUri, code);
		Response graphResponse = ClientBuilder.newClient().target("https://graph.facebook.com/me")
				.queryParam("access_token", accessToken).queryParam("fields", "name,email").request().get();
		raiseIfNotSuccessful(graphResponse, "graph.facebook.com/me");

		FacebookUser fbUser = graphResponse.readEntity(FacebookUser.class);
		return toAuthSourceUser(fbUser);
	}

	private AuthenticationSourceUser toAuthSourceUser(FacebookUser fbUser) {
		AuthenticationSourceUser user = new AuthenticationSourceUser();
		user.setId(fbUser.getId());
		user.setEmail(fbUser.getEmail());
		user.setFullName(fbUser.getName());
		return user;
	}

	public static class FacebookUser {
		private String id;
		private String name;
		private String email;
		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getEmail() {
			return email;
		}
		public void setEmail(String email) {
			this.email = email;
		}
	}

	private void raiseIfNotSuccessful(Response response, String request) throws ApiException {
		if (!response.getStatusInfo().getFamily().equals(Response.Status.Family.SUCCESSFUL)) {
			throw ApiException.builder().code(String.valueOf(response.getStatus())).source(AuthenticationSource.FACEBOOK.name()).details(getDetails(response, request)).build();
		}
	}

	private String getDetails(Response response, String request) {
		try {
			return request + ": " + response.readEntity(String.class);
		} catch (Exception e) {
			return "Failed to read error details: " + e.getMessage();
		}
	}

}
