package fi.luomus.lajiauth.service;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import fi.luomus.lajiauth.util.Hasher;
import fi.luomus.lajiauth.util.LajiAuthPreferences;
import fi.luomus.utils.preferences.LuomusPreferences;

@Service
public class HasherService {
	
    private final LuomusPreferences luomusPreferences;

    @Inject
    public HasherService(LuomusPreferences luomusPreferences) {
        this.luomusPreferences = luomusPreferences;
    }

    public Hasher getHasher() {
        return new Hasher(luomusPreferences.get(LajiAuthPreferences.SECRET_KEY).get());
    }
    
}
