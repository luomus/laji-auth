package fi.luomus.lajiauth.service;

import java.net.URI;
import java.util.ResourceBundle;

import javax.inject.Inject;
import javax.mail.MessagingException;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import fi.luomus.lajiauth.model.AuthenticationSourceUser;
import fi.luomus.lajiauth.service.UserService.UserAttributes;

@Service
public class NotificationService {

	private final Logger logger = LoggerFactory.getLogger(NotificationService.class);
	private final static String FROM_ADDRESS = "noreply@laji.fi";

	private final EmailService emailService;

	@Inject
	public NotificationService(EmailService emailService) {
		this.emailService = emailService;
	}

	public void notifyEmailConfirmationRequest(String email, String name, String locale, URI confirmUri) {
		String subject = getMessage(locale, "confirm_subject");
		String body = getMessage(locale, "confirm_body", name, confirmUri.toString());
		send(subject, body, email, "confirmation request", true);
	}

	public void notifyEmailChangeRequested(String currentEmail, String name, String locale) {
		String subject = getMessage(locale, "email_change_notice_subject");
		String body = getMessage(locale, "email_change_notice_body", name);
		send(subject, body, currentEmail, "email change notification", false);
	}

	public void notifyLocalUserForgotPassword(AuthenticationSourceUser user, String locale, URI passwordResetUri) {
		String subject = getMessage(locale, "password_reset_subject");
		String body = getMessage(locale, "password_reset_body", user.getFullName(), passwordResetUri.toString());
		send(subject, body, user.getEmail(), "forgot password link", true);
	}

	public void notifySecurePortalAccessGranted(UserAttributes targetUser) {
		String locale = getLocale(targetUser);
		String subject = getMessage(locale, "secure_portal_access_granted_subject");
		String body = getMessage(locale, "secure_portal_access_granted_body", targetUser.getName());
		send(subject, body, targetUser.getEmail(), "secure portal access granted notification", false);
	}

	public void notifySecurePortalAccessRenewed(UserAttributes targetUser, DateTime expireDate) {
		String locale = getLocale(targetUser);
		String subject = getMessage(locale, "secure_portal_access_renewed_subject");
		String body = getMessage(locale, "secure_portal_access_renewed_body", targetUser.getName(), toHumanDate(locale, expireDate));
		send(subject, body, targetUser.getEmail(), "secure portal access renewed notification", false);
	}

	public void notifyAccountDeleted(UserAttributes userInfo) {
		String locale = getLocale(userInfo);
		String subject = getMessage(locale, "account_deleted_subject");
		String body = getMessage(locale, "account_deleted_body", userInfo.getName());
		send(subject, body, userInfo.getEmail(), "account delete notification", false);
	}

	private void send(String subject, String body, String email, String emailTypeInCaseOfError, boolean failIfError) {
		try {
			emailService.email()
			.from(FROM_ADDRESS)
			.to(email)
			.subject(subject)
			.body(body)
			.send();
		} catch (MessagingException e) {
			logger.error(String.format("error sending " + emailTypeInCaseOfError + " to " + email), e);
			if (failIfError) throw new RuntimeException(String.format("error sending " + emailTypeInCaseOfError + " to " + email), e);
		}
	}

	private String getMessage(String language, String key, String... args) {
		ResourceBundle bundle = ResourceBundle.getBundle("localization.notifications");
		String message = bundle.getString(String.format("%s_%s", key, language));
		if (args.length > 0) {
			return String.format(message, (Object[]) args);
		}
		return message.trim();
	}

	private String getLocale(UserAttributes targetUser) {
		String locale = targetUser.getDefaultLanguage();
		if (locale == null) locale = "fi";
		return locale;
	}

	private String toHumanDate(String locale, DateTime expireDate) {
		if ("en".equals(locale)) return expireDate.toString("d MMM yyyy");
		return expireDate.toString("d.M.yyyy");
	}

}
