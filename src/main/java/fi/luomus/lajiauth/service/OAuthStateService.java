package fi.luomus.lajiauth.service;

import static fi.luomus.lajiauth.model.Constants.NEXT_PARAMETER;
import static fi.luomus.lajiauth.model.Constants.REDIRECT_METHOD;
import static fi.luomus.lajiauth.model.Constants.TARGET_SYSTEM_PARAMETER;
import static fi.luomus.lajiauth.model.ServerConstants.PERMANENT;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import fi.luomus.lajiauth.model.OAuthState;

@Service
public class OAuthStateService {

	private final ObjectMapper objectMapper;

	@Inject
	public OAuthStateService(ObjectMapper objectMapper) {
		this.objectMapper = objectMapper;
	}

	/**
	 * Parse a state value back to a map
	 *
	 * @param stateValue The encoded state
	 * @return The parsed state
	 */
	public OAuthState parseState(String stateValue) {
		try {
			Map<String, String> map = objectMapper.readValue(stateValue, new TypeReference<Map<String, String>>() {});
			String target = map.get(TARGET_SYSTEM_PARAMETER);
			String next = map.get(NEXT_PARAMETER);
			String redirectMethod = map.get(REDIRECT_METHOD);
			boolean permanent = Boolean.valueOf(map.get(PERMANENT));
			return new OAuthState(target, next, redirectMethod, permanent);
		} catch (IOException e) {
			throw new RuntimeException("cannot deserialize state", e);
		}
	}

	public String serializeState(String target, String next, String redirectMethod, boolean permanent) {
		Map<String, String> state = new HashMap<>();
		state.put(TARGET_SYSTEM_PARAMETER, target);
		state.put(NEXT_PARAMETER, next);
		state.put(REDIRECT_METHOD, redirectMethod);
		state.put(PERMANENT, Boolean.toString(permanent));
		try {
			String serialized = objectMapper.writeValueAsString(state);
			return serialized;
		} catch (JsonProcessingException e) {
			throw new RuntimeException("cannot serialize state", e);
		}
	}

}
