package fi.luomus.lajiauth.service.additional.source;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import fi.luomus.lajiauth.model.ServerConstants;
import fi.luomus.lajiauth.repository.LintuvaaraRepository;
import fi.luomus.lajiauth.repository.LintuvaaraRepository.LintuvaaraUserData;
import fi.luomus.lajiauth.service.additional.AdditionalUserIdSource;
import fi.luomus.lajiauth.service.additional.ResolveResult;
import fi.luomus.lajiauth.service.additional.ResolvedUser;
import fi.luomus.lajiauth.service.additional.UnresolvedUser;

@Service
public class LintuvaaraSource implements AdditionalUserIdSource {

	private final LintuvaaraRepository repository;

	@Inject
	public LintuvaaraSource(LintuvaaraRepository repository) {
		this.repository = repository;
	}

	@Override
	public ResolveResult resolve(UnresolvedUser unresolvedUser) {
		LintuvaaraUserData data = repository.getMatchingUser(unresolvedUser);
		if (data == null) {
			return ResolveResult.failure();
		}
		ResolvedUser resolvedUser = new ResolvedUser(ServerConstants.ADDITIONAL_SYSTEM_LINTUVAARA, data.getId().toString());
		ResolveResult resolveResult = ResolveResult.success(resolvedUser);
		if (data.getHatikkaId() != null) {
			resolveResult.addAdditionalResolved(new ResolvedUser(ServerConstants.ADDITIONAL_SYSTEM_HATIKKA, data.getHatikkaId()));
		}
		if (data.getEmail() != null) {
			resolveResult.addEmail(data.getEmail());
		}
		return resolveResult;
	}

	@Override
	public ResolveResult resolve(ResolvedUser resolvedUser) {
		validateSystem(resolvedUser);
		Integer id = getId(resolvedUser);
		if (id == null) return ResolveResult.empty();
		
		LintuvaaraUserData data = repository.getByUsername(id);
		if (data == null) {
			return ResolveResult.empty();
		}
		
		ResolveResult resolveResult = ResolveResult.empty();
		if (data.getHatikkaId() != null) {
			resolveResult.addAdditionalResolved(new ResolvedUser(ServerConstants.ADDITIONAL_SYSTEM_HATIKKA, data.getHatikkaId()));
		}
		if (data.getEmail() != null) {
			resolveResult.addEmail(data.getEmail());
		}
		return resolveResult;
	}

	private Integer getId(ResolvedUser resolvedUser) {
		try {
			return Integer.valueOf(resolvedUser.getUsername());
		} catch (Exception e) {
			return null;
		}
	}

	private void validateSystem(ResolvedUser resolvedUser) {
		if (!resolvedUser.getSystem().equals(ServerConstants.ADDITIONAL_SYSTEM_LINTUVAARA)) {
			throw new IllegalArgumentException("Expected system " + ServerConstants.ADDITIONAL_SYSTEM_LINTUVAARA);
		}
	}

	@Override
	public Collection<ResolvedUser> resolve(String email) {
		List<LintuvaaraUserData> datas = repository.getByEmail(email);
		List<ResolvedUser> resolved = new ArrayList<>();
		for (LintuvaaraUserData data : datas) {
			resolved.add(new ResolvedUser(ServerConstants.ADDITIONAL_SYSTEM_LINTUVAARA, data.getId().toString()));
		}
		return resolved;
	}

}
