package fi.luomus.lajiauth.service.additional;

public class ResolvedUser {
	
	private final String system;
	private final String username;
	
	public ResolvedUser(String system, String username) {
		this.system = system;
		this.username = username;
	}
	
	public String getSystem() {
		return system;
	}
	
	public String getUsername() {
		return username;
	}

	@Override
	public String toString() {
		return "ResolvedUser [system=" + system + ", username=" + username + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((system == null) ? 0 : system.hashCode());
		result = prime * result + ((username == null) ? 0 : username.toLowerCase().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ResolvedUser other = (ResolvedUser) obj;
		if (system == null) {
			if (other.system != null)
				return false;
		} else if (!system.equals(other.system))
			return false;
		if (username == null) {
			if (other.username != null)
				return false;
		} else if (!username.equalsIgnoreCase(other.username))
			return false;
		return true;
	}
	
}