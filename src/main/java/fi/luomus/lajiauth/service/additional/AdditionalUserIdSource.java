package fi.luomus.lajiauth.service.additional;

import java.util.Collection;

public interface AdditionalUserIdSource {

	/**
	 * Resolve given credentials and return matching user from the system and any additional resolved users of the matching user
	 * @param unresolvedUser
	 * @return
	 */
	public ResolveResult resolve(UnresolvedUser unresolvedUser);

	/**
	 * Return additional users from this source of the resolved user (of this source)
	 * @param resolvedUser
	 * @return
	 */
	public ResolveResult resolve(ResolvedUser resolvedUser);

	/**
	 * Return users from this source with the given email
	 * @param email
	 * @return
	 */
	public Collection<ResolvedUser> resolve(String email);

}