package fi.luomus.lajiauth.service.additional;

public class UnresolvedUser extends ResolvedUser {

	private final String password;
	private ResolvedUser resolvedUser;

	public UnresolvedUser(String system, String username, String password) {
		super(system, username);
		this.password = password;
	}

	public String getPassword() {
		return password;
	}

	public boolean isUnresolved() {
		return resolvedUser == null;
	}

	public ResolvedUser getResolved() {
		return resolvedUser;
	}

	public UnresolvedUser setResolved(ResolvedUser resolvedUser) {
		this.resolvedUser = resolvedUser;
		return this;
	}

	@Override
	public String toString() {
		return "UnresolvedUser [system=" + getSystem() + ", username=" + getUsername() + ", password=" + password + ", unresolved=" + isUnresolved() + "]";
	}

	
}