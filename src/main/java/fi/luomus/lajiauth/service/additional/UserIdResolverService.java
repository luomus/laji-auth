package fi.luomus.lajiauth.service.additional;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import fi.luomus.lajiauth.model.AuthenticationEvent;
import fi.luomus.lajiauth.model.ServerConstants;
import fi.luomus.lajiauth.service.additional.source.HatikkaSource;
import fi.luomus.lajiauth.service.additional.source.LintuvaaraSource;
import fi.luomus.lajiauth.service.additional.source.OldHatikkaSource;

@Service
public class UserIdResolverService {

	private final Map<String, AdditionalUserIdSource> sources;

	@Inject
	public UserIdResolverService(HatikkaSource hatikka, OldHatikkaSource oldHatikka, LintuvaaraSource lintuvaara) {
		sources = new HashMap<>();
		sources.put(ServerConstants.ADDITIONAL_SYSTEM_HATIKKA, hatikka);
		sources.put(ServerConstants.ADDITIONAL_SYSTEM_OLD_HATIKKA, oldHatikka);
		sources.put(ServerConstants.ADDITIONAL_SYSTEM_LINTUVAARA, lintuvaara);
	}

	public UserIdResolver getResolver(AuthenticationEvent event) {
		return new UserIdResolver(event, this);
	}

	public AdditionalUserIdSource getSource(String system) {
		return sources.get(system);
	}

	public Collection<AdditionalUserIdSource> getSources() {
		return Collections.unmodifiableCollection(sources.values());
	}

}
