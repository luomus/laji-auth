package fi.luomus.lajiauth.service.additional;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ResolveResult {

	private final ResolvedUser resolvedUser;
	private final List<ResolvedUser> additionalResolved = new ArrayList<>();
	private final Set<String> emails = new HashSet<>();

	private ResolveResult(ResolvedUser resolvedUser) {
		this.resolvedUser = resolvedUser;
	}

	private ResolveResult() {
		this.resolvedUser = null;
	}

	public boolean isSuccees() {
		return resolvedUser != null;
	}

	public ResolvedUser getResolvedUser() {
		return resolvedUser;
	}
	public List<ResolvedUser> getAdditionalResolved() {
		return additionalResolved;
	}

	public static ResolveResult success(ResolvedUser resolvedUser) {
		return new ResolveResult(resolvedUser);
	}

	public static ResolveResult failure() {
		return new ResolveResult();
	}

	public ResolveResult addAdditionalResolved(ResolvedUser additional) {
		this.additionalResolved.add(additional);
		return this;
	}

	public static ResolveResult empty() {
		return new ResolveResult();
	}

	public ResolveResult addEmail(String email) {
		if (email != null) {
			emails.add(email.toLowerCase().trim());
		}
		return this;
	}

	public Set<String> getEmails() {
		return emails;
	}

}