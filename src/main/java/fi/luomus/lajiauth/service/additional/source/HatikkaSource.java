package fi.luomus.lajiauth.service.additional.source;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import fi.luomus.lajiauth.model.ServerConstants;
import fi.luomus.lajiauth.repository.HatikkaRepository;
import fi.luomus.lajiauth.repository.HatikkaRepository.HatikkaUserData;
import fi.luomus.lajiauth.service.additional.AdditionalUserIdSource;
import fi.luomus.lajiauth.service.additional.ResolveResult;
import fi.luomus.lajiauth.service.additional.ResolvedUser;
import fi.luomus.lajiauth.service.additional.UnresolvedUser;

@Service
public class HatikkaSource implements AdditionalUserIdSource {

	private final HatikkaRepository repository;
	
	@Inject
	public HatikkaSource(HatikkaRepository repository) {
		this.repository = repository;
	}
	
	@Override
	public ResolveResult resolve(UnresolvedUser unresolvedUser) {
		HatikkaUserData data = repository.getMatchingUser(unresolvedUser);
		if (data == null) {
			return ResolveResult.failure();
		}
		ResolvedUser resolvedUser = new ResolvedUser(ServerConstants.ADDITIONAL_SYSTEM_HATIKKA, data.getId());
		ResolveResult resolveResult = ResolveResult.success(resolvedUser);
		if (data.getOldHatikkaId() != null) {
			resolveResult.addAdditionalResolved(new ResolvedUser(ServerConstants.ADDITIONAL_SYSTEM_OLD_HATIKKA, data.getOldHatikkaId()));
		}
		if (data.getEmail() != null) {
			resolveResult.addEmail(data.getEmail());
		}
		return resolveResult;
	}

	@Override
	public ResolveResult resolve(ResolvedUser resolvedUser) {
		validateSystem(resolvedUser);
		HatikkaUserData data = repository.getByUsername(resolvedUser.getUsername());
		if (data == null) {
			return ResolveResult.empty();
		}
		ResolveResult resolveResult = ResolveResult.empty();
		if (data.getOldHatikkaId() != null) {
			resolveResult.addAdditionalResolved(new ResolvedUser(ServerConstants.ADDITIONAL_SYSTEM_OLD_HATIKKA, data.getOldHatikkaId()));
		}
		if (data.getEmail() != null) {
			resolveResult.addEmail(data.getEmail());
		}
		return resolveResult;
	}

	private void validateSystem(ResolvedUser resolvedUser) {
		if (!resolvedUser.getSystem().equals(ServerConstants.ADDITIONAL_SYSTEM_HATIKKA)) {
			throw new IllegalArgumentException("Expected system " + ServerConstants.ADDITIONAL_SYSTEM_HATIKKA);
		}
	}

	@Override
	public Collection<ResolvedUser> resolve(String email) {
		List<HatikkaUserData> datas = repository.getByEmail(email);
		List<ResolvedUser> resolved = new ArrayList<>();
		for (HatikkaUserData data : datas) {
			resolved.add(new ResolvedUser(ServerConstants.ADDITIONAL_SYSTEM_HATIKKA, data.getId()));
		}
		return resolved;
	}

}
