package fi.luomus.lajiauth.service.additional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import fi.luomus.lajiauth.model.AuthenticationEvent;

public class UserIdResolver {

	private final List<ResolvedUser> resolved = new ArrayList<>();
	private final List<ResolvedUser> newResolved = new ArrayList<>();
	private final UserIdResolverService userIdservice;

	public UserIdResolver(AuthenticationEvent authInfo, UserIdResolverService resolverService) {
		this.userIdservice = resolverService;
		for (Map.Entry<String, List<String>> e : authInfo.getUser().getAdditionalUserIds().entrySet()) {
			String system = e.getKey();
			for (String username : e.getValue()) {
				resolved.add(new ResolvedUser(system, username));
			}
		}
	}

	public Collection<ResolvedUser> getNewResolved() {
		return newResolved;
	}

	public UserIdResolver resolve(UnresolvedUser unresolvedUser) {
		AdditionalUserIdSource source = userIdservice.getSource(unresolvedUser.getSystem());
		if (source == null) {
			return this;
		}

		ResolveResult result = source.resolve(unresolvedUser);
		if (!result.isSuccees()) return this;

		unresolvedUser.setResolved(result.getResolvedUser());

		if (!resolved.contains(result.getResolvedUser())) {
			newResolved.add(result.getResolvedUser());
			resolved.add(result.getResolvedUser());
		}

		addAdditional(result);

		return this;
	}

	private void addAdditional(ResolveResult result) {
		for (ResolvedUser additional : result.getAdditionalResolved()) {
			addAdditional(additional);
		}
		for (String email : result.getEmails()) {
			for (ResolvedUser additional : resolve(email)) {
				addAdditional(additional);
			}
		}
	}

	private void addAdditional(ResolvedUser additional) {
		if (!resolved.contains(additional)) {
			newResolved.add(additional);
			resolved.add(additional);
			resolve(additional);
		}
	}

	private List<ResolvedUser> resolve(String email) {
		List<ResolvedUser> resolved = new ArrayList<>();
		for (AdditionalUserIdSource source : userIdservice.getSources()) {
			for (ResolvedUser u : source.resolve(email)) {
				if (!resolved.contains(u)) {
					resolved.add(u);
				}
			}
		}
		return resolved;
	}

	private void resolve(ResolvedUser resolvedUser) {
		AdditionalUserIdSource source = getSource(resolvedUser);
		ResolveResult result = source.resolve(resolvedUser);
		addAdditional(result);
	}

	private AdditionalUserIdSource getSource(ResolvedUser resolvedUser) {
		return userIdservice.getSource(resolvedUser.getSystem());
	}

}