package fi.luomus.lajiauth.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Provider;
import javax.sql.DataSource;

import org.apache.commons.codec.digest.DigestUtils;
import org.skife.jdbi.v2.DBI;
import org.skife.jdbi.v2.Handle;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;
import org.springframework.stereotype.Service;

import fi.luomus.lajiauth.service.additional.UnresolvedUser;

@Service
public class HatikkaRepository {

	public static class HatikkaUserData {
		private String id;
		private String email;
		private String oldHatikkaId;
		public String getId() {
			return id;
		}
		public String getEmail() {
			return email;
		}
		public String getOldHatikkaId() {
			return oldHatikkaId;
		}
	}
	private static class HatikkaUserDataMapper implements ResultSetMapper<HatikkaUserData> {
		@Override
		public HatikkaUserData map(int index, ResultSet rs, StatementContext ctx) throws SQLException {
			HatikkaUserData data = new HatikkaUserData();
			data.id = rs.getString("id");
			data.email = rs.getString("email");
			data.oldHatikkaId = rs.getString("vanha_hatikka_id");
			return data;
		}
	}

	private final Provider<DataSource> dataSourceProvider;

	@Inject
	public HatikkaRepository(Provider<DataSource> dataSourceProvider) {
		this.dataSourceProvider = dataSourceProvider;
	}

	public HatikkaUserData getByUsername(String id) {
		try (Handle h = dbi()) {
			return  h.createQuery(
					"select id, email, vanha_hatikka_id from hatikka_user where lower(id) = :id")
					.bind("id", id.toLowerCase())
					.map(new HatikkaUserDataMapper())
					.first();
		}
	}

	public List<HatikkaUserData> getByEmail(String email) {
		try (Handle h = dbi()) {
			return  h.createQuery(
					"select id, email, vanha_hatikka_id from hatikka_user where lower(email) = :email")
					.bind("email", email.toLowerCase())
					.map(new HatikkaUserDataMapper())
					.list();
		}
	}

	public HatikkaUserData getMatchingUser(UnresolvedUser unresolvedUser) {
		String username = unresolvedUser.getUsername().toLowerCase();
		if (!username.toLowerCase().contains("@hatikka.fi")) {
			username += "@hatikka.fi";
		}
		try (Handle h = dbi()) {
			return  h.createQuery(
					"select id, email, vanha_hatikka_id from hatikka_user where lower(id) = :id and password = :hash")
					.bind("id", username.toLowerCase())
					.bind("hash", passwordHash(unresolvedUser.getPassword()))
					.map(new HatikkaUserDataMapper())
					.first();
		}
	}

	private String passwordHash(String password) {
		return DigestUtils.md5Hex(password);
	}

	private Handle dbi() {
		return DBI.open(dataSourceProvider.get());
	}

}
