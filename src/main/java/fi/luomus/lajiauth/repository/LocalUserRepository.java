package fi.luomus.lajiauth.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Provider;
import javax.sql.DataSource;

import org.skife.jdbi.v2.DBI;
import org.skife.jdbi.v2.Handle;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;
import org.springframework.stereotype.Repository;

import fi.luomus.lajiauth.model.LocalUser;

@Repository
public class LocalUserRepository {

	private final Provider<DataSource> dataSourceProvider;

	@Inject
	public LocalUserRepository(Provider<DataSource> dataSourceProvider) {
		this.dataSourceProvider = dataSourceProvider;
	}

	public void insert(LocalUser user) {
		try (Handle h = dbi()) {
			h.execute(
					"INSERT INTO local_users (email, name, password, nonce, confirmed) VALUES (?, ?, ?, ?, ?)", 
					user.getEmail().trim().toLowerCase(), user.getFullName(), user.getPassword(), user.getNonce(), user.getConfirmed());
		}
	}

	public LocalUser find(String email) {
		if (email == null) throw new IllegalArgumentException("Null email given");
		try (Handle h = dbi()) {
			return h.createQuery("SELECT email, name, confirmed, password, nonce FROM local_users WHERE email = :email")
					.bind("email", email.trim().toLowerCase())
					.map(new ResultSetMapper<LocalUser>() {
						@Override
						public LocalUser map(int i, ResultSet rs, StatementContext ctx) throws SQLException {
							LocalUser user = new LocalUser();
							user.setEmail(rs.getString(1));
							user.setFullName(rs.getString(2));
							user.setConfirmed(rs.getInt(3) == 1);
							user.setPassword(rs.getString(4));
							user.setNonce(rs.getString(5));
							return user;
						}
					}).first();
		}
	}

	public List<LocalUser> getAll() {
		try (Handle h = dbi()) {
			return h.createQuery("SELECT email, name, confirmed FROM local_users").map(new ResultSetMapper<LocalUser>() {
				@Override
				public LocalUser map(int i, ResultSet rs, StatementContext ctx) throws SQLException {
					LocalUser user = new LocalUser();
					user.setEmail(rs.getString(1));
					user.setFullName(rs.getString(2));
					user.setConfirmed(rs.getInt(3) == 1);
					return user;
				}
			}).list();
		}
	}

	public int delete(String email) {
		try (Handle h = dbi()) {
			return h.update("DELETE FROM local_users WHERE email = ?", email.trim().toLowerCase());
		}
	}

	public int updatePassword(String email, String password, String nonce) {
		try (Handle h = dbi()) {
			return h.update("UPDATE local_users SET password = ?, nonce = ? WHERE email = ?", password, nonce, email.trim().toLowerCase());
		}
	}

	public int updateEmail(String currentEmail, String newEmail) {
		try (Handle h = dbi()) {
			return h.update("UPDATE local_users SET email = ? WHERE email = ?", newEmail.trim().toLowerCase(), currentEmail.trim().toLowerCase());
		}
	}

	public int confirm(String email) {
		try (Handle h = dbi()) {
			return h.update("UPDATE local_users SET confirmed = 1 WHERE email = ?", email.trim().toLowerCase());
		}
	}

	private Handle dbi() {
		return DBI.open(dataSourceProvider.get());
	}

}
