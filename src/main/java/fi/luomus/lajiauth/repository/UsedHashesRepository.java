package fi.luomus.lajiauth.repository;

import java.sql.SQLIntegrityConstraintViolationException;

import javax.inject.Inject;
import javax.inject.Provider;
import javax.sql.DataSource;

import org.skife.jdbi.v2.DBI;
import org.skife.jdbi.v2.Handle;
import org.skife.jdbi.v2.exceptions.UnableToExecuteStatementException;
import org.skife.jdbi.v2.util.IntegerMapper;
import org.springframework.stereotype.Repository;

@Repository
public class UsedHashesRepository {
	
    private final Provider<DataSource> dataSourceProvider;

    @Inject
    public UsedHashesRepository(Provider<DataSource> dataSourceProvider) {
        this.dataSourceProvider = dataSourceProvider;
    }

    public void addHash(String hash) {
        try (Handle h = dbi()) {
            h.execute("INSERT INTO used_hashes (hash) VALUES (?)", hash);
        } catch (UnableToExecuteStatementException e) {
            // ignore duplicate key errors
            if (!(e.getCause() instanceof SQLIntegrityConstraintViolationException)) {
                throw e;
            }
        }
    }

    public boolean isHashUsed(String hash) {
        try (Handle h = dbi()) {
            Integer count = h.createQuery("SELECT COUNT(*) FROM used_hashes WHERE hash = :hash")
                    .bind("hash", hash)
                    .map(IntegerMapper.FIRST).first();
            return count > 0;
        }
    }

    private Handle dbi() {
        return DBI.open(dataSourceProvider.get());
    }
    
}
