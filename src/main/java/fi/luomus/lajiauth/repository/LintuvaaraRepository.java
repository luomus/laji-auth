package fi.luomus.lajiauth.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Provider;
import javax.sql.DataSource;

import org.apache.commons.codec.digest.DigestUtils;
import org.skife.jdbi.v2.DBI;
import org.skife.jdbi.v2.Handle;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;
import org.springframework.stereotype.Service;

import fi.luomus.lajiauth.service.additional.UnresolvedUser;

@Service
public class LintuvaaraRepository {

	public static class LintuvaaraUserData {
		private Integer id;
		private String email;
		private String hatikkaId;
		private String salt;
		public Integer getId() {
			return id;
		}
		public String getEmail() {
			return email;
		}
		public String getHatikkaId() {
			return hatikkaId;
		}
	}

	private static class LintuvaaraUserDataMapper implements ResultSetMapper<LintuvaaraUserData> {
		@Override
		public LintuvaaraUserData map(int index, ResultSet rs, StatementContext ctx) throws SQLException {
			LintuvaaraUserData data = new LintuvaaraUserData();
			data.id = rs.getInt("id");
			data.email = rs.getString("email");
			data.hatikkaId = rs.getString("hatikkausername");
			data.salt = rs.getString("passwordsalt");
			return data;
		}
	}

	private final Provider<DataSource> dataSourceProvider;

	@Inject
	public LintuvaaraRepository(Provider<DataSource> dataSourceProvider) {
		this.dataSourceProvider = dataSourceProvider;
	}

	public LintuvaaraUserData getByUsername(int id) {
		try (Handle h = dbi()) {
			return  h.createQuery(
					"select id, email, hatikkausername, passwordsalt from tipu_production.ringer where id = :id")
					.bind("id", id)
					.map(new LintuvaaraUserDataMapper())
					.first();
		}
	}

	public List<LintuvaaraUserData> getByEmail(String email) {
		try (Handle h = dbi()) {
			return  h.createQuery(
					"select id, email, hatikkausername, passwordsalt from tipu_production.ringer where lower(email) = :email")
					.bind("email", email.toLowerCase())
					.map(new LintuvaaraUserDataMapper())
					.list();
		}
	}

	public LintuvaaraUserData getMatchingUser(UnresolvedUser unresolvedUser) {
		String salt = getSalt(unresolvedUser);
		if (salt == null) return null;
		return getMatchingUser(unresolvedUser.getPassword(), salt);
	}

	private LintuvaaraUserData getMatchingUser(String password, String salt) {
		String hash = DigestUtils.sha256Hex(password+salt);  
		try (Handle h = dbi()) {
			return  h.createQuery(
					"select id, email, hatikkausername, passwordsalt from tipu_production.ringer where passwordhash = :hash")
					.bind("hash", hash)
					.map(new LintuvaaraUserDataMapper())
					.first();
		}
	}

	private String getSalt(UnresolvedUser unresolvedUser) {
		try {
			LintuvaaraUserData data = getByUsername(Integer.valueOf(unresolvedUser.getUsername()));
			if (data == null) return null;
			return data.salt;
		} catch (NumberFormatException e) {
			return null;
		}
	}

	private Handle dbi() {
		return DBI.open(dataSourceProvider.get());
	}

}
