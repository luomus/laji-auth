package fi.luomus.lajiauth.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Provider;
import javax.sql.DataSource;

import org.apache.commons.codec.digest.DigestUtils;
import org.skife.jdbi.v2.DBI;
import org.skife.jdbi.v2.Handle;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;
import org.springframework.stereotype.Service;

import fi.luomus.lajiauth.service.additional.UnresolvedUser;
import fi.luomus.lajiauth.util.LajiAuthPreferences;
import fi.luomus.utils.preferences.LuomusPreferences;

@Service
public class OldHatikkaRepository {

	public static class OldHatikkaUserData {
		private String id;
		private String email;
		private String newHatikkaId;
		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
		public String getEmail() {
			return email;
		}
		public void setEmail(String email) {
			this.email = email;
		}
		public String getNewHatikkaId() {
			return newHatikkaId;
		}
		public void setNewHatikkaId(String newHatikkaId) {
			this.newHatikkaId = newHatikkaId;
		}
	}

	private static class OldHatikkaUserDataUserDataMapper implements ResultSetMapper<OldHatikkaUserData> {
		@Override
		public OldHatikkaUserData map(int index, ResultSet rs, StatementContext ctx) throws SQLException {
			OldHatikkaUserData data = new OldHatikkaUserData();
			data.id = rs.getString("id");
			data.email = rs.getString("email");
			data.newHatikkaId = rs.getString("hatikka_id");
			return data;
		}
	}

	private final Provider<DataSource> dataSourceProvider;
	private final LuomusPreferences preferences;

	@Inject
	public OldHatikkaRepository(LuomusPreferences luomusPreferences, Provider<DataSource> dataSourceProvider) {
		this.dataSourceProvider = dataSourceProvider;
		this.preferences = luomusPreferences;
	}

	public OldHatikkaUserData getByUsername(String id) {
		try (Handle h = dbi()) {
			return  h.createQuery(
					"select id, email, hatikka_id from vanha_hatikka_user where lower(id) = :id")
					.bind("id", id.toLowerCase())
					.map(new OldHatikkaUserDataUserDataMapper())
					.first();
		}
	}

	public List<OldHatikkaUserData> getByEmail(String email) {
		try (Handle h = dbi()) {
			return  h.createQuery(
					"select id, email, hatikka_id from vanha_hatikka_user where lower(email) = :email")
					.bind("email", email.toLowerCase())
					.map(new OldHatikkaUserDataUserDataMapper())
					.list();
		}
	}

	public OldHatikkaUserData getMatchingUser(UnresolvedUser unresolvedUser) {
		try (Handle h = dbi()) {
			return  h.createQuery(
					"select id, email, hatikka_id from vanha_hatikka_user where lower(id) = :id and password = :hash")
					.bind("id", unresolvedUser.getUsername().toLowerCase())
					.bind("hash", passwordHash(unresolvedUser.getPassword()))
					.map(new OldHatikkaUserDataUserDataMapper())
					.first();
		}
	}

	private String passwordHash(String password) {
		return DigestUtils.sha256Hex(password+salt());
	}

	private String salt() {
		return preferences.get(LajiAuthPreferences.OLD_HATIKKA_SALT).get();
	}

	private Handle dbi() {
		return DBI.open(dataSourceProvider.get());
	}

}
