package fi.luomus.lajiauth.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Provider;
import javax.sql.DataSource;

import org.skife.jdbi.v2.DBI;
import org.skife.jdbi.v2.Handle;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;
import org.skife.jdbi.v2.util.StringMapper;
import org.springframework.stereotype.Repository;

import fi.luomus.lajiauth.model.AuthenticationEvent;

@Repository
public class ValidLoginsRepository {

	private final Provider<DataSource> dataSourceProvider;

	@Inject
	public ValidLoginsRepository(Provider<DataSource> dataSourceProvider) {
		this.dataSourceProvider = dataSourceProvider;
	}

	public void addValidLogin(String token, String serializedLoginInfo, String userQname, String targetQname, boolean emailChange, boolean permanent) {
		try (Handle h = dbi()) {
			h.execute("INSERT INTO valid_logins (token, json, user_qname, target, emailchange, permanent) VALUES (?, ?, ?, ?, ?, ?)",
					token, serializedLoginInfo, userQname, targetQname, emailChange ? 1 : null, permanent ? 1 : null);
		}
	}

	public void updateValidLogin(String token, String serializedLoginInfo) {
		try (Handle h = dbi()) {
			h.execute("UPDATE valid_logins SET json = ? WHERE token = ?",
					serializedLoginInfo, token);
		}
	}

	public void removeLogin(String token) {
		try (Handle h = dbi()) {
			h.execute("DELETE FROM valid_logins WHERE token = ?", token);
		}
	}

	public void removeLogins(String userQname, String targetQname) {
		try (Handle h = dbi()) {
			h.execute("DELETE FROM valid_logins WHERE user_qname = ? AND target = ? AND emailchange IS NULL and permanent IS NULL", userQname, targetQname);
		}
	}

	public void removeLogins(String userQname) {
		try (Handle h = dbi()) {
			h.execute("DELETE FROM valid_logins WHERE user_qname = ?", userQname);
		}
	}

	public String getSerializedLoginInfo(String token) {
		try (Handle h = dbi()) {
			String serialized = h.createQuery("SELECT json FROM valid_logins WHERE token = :token")
					.bind("token", token)
					.map(StringMapper.FIRST).first();
			if (serialized != null) {
				h.execute("UPDATE valid_logins SET last_used = SYSDATE WHERE token = ? ", token);
			}
			return serialized;
		}
	}

	public static class PermanentLogin {
		private final String token;
		private final String json;
		private final Date lastUsed;
		public PermanentLogin(String token, String json, Date lastUsed) {
			this.token = token;
			this.json = json;
			this.lastUsed = lastUsed;
		}
		public String getJson() {
			return json;
		}
		public Date getLastUsed() {
			return lastUsed;
		}
		public String getToken() {
			return token;
		}
	}

	public List<PermanentLogin> getPermanentLogins(String userQname) {
		try (Handle h = dbi()) {
			return h.createQuery("SELECT token, json, last_used FROM valid_logins WHERE user_qname = :qname AND permanent = 1")
					.bind("qname", userQname)
					.map(new ResultSetMapper<PermanentLogin>() {
						@Override
						public PermanentLogin map(int i, ResultSet rs, StatementContext cx) throws SQLException {
							return new PermanentLogin(rs.getString(1), rs.getString(2), rs.getDate(3));
						}
					}).list();
		}
	}

	public void removeExpiredLogins() {
		try (Handle h = dbi()) {
			h.execute("DELETE FROM valid_logins WHERE permanent IS NULL AND ((last_used IS NULL AND created < sysdate - 2) OR (last_used IS NOT NULL AND last_used < sysdate - 14))");
		}
	}

	public void log(AuthenticationEvent authenticationInfo, String token, String json) {
		try (Handle h = dbi()) {
			h.execute("INSERT INTO log (source, target, user_qname, source_user_id, token, json) VALUES (?, ?, ?, ?, ?, ?)",
					authenticationInfo.getSource().name(),
					authenticationInfo.getTarget(),
					getQnameOrNull(authenticationInfo),
					authenticationInfo.getUser().getAuthSourceId(),
					token,
					json
					);
		}
	}

	private String getQnameOrNull(AuthenticationEvent authenticationInfo) {
		String qname = authenticationInfo.getUser().getQname().isPresent() ? authenticationInfo.getUser().getQname().get() : null;
		return qname;
	}

	private Handle dbi() {
		return DBI.open(dataSourceProvider.get());
	}

}
