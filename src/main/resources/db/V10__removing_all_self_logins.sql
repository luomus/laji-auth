ALTER TABLE valid_logins ADD user_qname varchar2(200 char);
UPDATE valid_logins SET user_qname = 'not available';

ALTER TABLE valid_logins ADD target varchar2(200 char);
UPDATE valid_logins SET target = 'not available';
ALTER TABLE valid_logins MODIFY target NOT NULL;

create index ix_valid_login_user_target on valid_logins(user_qname, target);