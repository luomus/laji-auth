CREATE sequence log_seq;
CREATE TABLE log
(
  id               number(30)                         DEFAULT log_seq.nextval,
  source           varchar2(200 char)                 NOT NULL,
  target           varchar2(200 char)                 NOT NULL,
  user_qname       varchar2(200 char),
  source_user_id   varchar2(1000 char)                NOT NULL,
  validated        date                               DEFAULT sysdate NOT NULL,
  PRIMARY KEY (id)
);