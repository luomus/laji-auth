CREATE TABLE valid_logins
(
  token            varchar2(500 char)                 NOT NULL,
  json             varchar2(4000 char)                NOT NULL,
  created          date                               DEFAULT sysdate NOT NULL,
  PRIMARY KEY (token)
);
create index ix_created_date on valid_logins(created);