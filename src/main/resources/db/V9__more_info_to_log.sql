ALTER TABLE log RENAME COLUMN validated TO created;

ALTER TABLE log ADD token varchar2(500 char);
UPDATE log SET token = 'not available';
ALTER TABLE log MODIFY token NOT NULL;

ALTER TABLE log ADD json varchar2(4000 char);
UPDATE log SET json = 'not available';
ALTER TABLE log MODIFY json NOT NULL;