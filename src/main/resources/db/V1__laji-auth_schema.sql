CREATE TABLE unapproved_users
(
  authSource       varchar2(50)                       NOT NULL,
  authSourceUserId varchar2(100)                      NOT NULL,
  name             varchar2(100)                      NOT NULL,
  email            varchar2(100)                      NOT NULL,
  targetSystemId   varchar2(50)                       NOT NULL,
  confirmed        char CHECK (confirmed IN (0, 1))   NOT NULL,
  language         varchar2(10)                       NOT NULL,
  PRIMARY KEY (authSource, authSourceUserId)
);
