ALTER TABLE local_users ADD created date default sysdate;
UPDATE local_users SET created = sysdate;
ALTER TABLE local_users MODIFY created NOT NULL;
