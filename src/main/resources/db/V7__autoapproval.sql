DROP TABLE unapproved_users;
ALTER TABLE local_users ADD confirmed char CHECK (confirmed IN (0, 1));
UPDATE local_users SET confirmed = 1;
ALTER TABLE local_users MODIFY confirmed NOT NULL;
DELETE from local_users WHERE password is null or nonce is null;
ALTER TABLE local_users MODIFY password VARCHAR2(100) NOT NULL;
ALTER TABLE local_users MODIFY nonce NOT NULL;