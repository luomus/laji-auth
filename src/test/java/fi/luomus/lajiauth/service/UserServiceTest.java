package fi.luomus.lajiauth.service;

import static fi.luomus.lajiauth.model.AuthenticationSource.HAKA;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.IOException;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.junit.Test;

import com.google.common.collect.ImmutableSet;

import fi.luomus.lajiauth.service.additional.UserIdResolverService;
import fi.luomus.triplestore.client.TriplestoreClient;
import fi.luomus.utils.exceptions.ApiException;

public class UserServiceTest {

	private final LocalUserService localUserService = new LocalUserService(null, null) {

	};

	private UserIdResolverService userIdResolverService = new UserIdResolverService(null, null, null) {

	};

	@Test
	public void testGetHakaUser() throws Exception {
		UserService userService = new UserService(mockClient("haka_present"), localUserService, userIdResolverService);
		UserService.UserAttributes userAttributes = userService.findUser("teppo@yliopisto.fi", HAKA).get();
		assertEquals("MA.65", userAttributes.getId());
		assertEquals("teppo.testaaja@yliopisto.fi", userAttributes.getEmail());
		assertEquals("Teppo Testaaja", userAttributes.getName());
		assertEquals(ImmutableSet.builder().add("MA.admin", "MA.awesome").build(), userAttributes.getRoles());
	}

	@Test
	public void testGetPartialMatch() throws Exception {
		UserService userService = new UserService(mockClient("haka_present"), localUserService, userIdResolverService);
		// simulate a partial match
		assertFalse(userService.findUser("teppo", HAKA).isPresent());
	}

	@Test
	public void testGetNoMatches() throws Exception {
		UserService userService = new UserService(mockClient("no_match"), localUserService, userIdResolverService);
		assertFalse(userService.findUser("teppo@yliopisto.fi", HAKA).isPresent());
	}

	@Test
	public void testMultipleMatches() throws Exception {
		UserService userService = new UserService(mockClient("multi_match"), localUserService, userIdResolverService);
		try {
			userService.findUser("teppo@yliopisto.fi", HAKA);
			fail("exception not raised");
		} catch (IllegalStateException e) {
			// nop
		}
	}

	private TriplestoreClient mockClient(String model) throws ApiException, IOException {
		TriplestoreClient triplestoreClient = mock(TriplestoreClient.class);
		TriplestoreClient.SearchQuery mockQuery = mock(TriplestoreClient.SearchQuery.class);
		when(triplestoreClient.searchLiteral()).thenReturn(mockQuery);
		when(mockQuery.predicate(anyString())).thenReturn(mockQuery);
		when(mockQuery.object(anyString())).thenReturn(mockQuery);
		when(mockQuery.limit(anyInt())).thenReturn(mockQuery);
		when(mockQuery.offset(anyInt())).thenReturn(mockQuery);
		when(mockQuery.execute()).thenReturn(readModel(model));

		return triplestoreClient;
	}

	private Model readModel(String model) throws IOException {
		String path = String.format("/%s/%s.xml", this.getClass().getSimpleName().toLowerCase(), model);
		return ModelFactory.createDefaultModel().read(this.getClass().getResource(path).openStream(), null);
	}

}