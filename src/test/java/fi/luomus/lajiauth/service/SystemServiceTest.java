package fi.luomus.lajiauth.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.IOException;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.junit.Test;

import com.google.common.base.Optional;

import fi.luomus.triplestore.client.TriplestoreClient;
import fi.luomus.utils.exceptions.ApiException;

public class SystemServiceTest {

	@Test
	public void testExistingSystemId() throws Exception {
		final String SYSTEM_ID = "KE.123";
		SystemService systemService = withMockModel("KE.123");
		String expected = "https://example.com/context/";
		String got = systemService.getSystemRedirectURI(SYSTEM_ID).get().toString();
		assertEquals(expected, got);
	}

	@Test
	public void testAbsentSystemId() throws Exception {
		final String SYSTEM_ID = "KE.124";
		SystemService systemService = withMockModel(SYSTEM_ID);
		assertFalse(systemService.getSystemRedirectURI(SYSTEM_ID).isPresent());
	}

	@Test
	public void testSystemName() throws Exception {
		assertEquals("Esimerkkijärjestelmä jolta puuttuu uri", withMockModel("KE.124").getSystemName("KE.124", "fi").get().get());
	}

	@Test
	public void testSystemNameWithNoNameForLocale() throws Exception {
		assertEquals("An example system missing an uri", withMockModel("KE.124").getSystemName("KE.124", "sv").get().get());
	}

	@Test
	public void testSystemNameWithNullLocale() throws Exception {
		assertEquals("An example system missing an uri", withMockModel("KE.124").getSystemName("KE.124", null).get().get());
	}

	@Test
	public void testSystemNamePresentNoName() throws Exception {
		assertFalse(withMockModel("KE.123").getSystemName("KE.123", "fi").get().isPresent());
	}

	@Test
	public void testSystemNameNotPresent() throws Exception {
		TriplestoreClient triplestoreClient = mock(TriplestoreClient.class);
		when(triplestoreClient.getResource(anyString())).thenReturn(Optional.<Model>absent());
		assertFalse(new SystemService(triplestoreClient, null).getSystemName("KE.doesnotexist", "fi").isPresent());
	}

	private SystemService withMockModel(String modelName) throws ApiException, IOException {
		TriplestoreClient triplestoreClient = mock(TriplestoreClient.class);
		when(triplestoreClient.getResource(modelName)).thenReturn(Optional.of(readModel(modelName)));
		return new SystemService(triplestoreClient, null);
	}

	private Model readModel(String model) throws IOException {
		String path = String.format("/%s/%s.xml", this.getClass().getSimpleName().toLowerCase(), model);
		return ModelFactory.createDefaultModel().read(this.getClass().getResource(path).openStream(), null);
	}

}