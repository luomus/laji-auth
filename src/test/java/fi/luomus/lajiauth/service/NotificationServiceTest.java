package fi.luomus.lajiauth.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.net.URI;

import org.junit.Test;
import org.mockito.ArgumentCaptor;

public class NotificationServiceTest {

    @Test
    public void testUserConfirmationRequest() throws Exception {
        EmailService emailService = mock(EmailService.class);
        EmailService.EmailBuilder emailBuilder = mockBuilder();
        when(emailService.email()).thenReturn(emailBuilder);
        
        NotificationService notificationService = new NotificationService(emailService);

        notificationService.notifyEmailConfirmationRequest("my@email.com", "My Name", "en", new URI("http://gohere.to.confirm"));
        
        ArgumentCaptor<String> captor = ArgumentCaptor.forClass(String.class);
        verify(emailBuilder).to(captor.capture());
        assertEquals("my@email.com", captor.getValue());
        verify(emailBuilder).body(captor.capture());
        String body = captor.getValue();
        assertTrue(body.startsWith("Dear My Name,"));
        assertTrue(body.contains("http://gohere.to.confirm"));
    }

    private EmailService.EmailBuilder mockBuilder() {
        EmailService.EmailBuilder builder = mock(EmailService.EmailBuilder.class);
        when(builder.from(anyString())).thenReturn(builder);
        when(builder.to(anyString())).thenReturn(builder);
        when(builder.subject(anyString())).thenReturn(builder);
        when(builder.body(anyString())).thenReturn(builder);
        return builder;
    }
}