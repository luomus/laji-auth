package fi.luomus.lajiauth.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.UUID;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class LocalizationServiceTest {
    @Mock
    private LanguageService languageService;
    @InjectMocks
    private LocalizationService service;

    @Test
    public void testGetMessagePresent() {
        when(languageService.getLanguage()).thenReturn("fi");
        assertEquals("present", service.getMessage("test-messages", "basic"));
    }

    @Test
    public void testGetMessageFormat() {
        when(languageService.getLanguage()).thenReturn("fi");
        String format = UUID.randomUUID().toString();
        assertEquals("the message is " + format, service.getMessage("test-messages", "format", format));
    }

    @Test
    public void testSwedish() {
        when(languageService.getLanguage()).thenReturn("sv");
        assertEquals("här", service.getMessage("test-messages", "basic"));
    }
}