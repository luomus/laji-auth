package fi.luomus.lajiauth.service;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.UUID;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import fi.luomus.lajiauth.service.LocalUserService.HashWithNonce;
import fi.luomus.lajiauth.util.Hasher;
import net.avh4.test.junit.Nested;

@RunWith(Nested.class)
public class LocalUserServiceHashTests {
	
	private final String EMAIL = "foo@bar.com";
	private LocalUserService service;
	private HashWithNonce hashWithNonce;

	@Before
	public void init() {
		final Hasher hasher = new Hasher("foobar");
		service = new LocalUserService(null, new HasherService(null) {
			@Override
			public Hasher getHasher() {
				return hasher;
			}
		});
		hashWithNonce = service.hashLocalUser(EMAIL);
	}

	@Test
	public void testValid() {
		assertTrue(service.isValidLocalUser(EMAIL, hashWithNonce));
	}

	@Test
	public void testInvalidNonce() {
		assertFalse(service.isValidLocalUser(EMAIL,
				new HashWithNonce(hashWithNonce.hash, UUID.randomUUID().toString())));
	}

	@Test
	public void testInvalidHash() {
		String invalidHash = service.hashLocalUser("invalid@example.com").hash;
		assertFalse(service.isValidLocalUser(EMAIL,
				new HashWithNonce(invalidHash, hashWithNonce.nonce)));
	}

	@Test
	public void testInvalidUserId() {
		assertFalse(service.isValidLocalUser("invalid@example.com", hashWithNonce));
	}

}