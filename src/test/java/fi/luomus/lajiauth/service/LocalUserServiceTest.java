package fi.luomus.lajiauth.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.UUID;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;

import fi.luomus.lajiauth.model.AuthenticationSourceUser;
import fi.luomus.lajiauth.model.LocalUser;
import fi.luomus.lajiauth.repository.LocalUserRepository;
import fi.luomus.lajiauth.util.Hasher;
import fi.luomus.utils.exceptions.NotFoundException;
import net.avh4.test.junit.Nested;

@RunWith(Nested.class)
public class LocalUserServiceTest {
    private LocalUserService localUserService;
    private LocalUserRepository localUserRepository;
    final private String EMAIL = "foo@bar.com";
    final private String STRONG_PASSWORD = "1FooBar123";

    @Before
    public void before() {
        this.localUserRepository = mock(LocalUserRepository.class);
        Hasher hasher = new Hasher("secret");
        HasherService hasherService = mock(HasherService.class);
        when(hasherService.getHasher()).thenReturn(hasher);
        localUserService = new LocalUserService(localUserRepository, hasherService);
    }

    @Test
    public void testFind() {
        assertFalse(localUserService.find(EMAIL).isPresent());
    }

    @Test
    public void testLogin() throws LocalUserService.InvalidCredentialsException {
        try {
            localUserService.login(EMAIL, STRONG_PASSWORD);
            fail("exception not raised");
        } catch (NotFoundException e) {
            // nop
        }
    }

    @Test
    public void testSetPassword() {
        try {
            localUserService.setPassword(EMAIL, STRONG_PASSWORD);
            fail("exception not raised");
        } catch (NotFoundException e) {
            // nop
        }
    }

    public class WithNotApprovedUser {
        private LocalUser localUser;

        @Before
        public void setUp() {
            this.localUser = new LocalUser();
            localUser.setEmail(EMAIL);
            localUser.setFullName("name");
            when(localUserRepository.find(EMAIL)).thenReturn(localUser);
            when(localUserRepository.updatePassword(eq(EMAIL), anyString(), anyString())).thenReturn(1);
        }

        @Test
        public void testFind() {
            AuthenticationSourceUser user = new AuthenticationSourceUser();
            user.setEmail(EMAIL);
            user.setId(EMAIL);
            user.setFullName(localUser.getFullName());
            assertEquals(user, localUserService.find(EMAIL).get());
        }

        @Test
        @Ignore
        public void testLogin() throws NotFoundException {
            try {
                localUserService.login(EMAIL, "pass");
                fail("exception not raised");
            } catch (LocalUserService.InvalidCredentialsException e) {
                // nop
            }
        }

        @Test
        public void passwordUpdated() throws NotFoundException {
            localUserService.setPassword(EMAIL, STRONG_PASSWORD);
            verify(localUserRepository, atLeastOnce()).updatePassword(eq(EMAIL), anyString(), anyString());
        }

        public class WithPasswordSet {
            private final String PASSWORD = STRONG_PASSWORD;
            private LocalUser withPassword;

            @Before
            public void setUp() throws NotFoundException {
                localUserService.setPassword(EMAIL, PASSWORD);
                ArgumentCaptor<String> passwordCapturer = ArgumentCaptor.forClass(String.class);
                ArgumentCaptor<String> nonceCapturer = ArgumentCaptor.forClass(String.class);
                verify(localUserRepository).updatePassword(eq(EMAIL), passwordCapturer.capture(), nonceCapturer.capture());
                this.withPassword = new LocalUser();
                this.withPassword.setFullName(localUser.getFullName());
                this.withPassword.setEmail(localUser.getEmail());
                this.withPassword.setPassword(passwordCapturer.getValue());
                this.withPassword.setNonce(nonceCapturer.getValue());
                when(localUserRepository.find(EMAIL)).thenReturn(this.withPassword);
            }

            @Test
            @Ignore
            public void canLogin() throws NotFoundException, LocalUserService.InvalidCredentialsException {
                localUserService.login(EMAIL, PASSWORD);
            }

            @Test
            @Ignore
            public void cantLoginWithWrongPassword() throws NotFoundException {
                try {
                    localUserService.login(EMAIL, "invalid");
                    fail("not raised");
                } catch (LocalUserService.InvalidCredentialsException e) {
                    // nop
                }
            }

            public class WithNonceAltered {
                @Before
                public void alterNonce() {
                    withPassword.setNonce(UUID.randomUUID().toString());
                }

                @Test
                @Ignore
                public void loginFails() throws NotFoundException {
                    try {
                        localUserService.login(EMAIL, PASSWORD);
                        fail("not raised");
                    } catch (LocalUserService.InvalidCredentialsException e) {
                        // nop
                    }
                }
            }
        }
    }

    public class PasswordTests {
        @Test
        public void tooShort() {
            assertFalse(localUserService.isPasswordStrongEnough("Nope!1"));
        }

        @Test
        public void noUppercase() {
            assertFalse(localUserService.isPasswordStrongEnough("notreallyno!1"));
        }

        @Test
        public void noLowercase() {
            assertFalse(localUserService.isPasswordStrongEnough("REALLYNOITSNOT!1"));
        }

        @Test
        public void noSpecialOrNumber() {
            assertFalse(localUserService.isPasswordStrongEnough("NoTGooDEnouGh"));
        }

        @Test
        public void justRight() {
            assertTrue(localUserService.isPasswordStrongEnough("And its gr3at"));
            assertTrue(localUserService.isPasswordStrongEnough("Me too !!!!!!!!!!"));
        }
    }
}