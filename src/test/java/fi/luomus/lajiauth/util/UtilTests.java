package fi.luomus.lajiauth.util;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import fi.luomus.lajiauth.resource.system.AppLogin;

public class UtilTests {
	
    @Test
    public void testTmpTokenParsing() {
        assertEquals("tmp_ABC", AppLogin.parseTempToken("foo/?bar=doo&tmpToken=tmp_ABC&zoo=hoo"));
        assertEquals("tmp_ABC", AppLogin.parseTempToken("?tmpToken=tmp_ABC"));
    }

}