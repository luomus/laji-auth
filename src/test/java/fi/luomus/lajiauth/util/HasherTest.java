package fi.luomus.lajiauth.util;

import org.junit.Test;

import static org.junit.Assert.*;

public class HasherTest {
    @Test
    public void testValid() {
        Hasher hasher = new Hasher("key");
        String hash = hasher.hash("foo");
        assertTrue(hasher.check("foo", hash));
    }

    @Test
    public void testInvalidContent() {
        Hasher hasher = new Hasher("key");
        String hash = hasher.hash("foo");
        assertFalse(hasher.check("bar", hash));
    }

    @Test
    public void testInvalidKey() {
        Hasher otherHasher = new Hasher("false key");
        String hash = otherHasher.hash("foo");

        Hasher hasher = new Hasher("key");
        assertFalse(hasher.check("bar", hash));
    }
}