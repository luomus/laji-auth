package fi.luomus.lajiauth.util;

import org.junit.Test;

import java.net.URI;
import java.net.URISyntaxException;

import static org.junit.Assert.assertEquals;

public class UriRelativizerTest {
    @Test
    public void testSimple() throws URISyntaxException {
        assertEquals(new URI("./static"), UriRelativizer.relativePath(new URI("http://localhost/admin/preferences"), new URI("http://localhost/admin/static")));
        assertEquals(new URI("../../static"), UriRelativizer.relativePath(new URI("http://localhost/admin/preferences/"), new URI("http://localhost/static")));
        assertEquals(new URI("../static"), UriRelativizer.relativePath(new URI("http://localhost/admin/preferences"), new URI("http://localhost/static")));
        assertEquals(new URI("../static"), UriRelativizer.relativePath(new URI("http://localhost/admin/preferences"), new URI("http://localhost/static/")));
        assertEquals(new URI("../../static"), UriRelativizer.relativePath(new URI("http://localhost/admin/preferences/"), new URI("http://localhost/static/")));
        assertEquals(new URI("../../foo/bar/baz"), UriRelativizer.relativePath(new URI("http://localhost/admin/preferences/perkele"), new URI("http://localhost/foo/bar/baz")));
        assertEquals(new URI("bar/baz"), UriRelativizer.relativePath(new URI("http://localhost/foo/file"), new URI("http://localhost/foo/bar/baz")));
    }

    @Test
    public void preserveQuery() throws URISyntaxException {
        assertEquals(new URI("../foobar?common=yes&unique=yes"), UriRelativizer.relativePath(
                new URI("http://localhost/admin/?common=no&original=yes"),
                new URI("http://localhost/foobar?common=yes&unique=yes")));
    }

    @Test
    public void testQueryEncoding() throws URISyntaxException {
        assertEquals(new URI("../foobar?encoded=%2Ftest%2F%C3%B6%C3%B6%C3%B6"), UriRelativizer.relativePath(
                new URI("http://localhost/admin/"),
                new URI("http://localhost/foobar?encoded=%2Ftest%2F%C3%B6%C3%B6%C3%B6")));
    }
    @Test
    public void testMultiQuery() throws URISyntaxException {
        assertEquals(new URI("../foobar?foo=bar&foo=baz"), UriRelativizer.relativePath(
                new URI("http://localhost/admin/"),
                new URI("http://localhost/foobar?foo=bar&foo=baz")));
    }
    @Test
    public void testMultiEncodedQuery() throws URISyntaxException {
        assertEquals(new URI("../foobar?%2Fintegration-test%2F%3Fsingle%3Dbaz%26multi%3Dfoo%26multi%3Dbar"), UriRelativizer.relativePath(
                new URI("http://localhost/admin/"),
                new URI("http://localhost/foobar?%2Fintegration-test%2F%3Fsingle%3Dbaz%26multi%3Dfoo%26multi%3Dbar")));
    }

}