package fi.luomus.lajiauth.integrationtests;

import org.apache.commons.io.FileUtils;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.Augmenter;

import java.io.File;
import java.io.IOException;

public class ScreenshotWatcher extends TestWatcher{
    private final WebDriver driver;

    public ScreenshotWatcher(WebDriver driver) {
        this.driver = driver;
    }

    @Override
    protected void failed(Throwable e, Description description) {
        WebDriver driver = new Augmenter().augment(this.driver);
        File srcFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        String filename = "/tmp/" + description.getDisplayName() + "_screenshot.png";
        try {
            FileUtils.copyFile(srcFile, new File(filename));
        } catch (IOException e1) {
            throw new RuntimeException(e1);
        }
        System.out.println("screenshot saved at " + filename);
    }
}
