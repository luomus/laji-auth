package fi.luomus.lajiauth.integrationtests;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.Rule;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import javax.ws.rs.core.UriBuilder;
import java.io.IOException;
import java.net.URI;
import java.net.URL;

public class BaseWebDriverTest extends BaseIntegrationTest {
    protected RemoteWebDriver driver = new PhantomJSDriver();
    @Rule
    public ScreenshotWatcher watcher = new ScreenshotWatcher(driver);

    protected Document hakaLogin(URI loginURI, String username, String name) throws IOException {
        driver.get(loginURI.toString());
        String href = driver.findElementById("haka-login").getAttribute("href");

        // inject fake username
        URL hakaUrl = UriBuilder.fromUri(href)
                .queryParam("authSourceUserId", username)
                .queryParam("authSourceName", name)
                .build().toURL();
        driver.get(hakaUrl.toString());
        return Jsoup.parse(hakaUrl, 10000);
    }
}
