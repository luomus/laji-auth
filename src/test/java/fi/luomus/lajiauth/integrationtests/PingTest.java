package fi.luomus.lajiauth.integrationtests;

import org.junit.Test;

import javax.ws.rs.core.GenericType;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class PingTest extends BaseIntegrationTest {
    @Test
    public void testPing() {
        Map<String, String> response = target.path("ping").request().get(new GenericType<Map<String, String>>() {
        });
        assertEquals("pong", response.get("response"));
    }
}
