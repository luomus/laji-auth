package fi.luomus.lajiauth.integrationtests.repository;

import javax.sql.DataSource;

import org.flywaydb.core.Flyway;
import org.junit.After;
import org.junit.Before;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class BaseRepositoryTest {
    private final Flyway flyway;
    protected ApplicationContext applicationContext;

    public BaseRepositoryTest() {
        this.applicationContext = new ClassPathXmlApplicationContext("/test-context.xml");
        DataSource dataSource = applicationContext.getBean(DataSource.class);
        flyway = new Flyway();
        flyway.setDataSource(dataSource);
        flyway.setLocations("filesystem:src/main/resources/db");
    }

    @Before
    public void setUp() {
       // flyway.clean();
       // flyway.migrate();
    }

    @After
    public void clean() {
       // flyway.clean();
    }
}
