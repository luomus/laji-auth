package fi.luomus.lajiauth.integrationtests.repository;

import fi.luomus.lajiauth.repository.UsedHashesRepository;
import net.avh4.test.junit.Nested;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.UUID;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(Nested.class)
public class UsedHashRepositoryTests extends BaseRepositoryTest {
    private UsedHashesRepository repo;
    private String hash;

    @Before
    public void initRepoAndHash() {
        this.repo = applicationContext.getBean(UsedHashesRepository.class);
        this.hash = UUID.randomUUID().toString();
    }

    @Test
    public void hashNotPresent() {
        assertFalse(repo.isHashUsed(hash));
    }

    public class WithHash {
        @Before
        public void addHash() {
            repo.addHash(hash);
        }

        @Test
        public void hashPresent() {
            assertTrue(repo.isHashUsed(hash));
        }

        @Test
        public void reAddingDoesNotThrow() {
            repo.addHash(hash);
        }
    }
}
