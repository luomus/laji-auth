package fi.luomus.lajiauth.integrationtests.repository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import java.util.UUID;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.skife.jdbi.v2.exceptions.UnableToExecuteStatementException;

import fi.luomus.lajiauth.model.LocalUser;
import fi.luomus.lajiauth.repository.LocalUserRepository;
import net.avh4.test.junit.Nested;

@RunWith(Nested.class)
public class LocalUserRepositoryTests extends BaseRepositoryTest {
	protected final String USER_EMAIL = UUID.randomUUID() + "@example.com";
	protected LocalUserRepository repo;

	@Before
	public void initRepo() {
		this.repo = this.applicationContext.getBean(LocalUserRepository.class);
	}

	@Test
	@Ignore
	public void userDoesNotExist() {
		assertNull(repo.find(USER_EMAIL));
	}

	public class UserWithPasswordExists {
		private LocalUser localUser;

		@Before
		public void createUser() {
			this.localUser = new LocalUser();
			localUser.setEmail(USER_EMAIL);
			localUser.setFullName(UUID.randomUUID().toString());
			localUser.setPassword(null);
			localUser.setNonce(null);
			repo.insert(localUser);
		}

		@Test
		@Ignore
		public void testGet() {
			assertEquals(localUser, repo.find(USER_EMAIL));
		}

		@Test
		@Ignore
		public void duplicateThrows() {
			try {
				repo.insert(localUser);
				fail("not raised");
			} catch (UnableToExecuteStatementException e) {
				// nop
			}
		}

		public class UserModified {
			@Before
			public void modifyUser() {
				repo.updatePassword(USER_EMAIL, "pass", "word");
			}

			@Test
			@Ignore
			public void assertChanged() {
				LocalUser changedUser = repo.find(USER_EMAIL);
				assertEquals("pass", changedUser.getPassword());
				assertEquals("word", changedUser.getNonce());
			}
		}

		public class UserDeleted {
			private int deletedAmount;

			@Before
			public void delete() {
				deletedAmount = repo.delete(USER_EMAIL);
			}

			@Test
			@Ignore
			public void testDeletedAmount() {
				assertEquals(1, deletedAmount);
			}

			@Test
			@Ignore
			public void testDoesNotExist() {
				assertNull(repo.find(USER_EMAIL));
			}

		}
	}
}