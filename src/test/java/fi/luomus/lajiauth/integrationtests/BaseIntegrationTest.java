package fi.luomus.lajiauth.integrationtests;

import fi.luomus.lajiauth.service.LajiAuthClient;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import org.junit.Before;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Properties;

public class BaseIntegrationTest {
    protected WebTarget target;
    protected LajiAuthClient client;
    protected String baseUri;
    protected final String CLIENT_SYSTEM_ID = "KE.342";
    private Properties properties;

    @Before
    public void setUpBase() throws URISyntaxException, IOException {
        properties = new Properties();
        properties.load(new FileInputStream(System.getProperty("lajiauth.secretsLocation")));
        this.baseUri = System.getProperty("lajiauth.baseUri");
        if (this.baseUri == null) {
            throw new RuntimeException("base uri property not configured");
        }
        this.client = new LajiAuthClient(CLIENT_SYSTEM_ID, new URI(this.baseUri));
        this.target = ClientBuilder.newClient().register(HttpAuthenticationFeature.basic("admin", "password")).target(this.baseUri);
    }

    protected String property(String key) {
        return properties.getProperty(key);
    }
}
