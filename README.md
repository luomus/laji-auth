# Laji-auth

The purpose of the Laji-auth authentication portal is to allow the users of the Finnish Biodiversity Information Facility (FinBIF) to log in with their home organizations authorization mechanism or use Google, Facebook and other types of authentication systems. The portal also offers the possibility to use a separate FinBIF-username/password authentication method.

After first log in using one of the provided authentication methods, the user is created a FinBIF-account (a person ID). It is possible for the user to associate several different authentication methods to their person ID.

Using existing common authentication methods reduces the maintenance associated with usernames (forgotten passwords, locked usernames, etc.) while lowering the threshold for users to register to the service (as the user doesn't have to create/remember yet another password).

Laji-auth can be used as a login mechanism for 3rd party applications: Anyone can request permissions to start using Laji-auth. FinBIF uses laji-auth for a number of it's own systems like FinBIF main portal, Taxon editor, Kotka collection management.

## Technical instructions

### Installation

Install PhantomJS and add /bin folder location to PATH.

Install the following components to your local maven repository:

* [Oracle JDBC Driver](http://www.oracle.com/technetwork/database/features/jdbc/default-2280470.html)
* [triplestore-client](https://bitbucket.org/luomus/triplestore-client/src/master/)
* [luomus-utils](https://bitbucket.org/luomus/utils/src/master/)
* [laji-auth-client](https://bitbucket.org/luomus/laji-auth-client/src/master/)

Example on how to install the Oracle jdbc driver in the local maven repository:

```shell
mvn install:install-file \
    -Dfile=ojdbc7.jar \
    -DgroupId=com.oracle \
    -DartifactId=oracle \
    -Dversion=12.1.0.2 \
    -Dpackaging=jar \
    -DgeneratePom=true
```

```shell
git clone https://bitbucket.org/luomus/laji-auth.git
cd laji-auth
```

#### Setting database credentials, etc.

```shell
cp secrets.properties.template secrets.properties
vi secrets.properties
```

And enter the following properties:

```text
# start the test server
mvn jetty:run

# configuration
GET http://localhost:10030/laji-auth/admin/preferences (user: admin, pass: password)

SECRET_KEY                  - The key used for encryption
DEV_MODE                    - true for test- or development server, false in production
LAJI_AUTH_SYSTEM_ID         - Laji-auth identifier (starting with KE) in the triplestore database

EMAIL_HOST                  - SMTP server address
ADMIN_EMAIL                 - The email address of the administrator for exception reporting

DB_URL                      - JDBC address of the development database
DB_USERNAME                 - development database username
DB_PASSWORD                 - development database password

TRIPLESTORE_URL             - address of the triplestore API
TRIPLESTORE_USERNAME        - triplestore username
TRIPLESTORE_PASSWORD        - triplestore password

FACEBOOK_APP_ID             - Facebook | Meta app id
FACEBOOK_SECRET             - Facebook | Meta app secret

GOOGLE_CLIENT_ID            - Google client id
GOOGLE_SECRET               - Goole client secret

INATURALIST_APP_ID          - Inaturalist app id 
INATURALIST_SECRET          - Inaturalist app secret

OMARIISTA_APP_ID            - Omariista app id 
OMARIISTA_SECRET            - Omariista app secret

APPLE_TEAM_ID               - This is ten characters, combination of letters and numbers 
APPLE_KEY_ID                - This is ten characters, combination of letters and numbers - created when private key is generated in Apple developer console
APPLE_PRIVATE_KEY           - Our private key generated in Apple developer console

DISCOURSE_SSO_TOKEN          - Sigle sign on token for forum access
DISCOURSE_URL                - Forum URL
DISCOURSE_SYSTEM_ID          - Forum software identifier (starting with KE) in the triplestore database

LAJI_API_ACCESS_TOKEN        - Laji-auth access token for api.laji.fi 
LAJI_API_URL                 - api base url, for example http://apitest.laji.fi/v0

```

#### Login to localhost for testing

```shell
mvn initialize flyway:migrate
mvn jetty:run
```

#### Perform unit and integration tests

```shell
mvn integration-test
```

#### Deployment

```shell
mvn -P staging initialize flyway:migrate
mvn -P staging tomcat:undeploy tomcat:deploy
```

and the same goes for the profile production, i.e.

```shell
mvn -P production
```

or create war manually

```shell
mvn install -DskipTests
```

In order for the deployment to succeed, the usernames with the server manager-script role must be in maven's settings.xml file:

```apacheconf
<servers>
    <server>
        <id>fmnh-ws-test</id>
        <username></username>
        <password></password>
    </server>
    <server>
        <id>fmnh-ws-prod</id>
        <username></username>
        <password></password>
    </server>
</servers>
```

It is a good idea to put a CSP header and other Recommended headers that prevent the use of the iframe on the test and production server. Note that in production and testing, the frame-Ancestors values must differ from those below.

```apacheconf
<IfModule mod_headers.c>
  Header always set X-XSS-Protection "1; mode=block"
  Header always set X-Content-Type-Options "nosniff"
  Header always set Strict-Transport-Security "max-age=31536000; includeSubDomains"
  Header always set Referrer-Policy "strict-origin"
</IfModule>
```

If Shibboleth is not available (e.g. local development) should be given to the application server
JVM setting `-Dlajiauth.fakeShibboleth=true`, so you can specify the user ID and name from shibboleth
with additional address parameters when logging in:

```text
GET http://localhost:10030/laji-auth/auth-sources/shibboleth/HAKA?target=KE.342&next=%2Ftest&authSourceUserId=teppo@yliopisto.fi&authSourceName=Teppo%20TheTester
```

### Shibboleth configuration

[General instructions for configuring Shibboleth in Haka (login required, in Finnish)](https://wiki.helsinki.fi/pages/viewpage.action?spaceKey=luomusict&title=Haka-federointi+Shibbolethilla)

*shibboleth2.xml:*

```xml
<RequestMapper type="Native">
    <RequestMap applicationId="default"> 
        <!-- requests go to default application except.. -->
        <Host name="fmnh-ws-test.it.helsinki.fi">
            <Path name="laji-auth" applicationId="default" authType="shibboleth" requireSession="true">
                <!-- fmnh-ws-test.it.helsinki.fi/laji-auth/virtu/* -requests to laji-virtu-application -->
                <Path name="virtu" applicationId="laji-virtu" authType="shibboleth" requireSession="true"/> 
            </Path>
        </Host>
    </RequestMap>
</RequestMapper>
...
<ApplicationDefaults entityID="https://fmnh-ws-test.it.helsinki.fi/laji-auth/"
    REMOTE_USER="eppn persistent-id targeted-id"
    signing="front" encryption="false"
    attributePrefix="AJP_">
    ...
    <Sessions lifetime="28800" timeout="3600" relayState="ss:mem"
      handlerURL="/laji-auth/Shibboleth.sso/haka" checkAddress="false" handlerSSL="true" cookieProps="https">
    </Sessions>
    <!-- Add other HAKA directives here - see wiki link -->
    
    <!-- virtu-application -->
    <ApplicationOverride id="laji-virtu" entityID="https://fmnh-ws-test.it.helsinki.fi/laji-auth/virtu/" REMOTE_USER="eppn persistent-id targeted-id"
    signing="front" encryption="false"
    attributePrefix="AJP_">
        <Sessions lifetime="28800" timeout="3600" relayState="ss:mem"
            handlerURL="/laji-auth/Shibboleth.sso/virtu" checkAddress="false" handlerSSL="true" cookieProps="https">
            <SSO discoveryProtocol="SAMLDS" discoveryURL="https://industria.csc.fi/DS/">
                SAML2 SAML1
            </SSO>

            <Logout>SAML2 Local</Logout>
            <Handler type="MetadataGenerator" Location="/Metadata" signing="false"/>
            <Handler type="Status" Location="/Status" acl="127.0.0.1 ::1"/>
            <Handler type="Session" Location="/Session" showAttributeValues="false"/>
            <Handler type="DiscoveryFeed" Location="/DiscoFeed"/>
        </Sessions>
        <!-- virtu metadata -->
        <MetadataProvider type="XML" uri="https://virtu-ds.csc.fi/fed/virtu-test/CSC_Virtu_Test_Servers-metadata.xml"
        backingFilePath="virtu-metadata.xml" reloadInterval="7200">
        <MetadataFilter type="RequireValidUntil" maxValidityInterval="2419200"/>
        <!-- virtu certificate -->
        <MetadataFilter type="Signature" certificate="/etc/pki/tls/certs/virtu_test.crt"/>
        </MetadataProvider>
        <!-- our secret key -->
        <CredentialResolver type="File" key="/etc/pki/tls/private/laji_virtu_sp.key" certificate="/etc/pki/tls/certs/laji_virtu_sp.crt"/>
    </ApplicationOverride>
</ApplicationDefaults>
```

Apache:

```apacheconf
<!-- all requests to shibboleth starting with /Shibboleth.sso -->
<Location /Shibboleth.sso>
  SetHandler shib
  ProxyPass !
</Location>
<!-- require Shibboleth authentication for these addresses -->
<Location /auth-sources/shibboleth/VIRTU>
   require valid-user
   AuthType shibboleth
   ShibRequestSetting requireSession 1
   <!-- same as applicationId in shibboleth conf -->
   ShibRequestSetting applicationId laji-virtu
</Location>
<Location /auth-sources/shibboleth/HAKA>
   require valid-user
   AuthType shibboleth
   ShibRequestSetting requireSession 1
   ShibRequestSetting applicationId default
</Location>
<Location /Shibboleth.sso/haka>
   ShibRequestSetting applicationId default
</Location>
<Location /Shibboleth.sso/virtu>
   ShibRequestSetting applicationId laji-virtu
</Location>
```

### API Interface

See [API documentation](doc/lajiauth_api_fi.md).

#### Client software

You can use the following libraries to implement a client software that logs in using laji-auth: [Java-client](https://bitbucket.org/luomus/laji-auth) or [PHP-client](https://bitbucket.org/luomus/laji-auth-client-php).

## Functionality

### Concept

* **User** - A user who has credentials to an external authentication system supported by Laji-auth
* **External authentication system** - Authentication source supported by laji-auth authentication portal
* **External authentication system userid** - User's id in that authentication system (for example google id, virtu username, iNaturalist username etc)
* **Target system id** - FinBIF triplestore system id to which the end-user authentication is forwarded
* **Person id** - The identity of the person in triplestore (MA.123 format) to which several authentication methods user ids can be associated. For example, a user with a triplestore person id MA.123 can log in with the University of Helsinki and Facebook IDs, if both systems external userids are associated to that user.

### Description of the authentication process

See: [https://laji.fi/about/1328](https://laji.fi/about/1328)

#### Managing Your Information

The user can see a summary of the authentication methods associated to his account in addition to other information like name, email etc by logging in to laji-auth itself:

```text
GET /laji-auth/self
```

Also if the user tries to log in to front page of laji-auth application, the user is redirected to this info page.

On the info page the user can also associate new authentication methods to the account, invalidate permanent tokens and update name, email address and other information.

## Available authentication methods

### Haka

Haka is a trust network used by all (?) Finnish universities and maintained by CSC.
The actual authentication does not take place in Haka itself, but Haka directs the user's own home organization (eg the University of Helsinki)
system (Identity Provider, IdP), which, after the user is authenticated, transmits the user's information to Haka
Service Provider (SP).

Haka and its members use the SAMLv2 protocol and, as a rule, its Shibboleth software implementation.

### Virtu

Virtu is a "Haka" of government agencies and other public sector actors, maintained by Valtori.
Its technical implementation is pretty much the same as Haka.

### Google and Facebook

Authentication with your own Facebook and Google IDs is also possible. The user is asked to provide
an OAuth authorization for the Laji information center to act as authentication.

### Local user accounts

The users of the Biodiversity Information Facility (FinBIF) without the ability to identify themselves with any of the supported authentication methods, can create a separate FinBIF login (usernam/password).

The primary ID for local users is the user's own email address, which is verified when the IDs are created.

A local user can recover or change a forgotten password by requesting a password reset email.

#### Managing local users

```text
GET /laji-auth/admin/local-users
```

### Oma riista

Oma riista is service provided by The Finnish Wildlife Agency. It provides oauth-like interface for 3rd party apps (laji-auth included) to log in using oma riista accounts.

### iNaturalist

iNaturalist is a popular observation platform. It provides oauth-interface for 3rd party apps (laji-auth included) to log in using iNaturalist accounts.

### LUOMUS (to be removed Jan/2022)

You can also log in to Laji-auth with your "museum IDs." Internally, Laji-auth uses the Kotka JSON interface to check your username and password.
